apply(from = "../versions.gradle.kts")
val koin_version: String by extra
val coroutines_version: String by extra
val ktor_version: String by extra
val serialization_version: String by extra
val compose_ui: String by extra

plugins {
    id("multiplatform-library-convention")
    kotlin("plugin.serialization")
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("io.insert-koin:koin-core:$koin_version")
                implementation(project(":CoreMVIAbstract"))
                implementation(project(":CoreBackendApi"))
                implementation(project(":Utils"))
                implementation(project(":FeatureAuthApi"))
                implementation(project(":FeatureNavigationApi"))
                implementation(project(":CorePreferenceApi"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$serialization_version")
                implementation("io.ktor:ktor-client-serialization:$ktor_version")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
            }
        }
        val androidMain by getting {
            dependencies {
                implementation("com.firebaseui:firebase-ui-auth:7.2.0")
                implementation("com.facebook.android:facebook-android-sdk:12.0.0")
            }
        }
    }
}
