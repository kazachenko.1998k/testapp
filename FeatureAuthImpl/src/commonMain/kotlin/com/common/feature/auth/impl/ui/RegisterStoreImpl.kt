package com.common.feature.auth.impl.ui

import com.common.core.mviabstraction.ErrorEffect
import com.common.core.mviabstraction.helpers.ClearFocusFunctionality
import com.common.core.mviabstraction.usecases.BannerUseCase
import com.common.feature.auth.api.data.models.SocialLogin
import com.common.feature.auth.api.domain.RegisterCallbacks
import com.common.feature.auth.api.ui.RegisterStore
import com.common.feature.auth.api.usecases.*
import com.common.feature.navigation.api.usecases.BottomSheetUseCase
import com.common.utils.emailMaxLength
import com.common.utils.emailRegex
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch

class RegisterStoreImpl(
    private val registerUseCase: RegisterUseCase,
    private val loginUseCase: LoginUseCase,
    private val userAgreementAndPrivacyPoliciesUseCase: UserAgreementAndPrivacyPoliciesUseCase,
    private val passValidationUseCase: PassValidationUseCase,
    private val callbacks: RegisterCallbacks,
    private val facebookLoginUseCase: FacebookLoginUseCase,
    private val googleLoginUseCase: GoogleLoginUseCase,
    private val bottomSheetUseCase: BottomSheetUseCase,
    snackBarUseCase: BannerUseCase,
    state: RegisterState
) : RegisterStore(state) {

    private var needReopenBottomSheet = false

    init {
        snackBarUseCase.subscribe(this)
        launch {
            observeState().collect {
                updateState { copy(registerBtnEnabled = !hasErrorOnEmail() && !passValidationUseCase.passHasError) }
            }
        }
        launch {
            passValidationUseCase.pass.collect {
                updateState { copy(pass = it) }
            }
        }
        launch {
            loginUseCase.isLoggedIn().filter { it }.collect {
                passValidationUseCase.reset()
                updateState { RegisterState.defaultInstance() }
            }
        }
    }

    override fun dispatch(action: RegisterStoreAction) {
        if (currentState().isLoading) return
        when (action) {
            is OnEmailModify -> updateState {
                copy(
                    email = email.copy(
                        action.newText.take(emailMaxLength),
                        hasError = false
                    )
                )
            }
            is OnShowScreen -> launch {
                if (needReopenBottomSheet) {
                    needReopenBottomSheet = false
                    dispatch(OnRegisterClick)
                }
            }
            is OnPassModify -> {
                passValidationUseCase.onPassModify(action.newText)
            }
            is OnPassChangeVisible -> {
                passValidationUseCase.onPassChangeVisible()
            }
            is OnEmailModifyEnd -> {
                updateState {
                    copy(
                        email = email.copy(hasFocusOnce = true)
                    )
                }
                if (hasErrorOnEmail()) {
                    showErrorEmail()
                }
            }
            is OnPassModifyEnd -> {
                passValidationUseCase.onPassModifyEnd()
            }
            is OnPrivacyPolicyClick, is OnTermsAndConditionsClick -> launch {
                needReopenBottomSheet = true
                bottomSheetUseCase.close()
            }
            is OnPassModifyStart -> {
                passValidationUseCase.onPassModifyStart()
            }
            is OnBackClick -> callbacks.onBackClick()
            is OnAcceptAndContinueClick -> launch {
                userAgreementAndPrivacyPoliciesUseCase.accept()
                registerNewAccount()
            }
            is OnRegisterClick -> launch {
                emitSideEffect(ClearFocusFunctionality.ClearEffect)
                if (!userAgreementAndPrivacyPoliciesUseCase.hasAccept()) {
                    bottomSheetUseCase.awaitOpen()
                    return@launch
                }
                registerNewAccount()
            }
            is OnLoginClick -> callbacks.onLoginClick()
            is OnFacebookClick -> launch {
                updateState {
                    copy(socialRegisterState = SocialLogin.Facebook)
                }
                val respToken = facebookLoginUseCase.login()
                respToken.onSuccess {
                    val resp = loginUseCase.loginFacebook(it)
                    resp.onSuccess {
                        callbacks.onSuccessRegister()
                    }
                    resp.onFailure { error ->
                        emitSideEffect(ErrorEffect(error))
                    }
                }
                respToken.onFailure {
                    emitSideEffect(ErrorEffect(it))
                }
                updateState {
                    copy(socialRegisterState = null)
                }
            }
            is OnGoogleClick -> launch {
                updateState {
                    copy(socialRegisterState = SocialLogin.Google)
                }
                val respToken = googleLoginUseCase.login()
                respToken.onSuccess {
                    val resp = loginUseCase.loginGoogle(it)
                    resp.onSuccess {
                        callbacks.onSuccessRegister()
                    }
                    resp.onFailure { error ->
                        emitSideEffect(ErrorEffect(error))
                    }
                }
                respToken.onFailure {
                    emitSideEffect(ErrorEffect(it))
                }
                updateState {
                    copy(socialRegisterState = null)
                }
            }

        }
    }

    private suspend fun registerNewAccount() {
        updateState { copy(isLoading = true) }
        bottomSheetUseCase.close()
        val resp = registerUseCase.registerEmail(
            email = currentState().email.text,
            pass = currentState().pass.field.text
        )
        resp.onSuccess {
            callbacks.onSuccessRegister()
        }
        resp.onFailure {
            emitSideEffect(ErrorEffect(it))
        }
        updateState { copy(isLoading = false) }
    }

    private fun showErrorEmail() {
        updateState {
            copy(
                email = email.copy(hasError = true),
            )
        }
    }

    private fun hasErrorOnEmail() = !emailRegex.matches(currentState().email.text)

}