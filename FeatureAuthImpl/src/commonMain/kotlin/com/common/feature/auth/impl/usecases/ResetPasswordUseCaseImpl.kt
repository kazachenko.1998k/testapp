package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.data.AuthRemoteApi
import com.common.feature.auth.api.data.models.ResendCodeEmailReq
import com.common.feature.auth.api.usecases.ResetPasswordUseCase

class ResetPasswordUseCaseImpl(
    private val remoteApi: AuthRemoteApi,
) : ResetPasswordUseCase {

    override suspend fun resetPasswordEmail(email: String): Result<Unit> =
        remoteApi.sendCodeToEmail(ResendCodeEmailReq(email))

}