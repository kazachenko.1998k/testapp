package com.common.feature.auth.impl.domain

import com.common.feature.auth.api.domain.VerifyCodeProvider
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

class VerifyCodeProviderImpl : VerifyCodeProvider {
    var token = MutableStateFlow<String?>(null)
    var email = MutableStateFlow<String?>(null)

    override fun verifyCodeEmail(): Flow<String?> = email

    override fun tokenFromResponseOnValidationCode(): Flow<String?> = token

    override fun setEmailForVerifyCode(newEmail: String) {
        email.value = newEmail
    }

    override fun setTokenFromResponseOnValidationCode(newToken: String) {
        token.value = newToken
    }
}