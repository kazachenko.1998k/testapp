package com.common.feature.auth.impl.data

import com.common.feature.auth.api.data.AuthLocalApi
import com.common.feature.auth.api.data.models.Token
import com.common.feature.auth.impl.data.repository.AuthLocalRepositoryApi
import kotlinx.coroutines.flow.Flow


class AuthLocalImpl(private val localRepository: AuthLocalRepositoryApi) : AuthLocalApi {
    override suspend fun saveToken(token: Token) =
        localRepository.saveToken(token.value)

    override fun getToken(): Flow<String?> =
        localRepository.getToken()

    override suspend fun acceptUserAgreementAndPrivacyPolicies() {
        localRepository.acceptUserAgreementAndPrivacyPolicies()
    }

    override suspend fun hasAcceptUserAgreementAndPrivacyPolicies(): Flow<Boolean> =
        localRepository.hasAcceptUserAgreementAndPrivacyPolicies()

}