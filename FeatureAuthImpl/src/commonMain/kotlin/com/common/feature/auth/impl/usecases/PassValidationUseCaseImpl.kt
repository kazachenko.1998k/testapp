package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.data.models.ErrorCheckState
import com.common.feature.auth.api.data.models.PasswordState
import com.common.feature.auth.api.usecases.PassValidationUseCase
import com.common.utils.models.FieldEditable
import com.common.utils.passMaxLength
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine

class PassValidationUseCaseImpl : PassValidationUseCase {

    private val _hasMinimum8Characters = MutableStateFlow(ErrorCheckState.NOT_CHECK)
    private val _hasOneNumber = MutableStateFlow(ErrorCheckState.NOT_CHECK)
    private val _passTrust = MutableStateFlow(ErrorCheckState.NOT_CHECK)
    private val _helperCheckersIsVisible = MutableStateFlow(false)
    private val _pass = MutableStateFlow(FieldEditable.defaultInstance())
    override val pass = combine(
        _hasMinimum8Characters,
        _hasOneNumber,
        _passTrust,
        _helperCheckersIsVisible,
        _pass
    ) { hasMinimum8Characters,
        hasOneNumber,
        passTrust,
        helperCheckersIsVisible,
        pass ->
        PasswordState(
            hasMinimum8Characters,
            hasOneNumber,
            passTrust,
            helperCheckersIsVisible,
            pass
        )
    }
    override val passHasError: Boolean
        get() = hasErrorFirstOnPass()

    override fun onPassModify(newText: String) {
        updateValidationFirstPass(newText.take(passMaxLength))
    }

    override fun reset() {
        _hasMinimum8Characters.value = ErrorCheckState.NOT_CHECK
        _hasOneNumber.value = ErrorCheckState.NOT_CHECK
        _helperCheckersIsVisible.value = false
        _pass.value = FieldEditable.defaultInstance()
    }

    override fun onPassChangeVisible() {
        _pass.value = _pass.value.copy(hide = !_pass.value.hide)
    }

    override fun onPassModifyEnd() {
        _helperCheckersIsVisible.value = false
        _pass.value = _pass.value.copy(hasFocusOnce = true)
        updateValidationFirstPass(_pass.value.text)
    }

    override fun onPassModifyStart() {
        _helperCheckersIsVisible.value = true
    }

    private fun updateValidationFirstPass(newPass: String) {
        val minimum8Characters = validateStatePassMinimum8(newPass)
        val oneNumber = validateStatePassOneNumber(newPass)
        val passTrust = validateStatePassTrust(newPass)
        _hasMinimum8Characters.value = minimum8Characters
        _hasOneNumber.value = oneNumber
        _passTrust.value = passTrust
        _pass.value = _pass.value.copy(
            text = newPass,
            hasError = if (_pass.value.hasFocusOnce) {
                hasErrorFirstOnPass()
            } else _pass.value.hasError
        )
    }

    private fun validateStatePassMinimum8(newPass: String) =
        validateStatePass { newPass.length >= minLength }

    private fun validateStatePassOneNumber(newPass: String) =
        validateStatePass { newPass.any { it.isDigit() } }

    private fun validateStatePassTrust(newPass: String) =
        if (newPass.length >= minLength && !newPass.any { it.isLetter() }) ErrorCheckState.ERROR else ErrorCheckState.SUCCESS

    private fun hasErrorFirstOnPass() = listOf(
        _hasMinimum8Characters,
        _hasOneNumber,
        _passTrust
    ).any { it.value != ErrorCheckState.SUCCESS }

    private fun validateStatePass(condition: () -> Boolean): ErrorCheckState = when {
        condition.invoke() -> ErrorCheckState.SUCCESS
        _pass.value.hasFocusOnce -> ErrorCheckState.ERROR
        else -> ErrorCheckState.NOT_CHECK
    }

    companion object {
        private const val minLength = 8
    }
}