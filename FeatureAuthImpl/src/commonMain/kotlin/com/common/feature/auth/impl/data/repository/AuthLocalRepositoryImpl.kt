package com.common.feature.auth.impl.data.repository

import com.common.core.preferences.api.PreferencesApi
import com.common.core.preferences.api.PreferencesApi.ConstantsKey.TOKEN
import com.common.core.preferences.api.PreferencesApi.ConstantsKey.UserAgreementAndPrivacyPolicies
import kotlinx.coroutines.flow.Flow

class AuthLocalRepositoryImpl(private val preferences: PreferencesApi) : AuthLocalRepositoryApi {

    override suspend fun saveToken(token: String) =
        preferences.putString(TOKEN, token)

    override fun getToken(): Flow<String?> =
        preferences.getStringOrNullFlow(TOKEN)

    override suspend fun acceptUserAgreementAndPrivacyPolicies() =
        preferences.putBoolean(UserAgreementAndPrivacyPolicies, true)

    override suspend fun hasAcceptUserAgreementAndPrivacyPolicies(): Flow<Boolean> =
        preferences.getBooleanFlow(UserAgreementAndPrivacyPolicies, false)

}