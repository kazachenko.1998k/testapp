package com.common.feature.auth.impl.ui.password

import com.common.core.mviabstraction.ErrorEffect
import com.common.core.mviabstraction.usecases.BannerUseCase
import com.common.feature.auth.api.domain.ResetPasswordCallbacks
import com.common.feature.auth.api.domain.VerifyCodeProvider
import com.common.feature.auth.api.ui.ResetPasswordStore
import com.common.feature.auth.api.usecases.LoginUseCase
import com.common.feature.auth.api.usecases.ResetPasswordUseCase
import com.common.feature.auth.api.usecases.VerifyCodeTypeUseCase
import com.common.utils.emailMaxLength
import com.common.utils.emailRegex
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch

class ResetPasswordStoreImpl(
    private val resetPasswordUseCase: ResetPasswordUseCase,
    private val loginUseCase: LoginUseCase,
    private val callbacks: ResetPasswordCallbacks,
    private val verifyCodeProvider: VerifyCodeProvider,
    private val verifyCodeTypeUseCase: VerifyCodeTypeUseCase,
    snackBarUseCase: BannerUseCase,
    state: ResetPasswordState
) : ResetPasswordStore(state) {

    init {
        snackBarUseCase.subscribe(this)
        launch {
            observeState().collect {
                updateState { copy(sendBtnEnabled = !hasErrorOnEmail(it.email.text)) }
            }
        }
        launch {
            loginUseCase.isLoggedIn().filter { it }.collect {
                dispatch(OnEmailModify(""))
            }
        }
    }

    override fun dispatch(action: ResetPasswordStoreAction) {
        when (action) {
            is OnEmailModify -> updateState {
                val pass = action.newText.take(emailMaxLength)
                copy(
                    email = email.copy(
                        pass,
                        hasError = hasErrorOnEmail(pass) && email.hasFocusOnce
                    )
                )
            }
            is OnEmailModifyEnd -> {
                updateState {
                    copy(
                        email = email.copy(
                            hasFocusOnce = true,
                            hasError = hasErrorOnEmail()
                        )
                    )
                }
            }
            is OnBackClick -> callbacks.onBackClick()
            is OnSendInstructionsClick -> launch {
                updateState { copy(isLoading = true) }
                val resp = resetPasswordUseCase.resetPasswordEmail(
                    currentState().email.text
                )
                resp.onSuccess {
                    verifyCodeProvider.setEmailForVerifyCode(currentState().email.text)
                    verifyCodeTypeUseCase.setNewValue(VerifyCodeTypeUseCase.VerifyCodeType.ResetPassword)
                    callbacks.onSuccessResetPassword()
                }
                resp.onFailure {
                    emitSideEffect(ErrorEffect(it))
                }

                updateState { copy(isLoading = false) }
            }
        }
    }

    private fun hasErrorOnEmail(text: String = currentState().email.text): Boolean =
        !emailRegex.matches(text)
}