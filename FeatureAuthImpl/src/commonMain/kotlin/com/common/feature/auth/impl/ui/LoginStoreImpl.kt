package com.common.feature.auth.impl.ui

import com.common.core.mviabstraction.ErrorEffect
import com.common.core.mviabstraction.helpers.ClearFocusFunctionality
import com.common.core.mviabstraction.usecases.BannerUseCase
import com.common.feature.auth.api.data.models.SocialLogin
import com.common.feature.auth.api.domain.LoginCallbacks
import com.common.feature.auth.api.ui.LoginStore
import com.common.feature.auth.api.usecases.FacebookLoginUseCase
import com.common.feature.auth.api.usecases.GoogleLoginUseCase
import com.common.feature.auth.api.usecases.LoginUseCase
import com.common.utils.emailMaxLength
import com.common.utils.passMaxLength
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.launch

class LoginStoreImpl(
    private val loginUseCase: LoginUseCase,
    private val callbacks: LoginCallbacks,
    private val facebookLoginUseCase: FacebookLoginUseCase,
    private val googleLoginUseCase: GoogleLoginUseCase,
    snackBarUseCase: BannerUseCase,
    state: LoginState
) : LoginStore(state) {

    init {
        snackBarUseCase.subscribe(this)
        launch {
            observeState().collect {
                updateState { copy(loginBtnEnabled = email.text.isNotEmpty() && pass.text.isNotEmpty()) }
            }
        }
        launch {
            loginUseCase.isLoggedIn().filter { it }.collect {
                updateState { LoginState.defaultInstance() }
            }
        }
    }

    override fun dispatch(action: LoginStoreAction) {
        when (action) {
            is OnEmailModify -> updateState {
                copy(email = email.copy(action.newText.take(emailMaxLength)))
            }
            is OnPassModify -> updateState {
                copy(pass = pass.copy(action.newText.take(passMaxLength)))
            }
            is OnBackClick -> callbacks.onBackClick()
            is OnRegisterClick -> callbacks.onRegisterClick()
            is OnPassChangeVisible -> updateState {
                copy(pass = pass.copy(hide = !pass.hide))
            }
            is OnForgotPasswordClick -> callbacks.onForgotPasswordClick()
            is OnLoginClick -> launch {
                emitSideEffect(ClearFocusFunctionality.ClearEffect)
                updateState { copy(isLoading = true) }
                val resp = loginUseCase.loginEmail(
                    currentState().email.text,
                    currentState().pass.text
                )
                resp.onSuccess {
                    callbacks.onSuccessLogin()
                }
                resp.onFailure {
                    emitSideEffect(ErrorEffect(it))
                }
                updateState { copy(isLoading = false) }
            }
            is OnFacebookClick -> launch {
                updateState {
                    copy(socialLoginState = SocialLogin.Facebook)
                }
                val respToken = facebookLoginUseCase.login()
                respToken.onSuccess {
                    val resp = loginUseCase.loginFacebook(it)
                    resp.onSuccess {
                        callbacks.onSuccessLogin()
                    }
                    resp.onFailure { error ->
                        emitSideEffect(ErrorEffect(error))
                    }
                }
                respToken.onFailure {
                    emitSideEffect(ErrorEffect(it))
                }
                updateState {
                    copy(socialLoginState = null)
                }
            }
            is OnGoogleClick -> launch {
                updateState {
                    copy(socialLoginState = SocialLogin.Google)
                }
                val respToken = googleLoginUseCase.login()
                respToken.onSuccess {
                    val resp = loginUseCase.loginGoogle(it)
                    resp.onSuccess {
                        callbacks.onSuccessLogin()
                    }
                    resp.onFailure { error ->
                        emitSideEffect(ErrorEffect(error))
                    }
                }
                respToken.onFailure {
                    emitSideEffect(ErrorEffect(it))
                }
                updateState {
                    copy(socialLoginState = null)
                }
            }
        }
    }

}