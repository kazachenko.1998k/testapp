package com.common.feature.auth.impl.ui

import com.common.feature.auth.api.domain.HaveAccountQuestionCallbacks
import com.common.feature.auth.api.ui.HaveAccountQuestionStore

class HaveAccountQuestionStoreImpl(
    private val callbacks: HaveAccountQuestionCallbacks,
    state: HaveAccountQuestionState
) : HaveAccountQuestionStore(state) {

    override fun dispatch(action: HaveAccountQuestionStoreAction) {
        when (action) {
            OnGetStartedClick -> callbacks.onRegisterClick()
            OnLoginClick -> callbacks.onLoginClick()
        }
    }

}