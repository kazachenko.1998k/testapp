package com.common.feature.auth.impl.data.rawmodels

import com.common.core.backend.ReqResMapper
import com.common.feature.auth.api.data.models.*
import kotlinx.serialization.Serializable

@Serializable
data class ResetPasswordEmailReqImpl(
    val email: String,
)

class ResetPasswordEmailMapper :
    ReqResMapper<ResetPasswordEmailReq, ResetPasswordEmailReqImpl, Unit, ResetPasswordEmailRes> {

    override fun fromCommon(from: ResetPasswordEmailReq): ResetPasswordEmailReqImpl =
        ResetPasswordEmailReqImpl(email = from.email)

    override fun toCommon(from: Unit): ResetPasswordEmailRes =
        ResetPasswordEmailRes()
}


@Serializable
data class ResetPasswordCodeEmailReqImpl(
    val email: String,
    val code: String,
)

@Serializable
data class ResetPasswordCodeEmailResImpl(
    val token: String
)

class ResetPasswordCodeEmailMapper :
    ReqResMapper<ResetPasswordCodeEmailReq, ResetPasswordCodeEmailReqImpl, ResetPasswordCodeEmailResImpl, ResetPasswordCodeEmailRes> {

    override fun fromCommon(from: ResetPasswordCodeEmailReq): ResetPasswordCodeEmailReqImpl =
        ResetPasswordCodeEmailReqImpl(email = from.email, code = from.code)

    override fun toCommon(from: ResetPasswordCodeEmailResImpl): ResetPasswordCodeEmailRes =
        ResetPasswordCodeEmailRes(token = from.token)
}

class VerifyCodeFromEmailMapper :
    ReqResMapper<ResetPasswordCodeEmailReq, ResetPasswordCodeEmailReqImpl, Unit, Unit> {

    override fun fromCommon(from: ResetPasswordCodeEmailReq): ResetPasswordCodeEmailReqImpl =
        ResetPasswordCodeEmailReqImpl(email = from.email, code = from.code)

    override fun toCommon(from: Unit): Unit = from
}

@Serializable
data class SetNewPasswordEmailReqImpl(
    val token: String,
    val password: String,
)

@Serializable
data class SetNewPasswordEmailResImpl(
    val token: String
)


class SetNewPasswordEmailMapper :
    ReqResMapper<SetNewPasswordEmailReq, SetNewPasswordEmailReqImpl, SetNewPasswordEmailResImpl, TokenResponse> {
    override fun fromCommon(from: SetNewPasswordEmailReq): SetNewPasswordEmailReqImpl =
        SetNewPasswordEmailReqImpl(password = from.newPass, token = from.token)

    override fun toCommon(from: SetNewPasswordEmailResImpl): TokenResponse =
        UserLoginRes(token = from.token)
}
