package com.common.feature.auth.impl.ui

import com.common.core.mviabstraction.Action
import com.common.feature.auth.api.domain.SplashCallbacks
import com.common.feature.auth.api.usecases.TokenUseCase
import com.common.feature.auth.api.ui.SplashStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch

class SplashStoreImpl(
    private val tokenUseCase: TokenUseCase,
    private val callbacks: SplashCallbacks,
) : SplashStore() {

    override fun dispatch(action: Action) {
        when (action) {
            LoadData -> launch {
                if (tokenUseCase.getOrLoadValue().first().isSuccess) {
                    callbacks.onSuccessAuth()
                } else {
                    callbacks.onFailedAuth()
                }
                emitSideEffect(ReadyToChangeScreen)
            }
        }
    }

}