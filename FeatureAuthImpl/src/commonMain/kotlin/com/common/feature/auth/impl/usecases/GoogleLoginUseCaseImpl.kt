package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.usecases.FirebaseAuthUseCase
import com.common.feature.auth.api.usecases.GoogleLoginUseCase

class GoogleLoginUseCaseImpl(
    private val firebaseAuthUseCase: FirebaseAuthUseCase
) : GoogleLoginUseCase {

    override suspend fun login(): Result<String> =
        firebaseAuthUseCase.execute(FirebaseAuthUseCase.FirebaseLoginType.GOOGLE)

}
