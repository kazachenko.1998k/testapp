package com.common.feature.auth.impl.data.rawmodels

import com.common.core.backend.ReqResMapper
import com.common.feature.auth.api.data.models.UserRegisterEmailReq
import com.common.feature.auth.api.data.models.UserRegisterRes
import kotlinx.serialization.Serializable

@Serializable
data class UserRegisterEmailReqImpl(val password: String, val email: String)

@Serializable
data class UserRegisterResImpl(val token: String)

class UserRegisterEmailMapper :
    ReqResMapper<UserRegisterEmailReq, UserRegisterEmailReqImpl, UserRegisterResImpl, UserRegisterRes> {
    override fun fromCommon(from: UserRegisterEmailReq): UserRegisterEmailReqImpl =
        UserRegisterEmailReqImpl(
            email = from.email,
            password = from.pass
        )

    override fun toCommon(from: UserRegisterResImpl): UserRegisterRes =
        UserRegisterRes(from.token)
}