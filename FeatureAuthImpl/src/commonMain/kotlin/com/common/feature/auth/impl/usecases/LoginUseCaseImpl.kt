package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.data.AuthRemoteApi
import com.common.feature.auth.api.data.models.Token
import com.common.feature.auth.api.data.models.TokenResponse
import com.common.feature.auth.api.data.models.UserLoginEmailReq
import com.common.feature.auth.api.data.models.UserLoginSocialReq
import com.common.feature.auth.api.usecases.LoginUseCase
import com.common.feature.auth.api.usecases.TokenUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LoginUseCaseImpl(
    private val remoteApi: AuthRemoteApi,
    private val tokenUseCase: TokenUseCase,
) : LoginUseCase {

    override suspend fun loginEmail(email: String, pass: String): Result<TokenResponse> {
        val result = remoteApi.loginEmail(
            UserLoginEmailReq(
                email = email,
                pass = pass
            )
        )
        return result.proceedResponse()
    }

    override suspend fun loginFacebook(token: String): Result<TokenResponse> =
        loginGoogle(token)

    override suspend fun loginGoogle(token: String): Result<TokenResponse> {
        val result = remoteApi.loginSocial(
            UserLoginSocialReq(token)
        )
        return result.proceedResponse()
    }

    override suspend fun hasLogin(): Boolean {
        val savedToken = tokenUseCase.updateValue()
        return savedToken.isSuccess
    }

    override suspend fun isLoggedIn(): Flow<Boolean> =
        tokenUseCase.getOrLoadValue().map { it.isSuccess }

    private suspend fun Result<TokenResponse>.proceedResponse(): Result<TokenResponse> {
        onSuccess {
            tokenUseCase.saveToken(Token(it.value))
        }
        return this
    }
}