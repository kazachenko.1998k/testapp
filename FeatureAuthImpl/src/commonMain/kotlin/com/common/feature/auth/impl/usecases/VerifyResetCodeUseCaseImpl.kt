package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.data.AuthRemoteApi
import com.common.feature.auth.api.data.models.ResetPasswordCodeEmailReq
import com.common.feature.auth.api.data.models.ResetPasswordCodeEmailRes
import com.common.feature.auth.api.usecases.VerifyResetCodeUseCase

class VerifyResetCodeUseCaseImpl(
    private val remoteApi: AuthRemoteApi,
) : VerifyResetCodeUseCase {

    override suspend fun verifyCodeForEmail(
        code: String,
        email: String
    ): Result<ResetPasswordCodeEmailRes> =
        remoteApi.codeForResetPassFromEmail(ResetPasswordCodeEmailReq(email = email, code = code))

}