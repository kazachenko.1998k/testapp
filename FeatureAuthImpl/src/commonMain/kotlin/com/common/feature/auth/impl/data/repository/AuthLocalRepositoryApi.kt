package com.common.feature.auth.impl.data.repository

import kotlinx.coroutines.flow.Flow

interface AuthLocalRepositoryApi {
    suspend fun saveToken(token: String)
    fun getToken(): Flow<String?>
    suspend fun acceptUserAgreementAndPrivacyPolicies()
    suspend fun hasAcceptUserAgreementAndPrivacyPolicies(): Flow<Boolean>
}
