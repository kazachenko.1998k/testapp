package com.common.feature.auth.impl.data.repository

import com.common.feature.auth.impl.data.rawmodels.*

interface AuthRemoteRepositoryApi {

    /**
     * Login by email and pass
     * require register
     */
    suspend fun loginEmail(reqImpl: UserLoginEmailReqImpl): Result<UserLoginResImpl>

    /**
     * Register by email and pass new account
     * require not exist email
     */
    suspend fun registerEmail(reqImpl: UserRegisterEmailReqImpl): Result<UserRegisterResImpl>

    /**
     * Send code to exist email for reset password
     * @see [codeForResetPassFromEmail]
     * @see [setNewPassword]
     */
    suspend fun sendCodeToEmailForResetPassword(reqImpl: ResendCodeEmailReqImpl): Result<Unit>

    /**
     * Get token for set new password. Need send code from email. Email was send with [sendCodeToEmailForResetPassword]
     */
    suspend fun codeForResetPassFromEmail(reqImpl: ResetPasswordCodeEmailReqImpl): Result<ResetPasswordCodeEmailResImpl>

    /**
     * Set email for user if not exist. Also send code to email
     */
    suspend fun sendCodeToEmailForSetNewEmail(reqImpl: SetNewEmailReqImpl): Result<Unit>

    /**
     * Validate code from email. Used after [sendCodeToEmailForSetNewEmail]
     */
    suspend fun verifyCodeFromEmailForSetNewEmail(reqImpl: ResetPasswordCodeEmailReqImpl): Result<Unit>

    /**
     * Login or register by social token
     */
    suspend fun loginSocial(reqImpl: UserLoginSocialReqImpl): Result<UserLoginSocialResImpl>

    /**
     * Set new password (with token from [codeForResetPassFromEmail])
     */
    suspend fun setNewPassword(reqImpl: SetNewPasswordEmailReqImpl): Result<SetNewPasswordEmailResImpl>
}