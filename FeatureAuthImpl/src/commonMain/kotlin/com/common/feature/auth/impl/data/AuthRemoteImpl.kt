package com.common.feature.auth.impl.data

import com.common.core.backend.request
import com.common.feature.auth.api.data.AuthRemoteApi
import com.common.feature.auth.api.data.models.*
import com.common.feature.auth.impl.data.rawmodels.*
import com.common.feature.auth.impl.data.repository.AuthRemoteRepositoryApi


class AuthRemoteImpl(private val remoteRepository: AuthRemoteRepositoryApi) : AuthRemoteApi {

    override suspend fun loginEmail(req: UserLoginEmailReq): Result<UserLoginRes> =
        request(UserLoginEmailMapper(), remoteRepository::loginEmail, req)

    override suspend fun loginSocial(req: UserLoginSocialReq): Result<UserLoginRes> =
        request(UserLoginSocialMapper(), remoteRepository::loginSocial, req)

    override suspend fun registerEmail(req: UserRegisterEmailReq): Result<UserRegisterRes> =
        request(UserRegisterEmailMapper(), remoteRepository::registerEmail, req)

    override suspend fun setNewEmail(req: SetNewEmailReq): Result<Unit> =
        request(SetNewEmailMapper(), remoteRepository::sendCodeToEmailForSetNewEmail, req)

    override suspend fun sendCodeToEmail(req: ResendCodeEmailReq): Result<Unit> =
        request(ResendCodeEmailMapper(), remoteRepository::sendCodeToEmailForResetPassword, req)

    override suspend fun codeForResetPassFromEmail(req: ResetPasswordCodeEmailReq): Result<ResetPasswordCodeEmailRes> =
        request(ResetPasswordCodeEmailMapper(), remoteRepository::codeForResetPassFromEmail, req)

    override suspend fun verifyCodeFromEmail(req: ResetPasswordCodeEmailReq): Result<Unit> =
        request(
            VerifyCodeFromEmailMapper(),
            remoteRepository::verifyCodeFromEmailForSetNewEmail,
            req
        )

    override suspend fun setNewPasswordFromEmail(req: SetNewPasswordEmailReq): Result<TokenResponse> =
        request(SetNewPasswordEmailMapper(), remoteRepository::setNewPassword, req)
}