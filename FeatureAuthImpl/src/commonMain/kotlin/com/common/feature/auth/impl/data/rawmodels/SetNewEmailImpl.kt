package com.common.feature.auth.impl.data.rawmodels

import com.common.core.backend.ReqResMapper
import com.common.feature.auth.api.data.models.SetNewEmailReq
import kotlinx.serialization.Serializable


@Serializable
data class SetNewEmailReqImpl(
    val email: String,
)

class SetNewEmailMapper :
    ReqResMapper<SetNewEmailReq, SetNewEmailReqImpl, Unit, Unit> {

    override fun fromCommon(from: SetNewEmailReq): SetNewEmailReqImpl =
        SetNewEmailReqImpl(email = from.email)

    override fun toCommon(from: Unit): Unit = from
}
