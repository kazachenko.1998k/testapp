package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.data.AuthLocalApi
import com.common.feature.auth.api.data.models.Token
import com.common.feature.auth.api.usecases.TokenUseCase
import com.common.utils.models.NoToken
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class TokenUseCaseImpl(private val localApi: AuthLocalApi) : TokenUseCase {

    private val tokenFlow = localApi.getToken().map {
        it?.let { Result.success(Token(it)) } ?: Result.failure(NoToken)
    }

    override suspend fun getOrLoadValue(): Flow<Result<Token>> = tokenFlow

    override suspend fun updateValue(): Result<Token> = getOrLoadValue().first()

    override suspend fun getTokenForRequest(): String = updateValue().getOrNull()?.value ?: ""

    override suspend fun saveToken(newToken: Token) {
        localApi.saveToken(newToken)
    }

}