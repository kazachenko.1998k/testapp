package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.data.AuthRemoteApi
import com.common.feature.auth.api.data.models.ResetPasswordCodeEmailReq
import com.common.feature.auth.api.usecases.VerifyCodeUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

class VerifyCodeUseCaseImpl(
    private val remoteApi: AuthRemoteApi,
) : VerifyCodeUseCase {

    private val _verifyCodeResults = MutableSharedFlow<Result<Unit>>()

    override val verifyCodeResults: Flow<Result<Unit>> = _verifyCodeResults

    override suspend fun verifyCodeForEmail(
        code: String,
        email: String
    ): Result<Unit> {
        val resp = remoteApi.verifyCodeFromEmail(
            ResetPasswordCodeEmailReq(
                email = email,
                code = code
            )
        )
        _verifyCodeResults.emit(resp)
        return resp
    }

}