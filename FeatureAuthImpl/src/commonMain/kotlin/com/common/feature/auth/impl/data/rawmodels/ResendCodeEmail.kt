package com.common.feature.auth.impl.data.rawmodels

import com.common.core.backend.ReqResMapper
import com.common.feature.auth.api.data.models.ResendCodeEmailReq
import kotlinx.serialization.Serializable


@Serializable
data class ResendCodeEmailReqImpl(
    val email: String,
)

class ResendCodeEmailMapper :
    ReqResMapper<ResendCodeEmailReq, ResendCodeEmailReqImpl, Unit, Unit> {

    override fun fromCommon(from: ResendCodeEmailReq): ResendCodeEmailReqImpl =
        ResendCodeEmailReqImpl(email = from.email)

    override fun toCommon(from: Unit): Unit = Unit
}
