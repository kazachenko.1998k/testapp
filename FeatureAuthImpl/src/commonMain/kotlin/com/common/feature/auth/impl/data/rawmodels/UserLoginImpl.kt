package com.common.feature.auth.impl.data.rawmodels

import com.common.core.backend.ReqResMapper
import com.common.feature.auth.api.data.models.UserLoginEmailReq
import com.common.feature.auth.api.data.models.UserLoginRes
import kotlinx.serialization.Serializable

@Serializable
data class UserLoginEmailReqImpl(val password: String, val email: String)

@Serializable
data class UserLoginResImpl(val token: String)

class UserLoginEmailMapper :
    ReqResMapper<UserLoginEmailReq, UserLoginEmailReqImpl, UserLoginResImpl, UserLoginRes> {
    override fun fromCommon(from: UserLoginEmailReq): UserLoginEmailReqImpl =
        UserLoginEmailReqImpl(
            email = from.email,
            password = from.pass
        )

    override fun toCommon(from: UserLoginResImpl): UserLoginRes = UserLoginRes(from.token)

}