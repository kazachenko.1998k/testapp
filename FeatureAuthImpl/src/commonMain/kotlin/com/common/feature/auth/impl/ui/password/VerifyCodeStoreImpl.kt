package com.common.feature.auth.impl.ui.password

import com.common.core.mviabstraction.ErrorEffect
import com.common.core.mviabstraction.usecases.BannerUseCase
import com.common.feature.auth.api.data.models.ResetPasswordCodeEmailRes
import com.common.feature.auth.api.domain.VerifyCodeCallbacks
import com.common.feature.auth.api.domain.VerifyCodeProvider
import com.common.feature.auth.api.ui.VerifyCodeStore
import com.common.feature.auth.api.usecases.*
import com.common.feature.auth.api.usecases.VerifyCodeTypeUseCase.VerifyCodeType.*
import com.common.utils.firstNotNull
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch

class VerifyCodeStoreImpl(
    private val resendCodeUseCase: ResendCodeUseCase,
    private val verifyCodeUseCase: VerifyCodeUseCase,
    private val verifyResetCodeUseCase: VerifyResetCodeUseCase,
    private val verifyCodeTypeUseCase: VerifyCodeTypeUseCase,
    private val setNewEmailUseCase: SetNewEmailUseCase,
    private val verifyCodeProvider: VerifyCodeProvider,
    private val callbacks: VerifyCodeCallbacks,
    snackBarUseCase: BannerUseCase,
    state: VerifyCodeState
) : VerifyCodeStore(state) {

    init {
        snackBarUseCase.subscribe(this)
        launch {
            verifyCodeProvider.verifyCodeEmail().filterNotNull().collect {
                updateState { copy(email = it) }
            }
        }
    }

    override fun dispatch(action: VerifyCodeAction) {
        when (action) {
            is OnShowScreen -> resetCode()
            is OnCodeModify -> launch {
                if (currentState().isLoading) return@launch
                val newCode = action.newCode.take(CODE_LENGTH_BY_DEFAULT).filter { it.isDigit() }
                updateState {
                    copy(code = newCode)
                }
                if (newCode.length == CODE_LENGTH_BY_DEFAULT) {
                    updateState { copy(isLoading = true) }
                    val resp = when (verifyCodeTypeUseCase.current.value) {
                        null -> throw IllegalArgumentException()
                        ResetPassword -> verifyResetCodeUseCase.verifyCodeForEmail(
                            code = currentState().code,
                            email = verifyCodeProvider.verifyCodeEmail().firstNotNull()
                        )
                        Email,
                        AddEmail -> verifyCodeUseCase.verifyCodeForEmail(
                            code = currentState().code,
                            email = verifyCodeProvider.verifyCodeEmail().firstNotNull()
                        )
                    }
                    resp.onSuccess {
                        if (it is ResetPasswordCodeEmailRes) {
                            verifyCodeProvider.setTokenFromResponseOnValidationCode(it.token)
                        }
                        callbacks.onSuccessValidateCode()
                    }
                    resp.onFailure {
                        emitSideEffect(ErrorEffect(it))
                    }
                    resetCode()
                    updateState { copy(isLoading = false) }
                }
            }
            is OnBackClick -> callbacks.onBackClick()
            is OnResendInstructionsClick -> launch {
                updateState { copy(isLoading = true) }
                val resp = when (verifyCodeTypeUseCase.current.value) {
                    null -> throw IllegalArgumentException()
                    ResetPassword -> resendCodeUseCase.resendCodeToEmail(currentState().email)
                    Email,
                    AddEmail -> setNewEmailUseCase.setNewEmail(currentState().email)
                }
                resp.onSuccess {
                    resetCode()
                }
                resp.onFailure {
                    emitSideEffect(ErrorEffect(it))
                }
                updateState { copy(isLoading = false) }
            }
        }
    }

    private fun resetCode() {
        updateState {
            copy(code = "")
        }
    }

}