package com.common.feature.auth.impl.usecases

import com.common.core.mviabstraction.usecases.AbstractValueUseCase
import com.common.feature.auth.api.usecases.VerifyCodeTypeUseCase

class VerifyCodeTypeUseCaseImpl : AbstractValueUseCase<VerifyCodeTypeUseCase.VerifyCodeType>(),
    VerifyCodeTypeUseCase