package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.data.AuthRemoteApi
import com.common.feature.auth.api.data.models.ResendCodeEmailReq
import com.common.feature.auth.api.usecases.ResendCodeUseCase

class ResendCodeUseCaseImpl(
    private val remoteApi: AuthRemoteApi,
) : ResendCodeUseCase {

    override suspend fun resendCodeToEmail(email: String): Result<Unit> =
        remoteApi.sendCodeToEmail(ResendCodeEmailReq(email))

}