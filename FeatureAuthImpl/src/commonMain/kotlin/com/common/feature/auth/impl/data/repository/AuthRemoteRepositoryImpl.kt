package com.common.feature.auth.impl.data.repository

import com.common.core.backend.CustomBuildConfig
import com.common.feature.auth.api.usecases.TokenUseCase
import com.common.feature.auth.impl.data.rawmodels.*
import com.common.utils.AUTHORIZATION_TOKEN_KEY
import com.common.utils.headers
import com.common.utils.requestWrapper
import io.ktor.client.*
import io.ktor.client.request.*

class AuthRemoteRepositoryImpl(
    private val client: HttpClient,
    private val tokenUseCase: TokenUseCase,
) : AuthRemoteRepositoryApi {

    override suspend fun loginEmail(reqImpl: UserLoginEmailReqImpl): Result<UserLoginResImpl> =
        requestWrapper {
            client.post("$url/auth/login") {
                body = reqImpl
                headers {}
            }
        }

    override suspend fun loginSocial(reqImpl: UserLoginSocialReqImpl): Result<UserLoginSocialResImpl> =
        requestWrapper {
            client.post("$url/auth/social") {
                body = reqImpl
                headers {}
            }
        }


    override suspend fun registerEmail(reqImpl: UserRegisterEmailReqImpl): Result<UserRegisterResImpl> =
        requestWrapper {
            client.post("$url/auth/register") {
                body = reqImpl
                headers {}
            }
        }

    override suspend fun sendCodeToEmailForResetPassword(reqImpl: ResendCodeEmailReqImpl): Result<Unit> =
        requestWrapper {
            client.post("$url/auth/reset") {
                body = reqImpl
                headers {}
            }
        }

    override suspend fun verifyCodeFromEmailForSetNewEmail(reqImpl: ResetPasswordCodeEmailReqImpl): Result<Unit> =
        requestWrapper {
            client.post("$url/auth/verify/code") {
                body = reqImpl
                headers {
                    append(AUTHORIZATION_TOKEN_KEY, tokenUseCase.getTokenForRequest())
                }
            }
        }

    override suspend fun sendCodeToEmailForSetNewEmail(reqImpl: SetNewEmailReqImpl): Result<Unit> =
        requestWrapper {
            client.post("$url/auth/email") {
                body = reqImpl
                headers {
                    append(AUTHORIZATION_TOKEN_KEY, tokenUseCase.getTokenForRequest())
                }
            }
        }

    override suspend fun codeForResetPassFromEmail(reqImpl: ResetPasswordCodeEmailReqImpl): Result<ResetPasswordCodeEmailResImpl> =
        requestWrapper {
            client.post("$url/auth/code") {
                body = reqImpl
                headers {}
            }
        }

    override suspend fun setNewPassword(reqImpl: SetNewPasswordEmailReqImpl): Result<SetNewPasswordEmailResImpl> =
        requestWrapper {
            client.post("$url/auth/password") {
                body = reqImpl
                headers {
                    append("x-pswd-reset-token", reqImpl.token)
                }
            }
        }

    companion object {
        val url = CustomBuildConfig.OUR_SIDE_URL
    }

}