package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.data.AuthRemoteApi
import com.common.feature.auth.api.data.models.Token
import com.common.feature.auth.api.data.models.TokenResponse
import com.common.feature.auth.api.data.models.UserRegisterEmailReq
import com.common.feature.auth.api.usecases.RegisterUseCase
import com.common.feature.auth.api.usecases.TokenUseCase

class RegisterUseCaseImpl(
    private val remoteApi: AuthRemoteApi,
    private val tokenUseCase: TokenUseCase,
) : RegisterUseCase {
    override suspend fun registerEmail(email: String, pass: String): Result<TokenResponse> {
        val result = remoteApi.registerEmail(
            UserRegisterEmailReq(
                email = email,
                pass = pass
            )
        )
        if (result.isSuccess) {
            val token = Token(result.getOrThrow().value)
            tokenUseCase.saveToken(token)
        }
        return result
    }

}