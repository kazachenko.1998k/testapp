package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.data.AuthLocalApi
import com.common.feature.auth.api.usecases.UserAgreementAndPrivacyPoliciesUseCase
import kotlinx.coroutines.flow.first

class UserAgreementAndPrivacyPoliciesUseCaseImpl(
    private val localApi: AuthLocalApi,
) : UserAgreementAndPrivacyPoliciesUseCase {

    override suspend fun accept(): Result<Unit> {
        localApi.acceptUserAgreementAndPrivacyPolicies()
        return Result.success(Unit)
    }

    override suspend fun hasAccept(): Boolean =
        localApi.hasAcceptUserAgreementAndPrivacyPolicies().first()

}