package com.common.feature.auth.impl.usecases

import com.common.feature.auth.api.usecases.FacebookLoginUseCase
import com.common.feature.auth.api.usecases.FirebaseAuthUseCase

class FacebookLoginUseCaseImpl(
    private val firebaseAuthUseCase: FirebaseAuthUseCase
) : FacebookLoginUseCase {

    override suspend fun login(): Result<String> =
        firebaseAuthUseCase.execute(FirebaseAuthUseCase.FirebaseLoginType.FACEBOOK)

}