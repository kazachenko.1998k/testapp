package com.common.feature.auth.impl.data.rawmodels

import com.common.core.backend.ReqResMapper
import com.common.feature.auth.api.data.models.UserLoginRes
import com.common.feature.auth.api.data.models.UserLoginSocialReq
import kotlinx.serialization.Serializable

@Serializable
data class UserLoginSocialReqImpl(val token: String)

@Serializable
data class UserLoginSocialResImpl(val token: String)

class UserLoginSocialMapper :
    ReqResMapper<UserLoginSocialReq, UserLoginSocialReqImpl, UserLoginSocialResImpl, UserLoginRes> {
    override fun fromCommon(from: UserLoginSocialReq): UserLoginSocialReqImpl =
        UserLoginSocialReqImpl(token = from.token)

    override fun toCommon(from: UserLoginSocialResImpl): UserLoginRes = UserLoginRes(from.token)

}