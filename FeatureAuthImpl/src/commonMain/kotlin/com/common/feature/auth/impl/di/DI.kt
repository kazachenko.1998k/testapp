package com.common.feature.auth.impl.di

import com.common.feature.auth.api.data.AuthLocalApi
import com.common.feature.auth.api.data.AuthRemoteApi
import com.common.feature.auth.api.domain.VerifyCodeProvider
import com.common.feature.auth.api.ui.*
import com.common.feature.auth.api.usecases.*
import com.common.feature.auth.impl.data.AuthLocalImpl
import com.common.feature.auth.impl.data.AuthRemoteImpl
import com.common.feature.auth.impl.data.repository.AuthLocalRepositoryApi
import com.common.feature.auth.impl.data.repository.AuthLocalRepositoryImpl
import com.common.feature.auth.impl.data.repository.AuthRemoteRepositoryApi
import com.common.feature.auth.impl.data.repository.AuthRemoteRepositoryImpl
import com.common.feature.auth.impl.domain.VerifyCodeProviderImpl
import com.common.feature.auth.impl.ui.HaveAccountQuestionStoreImpl
import com.common.feature.auth.impl.ui.LoginStoreImpl
import com.common.feature.auth.impl.ui.RegisterStoreImpl
import com.common.feature.auth.impl.ui.SplashStoreImpl
import com.common.feature.auth.impl.ui.password.ResetPasswordStoreImpl
import com.common.feature.auth.impl.ui.password.VerifyCodeStoreImpl
import com.common.feature.auth.impl.usecases.*
import org.koin.dsl.bind
import org.koin.dsl.module

val DI = module {
    //data
    single {
        AuthLocalImpl(get())
    } bind AuthLocalApi::class
    single {
        AuthRemoteImpl(get())
    } bind AuthRemoteApi::class
    single {
        AuthLocalRepositoryImpl(get())
    } bind AuthLocalRepositoryApi::class
    single {
        AuthRemoteRepositoryImpl(get(), get())
    } bind AuthRemoteRepositoryApi::class

    //domain
    single {
        TokenUseCaseImpl(get())
    } bind TokenUseCase::class
    single {
        VerifyCodeProviderImpl()
    } bind VerifyCodeProvider::class

    //usecases
    single {
        LoginUseCaseImpl(get(), get())
    } bind LoginUseCase::class
    single {
        ResetPasswordUseCaseImpl(get())
    } bind ResetPasswordUseCase::class
    single {
        VerifyCodeUseCaseImpl(get())
    } bind VerifyCodeUseCase::class
    single {
        VerifyResetCodeUseCaseImpl(get())
    } bind VerifyResetCodeUseCase::class
    single {
        RegisterUseCaseImpl(get(), get())
    } bind RegisterUseCase::class
    single {
        ResendCodeUseCaseImpl(get())
    } bind ResendCodeUseCase::class
    single {
        UserAgreementAndPrivacyPoliciesUseCaseImpl(get())
    } bind UserAgreementAndPrivacyPoliciesUseCase::class
    single {
        VerifyCodeTypeUseCaseImpl()
    } bind VerifyCodeTypeUseCase::class
    single {
        FacebookLoginUseCaseImpl(get())
    } bind FacebookLoginUseCase::class
    single {
        GoogleLoginUseCaseImpl(get())
    } bind GoogleLoginUseCase::class
    factory {
        PassValidationUseCaseImpl()
    } bind PassValidationUseCase::class

    //ui
    single {
        ResetPasswordStoreImpl(
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            ResetPasswordStore.ResetPasswordState.defaultInstance()
        )
    } bind ResetPasswordStore::class
    single {
        VerifyCodeStoreImpl(
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            VerifyCodeStore.VerifyCodeState.defaultInstance()
        )
    } bind VerifyCodeStore::class
    single {
        SplashStoreImpl(
            get(),
            get(),
        )
    } bind SplashStore::class
    single {
        LoginStoreImpl(
            get(),
            get(),
            get(),
            get(),
            get(),
            LoginStore.LoginState.defaultInstance()
        )
    } bind LoginStore::class
    single {
        HaveAccountQuestionStoreImpl(
            get(),
            HaveAccountQuestionStore.HaveAccountQuestionState.defaultInstance()
        )
    } bind HaveAccountQuestionStore::class
    single {
        RegisterStoreImpl(
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            get(),
            RegisterStore.RegisterState.defaultInstance()
        )
    } bind RegisterStore::class
}