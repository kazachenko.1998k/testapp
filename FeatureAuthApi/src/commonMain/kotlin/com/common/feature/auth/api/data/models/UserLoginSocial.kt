package com.common.feature.auth.api.data.models

/**
 * @property token from google/facebook auth
 */
data class UserLoginSocialReq(
    val token: String,
) : LoginReq
