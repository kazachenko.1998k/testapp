package com.common.feature.auth.api.domain

interface HaveAccountQuestionCallbacks {
    fun onLoginClick()
    fun onRegisterClick()
}