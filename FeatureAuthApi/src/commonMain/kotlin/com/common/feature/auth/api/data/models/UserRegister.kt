package com.common.feature.auth.api.data.models

/**
 * Common interface for all register type
 */
interface RegisterReq

/**
 * Equals [UserLoginEmailReq]
 * @property email will be formatted as abc@mail.com
 * @property pass will be include number and TopCase chars and LoverCase chars
 * @suppress copyright for single responsibility
 */
data class UserRegisterEmailReq(
    val email: String,
    val pass: String,
) : RegisterReq

/**
 * Do not supply backend. Will be added in future
 */
data class UserRegisterPhoneReq(
    val phone: String,
) : RegisterReq

/**
 * Equals [UserLoginReq]
 * @suppress copyright for single responsibility
 */
data class UserRegisterRes(
    val token: String
) : TokenResponse(token)