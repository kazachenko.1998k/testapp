package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.ValueUseCase

/**
 * Verify code by email
 */
interface VerifyCodeTypeUseCase : ValueUseCase<VerifyCodeTypeUseCase.VerifyCodeType> {
    enum class VerifyCodeType {
        Email,
        ResetPassword,
        AddEmail,
    }
}