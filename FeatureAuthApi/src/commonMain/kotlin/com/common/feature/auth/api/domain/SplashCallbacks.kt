package com.common.feature.auth.api.domain

interface SplashCallbacks {
    fun onSuccessAuth()
    fun onFailedAuth()
}