package com.common.feature.auth.api.data.models


/**
 * Common interface for all Login type
 */
interface LoginReq

/**
 * Equals [UserRegisterEmailReq]
 * @property email will be formatted as abc@mail.com
 * @property pass will be include number and TopCase chars and LoverCase chars
 * @suppress copyright for single responsibility
 */
data class UserLoginEmailReq(
    val email: String,
    val pass: String,
) : LoginReq

/**
 * Do not supply backend. Will be added in future
 */
data class UserLoginPhoneReq(
    val phone: String,
) : LoginReq

data class UserLoginRes(
    val token: String
) : TokenResponse(token)