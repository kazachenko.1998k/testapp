package com.common.feature.auth.api.ui

import com.common.core.mviabstraction.Action
import com.common.core.mviabstraction.Effect
import com.common.core.mviabstraction.State
import com.common.core.mviabstraction.StoreAbstract
import com.common.utils.models.FieldEditable

/**
 * Store for reset password screen. Write email part.
 */
abstract class ResetPasswordStore(state: ResetPasswordState) :
    StoreAbstract<ResetPasswordStore.ResetPasswordState, ResetPasswordStore.ResetPasswordStoreAction, Effect>(
        state
    ) {

    sealed interface ResetPasswordStoreAction : Action

    /**
     * Action on click by "Send instructions" when email not empty
     */
    object OnSendInstructionsClick : ResetPasswordStoreAction

    /**
     * Action on modify current email
     */
    class OnEmailModify(val newText: String) : ResetPasswordStoreAction

    /**
     * Action on end modify current email
     */
    object OnEmailModifyEnd : ResetPasswordStoreAction

    /**
     * Action on click back navigation icon
     */
    object OnBackClick : ResetPasswordStoreAction

    /**
     * @property isLoading loading state
     * @property email see - [RegisterStore.RegisterState.email]
     */
    data class ResetPasswordState(
        val isLoading: Boolean,
        val email: FieldEditable,
        val sendBtnEnabled: Boolean,
    ) : State {
        companion object {
            fun defaultInstance() = ResetPasswordState(
                isLoading = false,
                email = FieldEditable.defaultInstance(),
                sendBtnEnabled = false,
            )
        }
    }
}