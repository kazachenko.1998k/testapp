package com.common.feature.auth.api.data.models

/**
 * @property email will be formatted as abc@mail.com
 */
data class SetNewEmailReq(
    val email: String,
)
