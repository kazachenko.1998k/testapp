package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase
import com.common.feature.auth.api.data.models.PasswordState
import kotlinx.coroutines.flow.Flow

interface PassValidationUseCase : BaseUseCase {

    val pass: Flow<PasswordState>
    val passHasError: Boolean

    fun onPassModify(newText: String)
    fun onPassChangeVisible()
    fun onPassModifyEnd()
    fun onPassModifyStart()
    fun reset()

}