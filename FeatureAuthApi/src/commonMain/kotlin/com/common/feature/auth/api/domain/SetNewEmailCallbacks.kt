package com.common.feature.auth.api.domain

interface SetNewEmailCallbacks {
    fun onSuccessSetEmail()
    fun onBackClick()
}