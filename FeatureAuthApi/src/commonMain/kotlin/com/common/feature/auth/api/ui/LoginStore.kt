package com.common.feature.auth.api.ui

import com.common.core.mviabstraction.Action
import com.common.core.mviabstraction.Effect
import com.common.core.mviabstraction.State
import com.common.core.mviabstraction.StoreAbstract
import com.common.feature.auth.api.data.models.SocialLogin
import com.common.utils.models.FieldEditable

/**
 * Store for login in app
 */
abstract class LoginStore(state: LoginState) :
    StoreAbstract<LoginStore.LoginState, LoginStore.LoginStoreAction, Effect>(state) {

    sealed interface LoginStoreAction : Action

    /**
     * If user success login navigate to robot screen else nothing
     */
    object OnLoginClick : LoginStoreAction

    /**
     * Action on modify current email
     */
    class OnEmailModify(val newText: String) : LoginStoreAction

    /**
     * Login by google
     */
    object OnGoogleClick : LoginStoreAction

    /**
     * Login by facebook
     */
    object OnFacebookClick : LoginStoreAction

    /**
     * Action on modify current pass
     */
    class OnPassModify(val newText: String) : LoginStoreAction

    /**
     * Action on click "eye" in pass field
     */
    object OnPassChangeVisible : LoginStoreAction

    /**
     * Action on click "Forgot password?" under password field
     */
    object OnForgotPasswordClick : LoginStoreAction

    /**
     * Action on click "Sign up"
     */
    object OnRegisterClick : LoginStoreAction

    /**
     * Action on click back navigation icon
     */
    object OnBackClick : LoginStoreAction


    /**
     * @property isLoading loading state
     * @property email see - [RegisterStore.RegisterState.email]
     * @property pass see - [RegisterStore.RegisterState.pass]
     */
    data class LoginState(
        val isLoading: Boolean,
        val loginBtnEnabled: Boolean,
        val socialLoginState: SocialLogin?,
        val email: FieldEditable,
        val pass: FieldEditable,
    ) : State {
        companion object {
            fun defaultInstance() = LoginState(
                isLoading = false,
                loginBtnEnabled = false,
                socialLoginState = null,
                email = FieldEditable.defaultInstance(),
                pass = FieldEditable.defaultInstance(),
            )
        }
    }
}