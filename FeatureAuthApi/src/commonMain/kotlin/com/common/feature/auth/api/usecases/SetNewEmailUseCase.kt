package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase

/**
 * Send new email useCase (for facebook user)
 */
interface SetNewEmailUseCase : BaseUseCase {

    /**
     * Add email to exists user
     */
    suspend fun setNewEmail(email: String): Result<Unit>

}