package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase

/**
 * Send new pass to email useCase
 */
interface ResetPasswordUseCase : BaseUseCase {

    /**
     * Send code to exist email for reset password
     */
    suspend fun resetPasswordEmail(email: String): Result<Unit>

}