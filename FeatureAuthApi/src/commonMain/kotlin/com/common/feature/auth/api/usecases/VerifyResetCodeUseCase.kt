package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase
import com.common.feature.auth.api.data.models.ResetPasswordCodeEmailRes

/**
 * Verify code by email for reset password
 */
interface VerifyResetCodeUseCase : BaseUseCase {

    /**
     * Verify code by email
     */
    suspend fun verifyCodeForEmail(code: String, email: String): Result<ResetPasswordCodeEmailRes>

}