package com.common.feature.auth.api.data.models

import com.common.utils.models.FieldEditable

data class PasswordState(
    val passHasMinimum8Characters: ErrorCheckState,
    val passHasOneNumber: ErrorCheckState,
    val passTrust: ErrorCheckState,
    val helperPassCheckersIsVisible: Boolean,
    val field: FieldEditable,
) {
    companion object {
        fun defaultInstance() = PasswordState(
            passHasMinimum8Characters = ErrorCheckState.NOT_CHECK,
            passHasOneNumber = ErrorCheckState.NOT_CHECK,
            passTrust = ErrorCheckState.NOT_CHECK,
            helperPassCheckersIsVisible = false,
            field = FieldEditable.defaultInstance(),
        )
    }
}