package com.common.feature.auth.api.domain

interface VerifyCodeCallbacks {
    fun onSuccessValidateCode()
    fun onBackClick()
}