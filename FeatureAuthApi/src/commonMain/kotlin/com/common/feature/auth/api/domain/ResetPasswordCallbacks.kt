package com.common.feature.auth.api.domain

interface ResetPasswordCallbacks {
    fun onSuccessResetPassword()
    fun onBackClick()
}