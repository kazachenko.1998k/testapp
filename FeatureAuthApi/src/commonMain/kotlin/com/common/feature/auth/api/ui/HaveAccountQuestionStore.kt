package com.common.feature.auth.api.ui

import com.common.core.mviabstraction.Action
import com.common.core.mviabstraction.Effect
import com.common.core.mviabstraction.State
import com.common.core.mviabstraction.StoreAbstract

/**
 * Store for select has user account ot not
 */
abstract class HaveAccountQuestionStore(state: HaveAccountQuestionState) :
    StoreAbstract<
            HaveAccountQuestionStore.HaveAccountQuestionState,
            HaveAccountQuestionStore.HaveAccountQuestionStoreAction,
            Effect>(state) {

    sealed interface HaveAccountQuestionStoreAction : Action

    /**
     * Action if user don't have account
     */
    object OnGetStartedClick : HaveAccountQuestionStoreAction

    /**
     * Action if user have account
     */
    object OnLoginClick : HaveAccountQuestionStoreAction

    /**
     * @property isLoading loading state
     */
    data class HaveAccountQuestionState(
        val isLoading: Boolean,
    ) : State {
        companion object {
            fun defaultInstance() = HaveAccountQuestionState(
                isLoading = false,
            )
        }
    }
}