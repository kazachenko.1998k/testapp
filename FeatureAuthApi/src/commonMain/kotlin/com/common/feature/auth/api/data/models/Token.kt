package com.common.feature.auth.api.data.models

data class Token(
    val value: String
)

abstract class TokenResponse(
    val value: String
)