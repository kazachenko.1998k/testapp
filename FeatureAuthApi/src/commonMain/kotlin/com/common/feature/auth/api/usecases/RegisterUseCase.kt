package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase
import com.common.feature.auth.api.data.models.TokenResponse

/**
 * Register user useCase
 */
interface RegisterUseCase : BaseUseCase {

    /**
     * Register new user by email and pass
     */
    suspend fun registerEmail(email: String, pass: String): Result<TokenResponse>

}