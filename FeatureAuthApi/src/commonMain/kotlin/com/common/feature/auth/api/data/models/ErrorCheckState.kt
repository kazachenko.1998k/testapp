package com.common.feature.auth.api.data.models


/**
 * State for elements in down list on register screen
 */
enum class ErrorCheckState {
    /**
     * Grey state. User now edit field
     */
    NOT_CHECK,

    /**
     * Green state. Success validation
     */
    SUCCESS,

    /**
     * Red state. Bad validation
     */
    ERROR
}
