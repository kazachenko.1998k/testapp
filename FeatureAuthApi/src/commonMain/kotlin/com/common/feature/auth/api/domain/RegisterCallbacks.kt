package com.common.feature.auth.api.domain

interface RegisterCallbacks {
    fun onSuccessRegister()

    fun onLoginClick()

    fun onBackClick()

    fun onPrivacyPolicies(link: String)

    fun onTermsAndConditions(link: String)

}