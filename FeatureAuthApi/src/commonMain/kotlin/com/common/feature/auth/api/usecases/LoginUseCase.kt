package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase
import com.common.feature.auth.api.data.models.TokenResponse
import kotlinx.coroutines.flow.Flow

/**
 * Management login state
 */
interface LoginUseCase : BaseUseCase {
    /**
     * Send request on login by email
     */
    suspend fun loginEmail(email: String, pass: String): Result<TokenResponse>

    /**
     * Send request on login by Facebook Token
     */
    suspend fun loginFacebook(token: String): Result<TokenResponse>

    /**
     * Send request on login by google Token
     */
    suspend fun loginGoogle(token: String): Result<TokenResponse>

    /**
     * Check user now authorised or not
     */
    suspend fun hasLogin(): Boolean

    /**
     * User logged state
     */
    suspend fun isLoggedIn(): Flow<Boolean>



}