package com.common.feature.auth.api.data

import com.common.core.backend.RemoteApi
import com.common.feature.auth.api.data.models.*

interface AuthRemoteApi : RemoteApi {

    /**
     * Login by email and pass
     * require register
     */
    suspend fun loginEmail(req: UserLoginEmailReq): Result<TokenResponse>

    /**
     * Register by email and pass new account
     * require not exist email
     */
    suspend fun registerEmail(req: UserRegisterEmailReq): Result<TokenResponse>

    /**
     * Send code to exist email for reset password
     * @see [codeForResetPassFromEmail]
     * @see [setNewPasswordFromEmail]
     */
    suspend fun sendCodeToEmail(req: ResendCodeEmailReq): Result<Unit>

    /**
     * Get token for set new password. Need send code from email. Email was send with [sendCodeToEmail]
     */
    suspend fun codeForResetPassFromEmail(req: ResetPasswordCodeEmailReq): Result<ResetPasswordCodeEmailRes>

    /**
     * Set email for user if not exist. Also send code to email
     */
    suspend fun setNewEmail(req: SetNewEmailReq): Result<Unit>

    /**
     * Validate code from email. Used after [setNewEmail]
     */
    suspend fun verifyCodeFromEmail(req: ResetPasswordCodeEmailReq): Result<Unit>

    /**
     * Login or register by social token
     */
    suspend fun loginSocial(req: UserLoginSocialReq): Result<TokenResponse>

    /**
     * Set new password (with token from [codeForResetPassFromEmail])
     */
    suspend fun setNewPasswordFromEmail(req: SetNewPasswordEmailReq): Result<TokenResponse>
}