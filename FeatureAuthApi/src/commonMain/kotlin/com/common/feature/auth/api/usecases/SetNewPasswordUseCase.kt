package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase
import com.common.feature.auth.api.data.models.TokenResponse

/**
 * Update pass for user
 */
interface SetNewPasswordUseCase : BaseUseCase {

    /**
     * Register new user by email and pass
     */
    suspend fun setNewPasswordEmailCode(token: String, newPass: String): Result<TokenResponse>

}