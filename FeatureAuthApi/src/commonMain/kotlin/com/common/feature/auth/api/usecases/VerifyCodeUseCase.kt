package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase
import kotlinx.coroutines.flow.Flow

/**
 * Verify code by email
 */
interface VerifyCodeUseCase : BaseUseCase {

    val verifyCodeResults: Flow<Result<Unit>>

    /**
     * Verify code by email
     */
    suspend fun verifyCodeForEmail(code: String, email: String): Result<Unit>

}