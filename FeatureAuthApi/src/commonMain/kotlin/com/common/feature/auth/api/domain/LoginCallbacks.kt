package com.common.feature.auth.api.domain

interface LoginCallbacks {
    fun onSuccessLogin()
    fun onRegisterClick()
    fun onBackClick()
    fun onForgotPasswordClick()
}