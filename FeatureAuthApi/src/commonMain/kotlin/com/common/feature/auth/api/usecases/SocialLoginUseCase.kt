package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase

interface SocialLoginUseCase : BaseUseCase {

    suspend fun login(): Result<String>

}