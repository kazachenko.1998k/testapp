package com.common.feature.auth.api.ui

import com.common.core.mviabstraction.Action
import com.common.core.mviabstraction.Effect
import com.common.core.mviabstraction.State
import com.common.core.mviabstraction.StoreAbstract
import com.common.feature.auth.api.data.models.PasswordState
import com.common.feature.auth.api.data.models.SocialLogin
import com.common.utils.models.FieldEditable

/**
 * Store for register in app
 */
abstract class RegisterStore(state: RegisterState) :
    StoreAbstract<
            RegisterStore.RegisterState,
            RegisterStore.RegisterStoreAction,
            Effect
            >(state) {

    sealed interface RegisterStoreAction : Action

    /**
     * Action on draw screen
     */
    object OnShowScreen : RegisterStoreAction

    /**
     * Try register new user
     */
    object OnRegisterClick : RegisterStoreAction

    /**
     * Action on modify current email
     */
    class OnEmailModify(val newText: String) : RegisterStoreAction
    object OnEmailModifyEnd : RegisterStoreAction

    /**
     * Action on modify current pass
     */
    class OnPassModify(val newText: String) : RegisterStoreAction
    object OnPassModifyStart : RegisterStoreAction
    object OnPassModifyEnd : RegisterStoreAction

    /**
     * Action on click "eye" in pass field
     */
    object OnPassChangeVisible : RegisterStoreAction

    /**
     * Action on click "Login"
     */
    object OnLoginClick : RegisterStoreAction

    /**
     * Action on click back navigation icon
     */
    object OnBackClick : RegisterStoreAction

    /**
     * Login by google
     */
    object OnGoogleClick : RegisterStoreAction

    /**
     * Login by facebook
     */
    object OnFacebookClick : RegisterStoreAction

    /**
     * Actions for BottomSheet view
     */
    sealed interface RegisterStoreBottomSheetAction : RegisterStoreAction

    /**
     * User agreement
     */
    object OnTermsAndConditionsClick : RegisterStoreBottomSheetAction

    /**
     * Privacy Policies
     */
    object OnPrivacyPolicyClick : RegisterStoreBottomSheetAction

    /**
     * Accept and Continue
     */
    object OnAcceptAndContinueClick : RegisterStoreBottomSheetAction

    /**
     * @property isLoading loading state
     * @property email example: janedoe@common.com
     * @property pass required: Minimum 8 characters, One uppercase character, One number
     */
    data class RegisterState(
        val isLoading: Boolean,
        val registerBtnEnabled: Boolean,
        val socialRegisterState: SocialLogin?,
        val email: FieldEditable,
        val pass: PasswordState,
    ) : State {
        companion object {
            fun defaultInstance() = RegisterState(
                isLoading = false,
                registerBtnEnabled = false,
                socialRegisterState = null,
                email = FieldEditable.defaultInstance(),
                pass = PasswordState.defaultInstance(),
            )
        }
    }
}