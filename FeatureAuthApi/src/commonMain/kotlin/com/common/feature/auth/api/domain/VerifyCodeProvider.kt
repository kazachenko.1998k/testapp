package com.common.feature.auth.api.domain

import kotlinx.coroutines.flow.Flow

/**
 * Provider for Reset password flow
 */
interface VerifyCodeProvider {

    /**
     * Email for verify code
     */
    fun verifyCodeEmail(): Flow<String?>

    /**
     * Update Email for verify code
     */
    fun setEmailForVerifyCode(newEmail: String)

    /**
     * Token for verify code
     */
    fun tokenFromResponseOnValidationCode(): Flow<String?>

    /**
     * Update Token for verify code
     */
    fun setTokenFromResponseOnValidationCode(newToken: String)

}