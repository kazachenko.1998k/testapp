package com.common.feature.auth.api.data.models

/**
 * @property email will be formatted as abc@mail.com
 */
data class ResetPasswordEmailReq(
    val email: String,
)

data class ResendCodeEmailReq(
    val email: String,
)

data class ResetPasswordCodeEmailReq(
    val code: String,
    val email: String,
)

data class SetNewPasswordEmailReq(
    val token: String,
    val newPass: String,
)

class ResetPasswordEmailRes

class ResetPasswordCodeEmailRes(
    val token: String
)

class SetNewPasswordEmailRes(
    val token: String
)