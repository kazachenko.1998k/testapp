package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase

/**
 * Resend new code to email
 */
interface ResendCodeUseCase : BaseUseCase {

    /**
     * Send verification code to email
     */
    suspend fun resendCodeToEmail(email: String): Result<Unit>

}