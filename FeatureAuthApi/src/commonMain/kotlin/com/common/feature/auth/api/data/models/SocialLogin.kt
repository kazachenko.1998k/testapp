package com.common.feature.auth.api.data.models

enum class SocialLogin {
    Facebook,
    Google
}
