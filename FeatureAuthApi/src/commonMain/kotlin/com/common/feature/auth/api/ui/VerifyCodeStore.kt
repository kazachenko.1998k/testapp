package com.common.feature.auth.api.ui

import com.common.core.mviabstraction.Action
import com.common.core.mviabstraction.Effect
import com.common.core.mviabstraction.State
import com.common.core.mviabstraction.StoreAbstract

/**
 * Store for reset password screen. Write email part.
 */
abstract class VerifyCodeStore(state: VerifyCodeState) :
    StoreAbstract<VerifyCodeStore.VerifyCodeState, VerifyCodeStore.VerifyCodeAction, Effect>(
        state
    ) {

    sealed interface VerifyCodeAction : Action

    /**
     * Action on click by "Didn’t get your email? Resend" when user don't get email
     */
    object OnResendInstructionsClick : VerifyCodeAction

    /**
     * Action on modify current code
     */
    class OnCodeModify(val newCode: String) : VerifyCodeAction

    /**
     * Action on show screen
     */
    object OnShowScreen : VerifyCodeAction

    /**
     * Action on click back navigation icon
     */
    object OnBackClick : VerifyCodeAction

    /**
     * @property isLoading Loading state
     * @property code An arbitrary length code (should be [CODE_LENGTH_BY_DEFAULT] by default). if null then the field is
     * empty, otherwise it must be a number. The code is ready to be sent only if all characters exist.
     * If the code does not fit, then all fields must be cleared
     * (you do not need to re-request the password)
     * @property email User email from previous step (see [ResetPasswordStore])
     */
    data class VerifyCodeState(
        val isLoading: Boolean,
        val email: String,
        val code: String,
    ) : State {
        companion object {
            fun defaultInstance() = VerifyCodeState(
                isLoading = false,
                email = "",
                code = ""
            )
        }
    }

    companion object {
        const val CODE_LENGTH_BY_DEFAULT = 5
    }
}