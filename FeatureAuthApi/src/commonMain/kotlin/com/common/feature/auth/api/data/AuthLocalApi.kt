package com.common.feature.auth.api.data

import com.common.core.preferences.api.LocalApi
import com.common.feature.auth.api.data.models.Token
import kotlinx.coroutines.flow.Flow

interface AuthLocalApi : LocalApi {
    suspend fun saveToken(token: Token)
    fun getToken(): Flow<String?>
    suspend fun acceptUserAgreementAndPrivacyPolicies()
    suspend fun hasAcceptUserAgreementAndPrivacyPolicies(): Flow<Boolean>
}