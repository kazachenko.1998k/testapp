package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.BaseUseCase

/**
 * Management state Accept User Agreement and Privacy Policies
 */
interface UserAgreementAndPrivacyPoliciesUseCase : BaseUseCase {

    /**
     * Accept User Agreement and Privacy Policies
     * @suppress set to shared preferences [true]
     */
    suspend fun accept(): Result<Unit>

    /**
     * @return [true] if User Agreement and Privacy Policies was accepted else [false]
     * @suppress value from shared preferences by default false
     */
    suspend fun hasAccept(): Boolean
}