package com.common.feature.auth.api.ui

import com.common.core.mviabstraction.Action
import com.common.core.mviabstraction.Effect
import com.common.core.mviabstraction.State
import com.common.core.mviabstraction.StoreAbstract

/**
 * Store for navigate to login if exist token.
 */
abstract class SplashStore :
    StoreAbstract<SplashStore.SplashState, Action, Effect>(SplashState.defaultInstance()) {

    object LoadData : Action

    object ReadyToChangeScreen : Effect

    class SplashState : State {
        companion object {
            fun defaultInstance() = SplashState()
        }
    }
}