package com.common.feature.auth.api.usecases

import com.common.core.mviabstraction.usecases.GetOrLoadUseCase
import com.common.feature.auth.api.data.models.Token

interface TokenUseCase : GetOrLoadUseCase<Token> {
    suspend fun getTokenForRequest(): String
    suspend fun saveToken(newToken: Token)
}