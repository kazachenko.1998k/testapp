package com.common.feature.auth.api.usecases

interface FirebaseAuthUseCase {

    suspend fun execute(type: FirebaseLoginType): Result<String>

    suspend fun logout(): Result<Unit>

    enum class FirebaseLoginType {
        GOOGLE,
        FACEBOOK,
    }
}