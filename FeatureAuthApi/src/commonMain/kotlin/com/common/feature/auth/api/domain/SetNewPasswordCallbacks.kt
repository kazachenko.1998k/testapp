package com.common.feature.auth.api.domain

interface SetNewPasswordCallbacks {
    fun onSuccessSaveNewPass()
    fun onBackClick()
}