apply(from = "../versions.gradle.kts")
val koin_version: String by extra
val coroutines_version: String by extra
plugins {
    id("multiplatform-library-convention")
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("io.insert-koin:koin-core:$koin_version")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
                implementation(project(":CoreMVIAbstract"))
                implementation(project(":FeatureNavigationApi"))
                implementation(project(":CoreBackendApi"))
                implementation(project(":CorePreferenceApi"))
                implementation(project(":Utils"))
            }
        }
    }
}
