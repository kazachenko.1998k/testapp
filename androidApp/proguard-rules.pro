# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keep class com.common.feature.account.impl.data.rawmodels.** { <fields>; }
-keep class com.common.feature.dashboard.impl.data.rawmodels.** { <fields>; }
-keep class com.common.feature.auth.impl.data.rawmodels.** { <fields>; }
-keep class com.common.feature.dashboard.impl.data.rawmodels.** { <fields>; }
-keep class com.common.feature.indicators.impl.data.rawmodels.** { <fields>; }
-keep class com.common.feature.robot.impl.data.rawmodels.** { <fields>; }
-keep class com.common.utils.BackendException { <fields>; }
-keep class androidx.datastore.*.** {*;}

-repackageclasses
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
    public static *** i(...);
    public static *** w(...);
}

# If you keep the line number information, uncomment this to
# hide the original source file name.
-renamesourcefileattribute SourceFile

# Branch
-keep class com.google.android.gms.** { *; }
-keep class com.huawei.hms.ads.** { *; }
-keep interface com.huawei.hms.ads.** { *; }
