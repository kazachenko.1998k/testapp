import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

apply(from = "../versions.gradle.kts")
val koin_version: String by extra
val activity_version: String by extra
val compose_activity: String by extra
val compose_ui: String by extra
val application_id: String by extra
val version_code: String by extra
val version_name: String by extra
val nav_version: String by extra
val coroutines_version: String by extra
val coil_compose: String by extra
val compile_sdk: String by extra
val accompanist_version: String by extra
val multiplatform_settings: String by extra
val firebase_bom: String by extra
val datastore: String by extra
val android_chart: String by extra
val compose_shimmer: String by extra
val constraintlayout_compose: String by extra
val splashscreen: String by extra
val lifecycle_runtime_ktx: String by extra
val crisp: String by extra
val multidex: String by extra
val oneSignalAppID: String by extra
val amplitudeKeyTest: String by extra
val amplitudeKeyLive: String by extra
val branchKeyTest: String by extra
val branchKeyLive: String by extra
val mockito: String by extra
val gson: String by extra
val orchestrator: String by extra
val exoplayer: String by extra
val androidx_camera: String by extra
val androidx_camera_view: String by extra
val barcode_scanning: String by extra
val guava: String by extra
val lottie_version: String by extra

plugins {
    id("android-app-convention")
    id("kotlin-kapt")
    id("com.google.firebase.crashlytics")
    id("com.google.firebase.firebase-perf")
}

dependencies {
    implementation(project(":shared"))

    // compose
    implementation("androidx.core:core-splashscreen:$splashscreen")
    implementation("androidx.compose.ui:ui:$compose_ui")
    implementation("androidx.activity:activity-ktx:$activity_version")
    implementation("androidx.activity:activity-compose:$compose_activity")

    // Testing compose
    androidTestImplementation("androidx.compose.ui:ui-test-junit4:$compose_ui")
    debugImplementation("androidx.compose.ui:ui-test-manifest:$compose_ui")
    androidTestImplementation("androidx.test:runner:$orchestrator")
    androidTestImplementation("io.insert-koin:koin-test:$koin_version")
    androidTestImplementation("io.insert-koin:koin-test-junit5:$koin_version")
    androidTestImplementation("org.mockito:mockito-android:$mockito")
    androidTestUtil("androidx.test:orchestrator:$orchestrator")

    // Tooling support (Previews, etc.)
    implementation("androidx.compose.ui:ui-tooling:$compose_ui")

    // Foundation (Border, Background, Box, Image, Scroll, shapes, animations, etc.)
    implementation("androidx.compose.foundation:foundation:$compose_ui")
    implementation("androidx.compose.foundation:foundation-layout:$compose_ui")

    // Material Design
    implementation("androidx.compose.material:material:$compose_ui")
    implementation("androidx.constraintlayout:constraintlayout-compose:$constraintlayout_compose")

    // Material design icons
    implementation("io.coil-kt:coil-compose:$coil_compose")
    implementation("io.coil-kt:coil-svg:$coil_compose")
    implementation("io.coil-kt:coil-video:$coil_compose")
    implementation("androidx.compose.material:material-icons-core:$compose_ui")
    implementation("androidx.compose.material:material-icons-extended:$compose_ui")

    // Integration with observables
    implementation("androidx.compose.runtime:runtime:$compose_ui")

    // Accompanist
    implementation("com.google.accompanist:accompanist-swiperefresh:$accompanist_version")
    implementation("com.google.accompanist:accompanist-pager:$accompanist_version")
    implementation("com.google.accompanist:accompanist-insets:$accompanist_version")
    implementation("com.google.accompanist:accompanist-pager-indicators:$accompanist_version")

    // Settings
    implementation("com.russhwolf:multiplatform-settings-coroutines-native-mt:$multiplatform_settings")

    // Datastore
    implementation("androidx.datastore:datastore:$datastore")
    implementation("androidx.datastore:datastore-preferences-core:$datastore")

    // Other
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:$lifecycle_runtime_ktx")
    implementation("com.github.PhilJay:MPAndroidChart:$android_chart")
    implementation("com.valentinilk.shimmer:compose-shimmer:$compose_shimmer")
    implementation("androidx.multidex:multidex:$multidex")


    // Google
    implementation(platform("com.google.firebase:firebase-bom:$firebase_bom"))
    implementation("com.google.firebase:firebase-analytics-ktx") {
        isTransitive = true
    }
    implementation("com.google.firebase:firebase-crashlytics-ktx") {
        isTransitive = true
    }
    implementation("com.google.firebase:firebase-perf-ktx") {
        isTransitive = true
    }
}

android {
    signingConfigs {
        create("qa") {
            storeFile = file("../app.keystore")
            storePassword = System.getenv("KEYSTORE_PASSWORD")
            keyAlias = System.getenv("KEY_ALIAS")
            keyPassword = System.getenv("KEY_PASSWORD")
        }
        create("release") {//comment topic for build local release build
            storeFile = file("../app.keystore")
            storePassword = System.getenv("KEYSTORE_PASSWORD_RELEASE")
            keyAlias = System.getenv("KEY_ALIAS_RELEASE")
            keyPassword = System.getenv("KEY_PASSWORD_RELEASE")
        }
    }
    defaultConfig {
        val version_code_env = System.getenv("VERSION_CODE")
        val version_name_env = System.getenv("VERSION_NAME")

        if (version_code_env != null && version_name_env != null) {
            versionCode = version_code_env.toInt()
            versionName = version_name_env
        } else {
            versionCode = version_code.toInt()
            versionName = version_name
        }
        applicationId = application_id
        compileSdkVersion = compile_sdk
        multiDexEnabled = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        testInstrumentationRunnerArguments["clearPackageData"] = "true"

        testOptions {
            execution = "ANDROIDX_TEST_ORCHESTRATOR"
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    // For Kotlin projects
    kotlinOptions {
        jvmTarget = "11"
    }

    packagingOptions {
        resources.excludes.add("META-INF/*")
    }

    buildTypes {
        onEach {
            it.manifestPlaceholders.putAll(
                mapOf(
                    "oneSignalAppID" to oneSignalAppID,
                    "branchKeyLive" to branchKeyLive,
                    "branchKeyTest" to branchKeyTest
                )
            )
        }
        getByName("release") {
            manifestPlaceholders["isBranchDebug"] = false
            manifestPlaceholders["amplitudeKey"] = amplitudeKeyLive
            isMinifyEnabled = true
            isDebuggable = false
            signingConfig = signingConfigs["release"]
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        getByName("rc") {
            initWith(getByName("release"))
        }
        getByName("debug") {
            manifestPlaceholders["isBranchDebug"] = true
            manifestPlaceholders["amplitudeKey"] = amplitudeKeyTest
            applicationIdSuffix = ".test"
            versionNameSuffix = " Test"
        }
        getByName("qa") {
            initWith(getByName("debug"))
            signingConfig = signingConfigs["qa"]
        }
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.freeCompilerArgs += "-Xjvm-default=all"
    kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
    kotlinOptions.freeCompilerArgs += listOf(
        "-P",
        "plugin:androidx.compose.compiler.plugins.kotlin:suppressKotlinVersionCompatibilityCheck=true"
    )
}
