package com.test.android.auth

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.test.android.R
import com.test.android.shared.*
import com.test.android.shared.elements.ButtonWithBigIcon
import com.test.android.shared.elements.defaultBtnModifier

@Composable
@SuppressLint("ModifierParameter")
fun SocialBlock(
    modifier: Modifier = Modifier.defaultBtnModifier(),
    onGoogle: () -> Unit,
    onFacebook: () -> Unit
) {
    Row(modifier) {
        ButtonWithBigIcon(
            onClick = onGoogle,
            modifier = Modifier.weight(1f),
            text = R.string.google_text,
            icon = R.drawable.ic_google_logo,
            background = GoogleBackground,
            textStyle = Body1_16Bold.copy(color = GoogleText)
        )
        Spacer(Modifier.width(LargePadding))
        ButtonWithBigIcon(
            onClick = onFacebook,
            modifier = Modifier.weight(1f),
            text = R.string.facebook_text,
            icon = R.drawable.ic_fb_logo,
            background = FBBackground,
            textStyle = Body1_16Bold.copy(color = FBText)
        )
    }
}