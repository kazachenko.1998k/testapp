package com.test.android.auth

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import coil.compose.rememberImagePainter
import com.common.feature.auth.api.data.models.SocialLogin
import com.test.android.R
import com.test.android.shared.Headline4_17Regular
import com.test.android.shared.IconBtnSize
import com.test.android.shared.IconInInfoSize
import com.test.android.shared.XLargePadding


@Composable
fun SocialLoginScreen(socialLogin: SocialLogin?) {
    AnimatedVisibility(socialLogin != null, enter = fadeIn(), exit = fadeOut()) {
        socialLogin?.let { SocialLoginInfo(it) }
    }
}


@Composable
fun SocialLoginInfo(socialLogin: SocialLogin) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black.copy(alpha = 0.85f))
            .clickable(
                indication = null,
                interactionSource = remember { MutableInteractionSource() },
                onClick = {}
            ),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        SocialLoginIconWithProgress(socialLogin)
        Spacer(modifier = Modifier.height(XLargePadding))
        Text(
            text = socialLogin.toText(),
            style = Headline4_17Regular.copy(color = MaterialTheme.colors.onBackground)
        )
    }
}

@Composable
fun SocialLoginIconWithProgress(socialLogin: SocialLogin) {
    Box {
        CircularProgressIndicator(
            modifier = Modifier
                .align(Alignment.Center)
                .size(IconBtnSize)
        )
        Image(
            painter = rememberImagePainter(data = socialLogin.toIcon()),
            contentDescription = socialLogin.toIcon().toString(),
            modifier = Modifier
                .align(Alignment.Center)
                .size(IconInInfoSize)
        )
    }
}

fun SocialLogin.toIcon() = when (this) {
    SocialLogin.Facebook -> R.drawable.ic_fb_logo
    SocialLogin.Google -> R.drawable.ic_google_logo
}


@Composable
fun SocialLogin.toText() = stringResource(id = R.string.loading_account_title, this.name)