package com.test.android.auth

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import com.common.feature.auth.api.ui.VerifyCodeStore
import com.common.feature.navigation.api.VerifyCodeScreen
import com.test.android.R
import com.test.android.shared.*
import com.test.android.shared.elements.CodeEditText
import com.test.android.shared.elements.TextWithClickAreaById
import com.test.android.shared.elements.TextWithClickAreaByText
import com.test.android.shared.elements.ToolbarWithBackIcon
import org.koin.java.KoinJavaComponent.getKoin

class VerifyCodeScreenImpl : VerifyCodeScreen(), WithComposeDraw {
    @Composable
    override fun OnDrawCompose() {
        VerifyCodeView()
    }

    override val parentClass = super.parentClass
}

@Composable
fun VerifyCodeView(
    store: VerifyCodeStore = remember { getKoin().get(VerifyCodeStore::class) }
) {
    val state by store.observeState().collectAsState()
    LaunchedEffect(Unit) {
        store.dispatch(VerifyCodeStore.OnShowScreen)
    }
    Column(
        modifier = Modifier
            .fillMaxSize(),
    ) {
        ToolbarWithBackIcon(onClick = { store.dispatch(VerifyCodeStore.OnBackClick) })
        TextWithClickAreaByText(
            original = R.string.enter_code_text,
            args = listOf(state.email to { }),
            styleDefault = Headline4_17Regular.copy(
                color = DesignTextPrimary,
                textAlign = TextAlign.Center
            ),
            styleSelect = Headline4_17Medium.copy(
                color = DesignPrimaryLight
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = XLargePadding,
                    end = XLargePadding,
                    bottom = XXLargePadding + XLargePadding,
                    top = XLargePadding
                )
        )
        CodeEditText(state.code, {
            store.dispatch(VerifyCodeStore.OnCodeModify(it))
        })
        TextWithClickAreaById(
            original = R.string.resend_email_text_0,
            args = mapOf(
                R.string.resend_title to { store.dispatch(VerifyCodeStore.OnResendInstructionsClick) },
            ),
            styleDefault = Body2_15Regular.copy(
                color = DesignTextDescription,
                textAlign = TextAlign.Center
            ),
            styleSelect = Body2_15Medium.copy(
                color = DesignPrimaryLight
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = XLargePadding,
                    end = XLargePadding,
                    top = XXLargePadding
                )
        )
    }
}
