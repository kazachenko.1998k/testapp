package com.test.android.auth

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import com.common.feature.auth.api.ui.LoginStore
import com.common.feature.navigation.api.LoginScreen
import com.test.android.R
import com.test.android.shared.*
import com.test.android.shared.elements.*
import org.koin.java.KoinJavaComponent.getKoin

class LoginScreenImpl : LoginScreen(), WithComposeDraw {
    @Composable
    override fun OnDrawCompose() {
        LoginView()
    }

    override val parentClass = super.parentClass
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun LoginView(store: LoginStore = remember { getKoin().get(LoginStore::class) }) {
    val state by store.observeState().collectAsState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .clearFocusOnTouch()
            .clearFocusOnEvent(store),
    ) {
        ToolbarWithBackIcon(onClick = { store.dispatch(LoginStore.OnBackClick) })
        TitleText(id = R.string.sign_in_title)
        Spacer(modifier = Modifier.height(XLargePadding))
        EditTextWithTitle(
            title = stringResource(id = R.string.enter_your_email_text),
            onEdit = { store.dispatch(LoginStore.OnEmailModify(it)) },
            text = state.email.text,
            hint = stringResource(id = R.string.email_title),
            hasError = state.email.hasError,
        )
        EditTextWithTitle(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = XLargePadding,
                    end = XLargePadding,
                    bottom = MediumPadding
                )
                .height(IconBtnSize),
            title = stringResource(id = R.string.enter_your_password_text),
            onEdit = { store.dispatch(LoginStore.OnPassModify(it)) },
            text = state.pass.text,
            lastElement = true,
            hint = stringResource(id = R.string.password_title),
            hasError = state.pass.hasError,
            keyboardType = KeyboardType.Password,
            isPasswordMode = state.pass.hide,
            onChangeVisibleState = { store.dispatch(LoginStore.OnPassChangeVisible) }
        )
        TextOnlyClickArea(
            original = R.string.forgot_password_text,
            onClick = { store.dispatch(LoginStore.OnForgotPasswordClick) },
            styleSelect = Body2_15Medium.copy(color = DesignPrimaryLight),
            modifier = Modifier
                .align(Alignment.End)
                .padding(horizontal = XLargePadding)
                .padding(bottom = XXXLargePadding)
        )
        ActionBtn(
            onClick = { store.dispatch(LoginStore.OnLoginClick) },
            text = R.string.sign_in_title,
            enabled = state.loginBtnEnabled,
            hasProgress = state.isLoading
        )
        TextWithClickAreaById(
            original = R.string.dont_have_an_account_text_0,
            args = mapOf(R.string.sign_up_title to { store.dispatch(LoginStore.OnRegisterClick) }),
            styleDefault = Body2_15Regular.copy(
                color = DesignTextDescription,
                textAlign = TextAlign.Center
            ),
            styleSelect = Body2_15Medium.copy(color = DesignPrimaryLight),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = XLargePadding)
                .padding(
                    top = MediumPadding,
                    bottom = XXLargePadding
                )
        )
        Spacer(modifier = Modifier.weight(1f))
        SocialBlock(
            onGoogle = { store.dispatch(LoginStore.OnGoogleClick) },
            onFacebook = { store.dispatch(LoginStore.OnFacebookClick) },
        )
    }
    SocialLoginScreen(state.socialLoginState)
}
