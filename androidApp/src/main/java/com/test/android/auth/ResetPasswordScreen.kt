package com.test.android.auth

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.common.feature.auth.api.ui.ResetPasswordStore
import com.common.feature.navigation.api.ResetPasswordScreen
import com.test.android.R
import com.test.android.shared.WithComposeDraw
import com.test.android.shared.elements.*
import com.test.android.shared.imeMinPadding
import org.koin.java.KoinJavaComponent.getKoin

class ResetPasswordScreenImpl : ResetPasswordScreen(), WithComposeDraw {
    @Composable
    override fun OnDrawCompose() {
        ResetPasswordView()
    }

    override val parentClass = super.parentClass
}

@Composable
fun ResetPasswordView(store: ResetPasswordStore = remember { getKoin().get(ResetPasswordStore::class) }) {
    val state by store.observeState().collectAsState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .clearFocusOnTouch()
            .imeMinPadding(),
    ) {
        ToolbarWithBackIcon(onClick = { store.dispatch(ResetPasswordStore.OnBackClick) })
        TitleText(id = R.string.reset_password_title)
        SubTitleText(R.string.reset_password_text)
        EditTextWithTitle(
            title = stringResource(id = R.string.enter_your_email_text),
            onEdit = { store.dispatch(ResetPasswordStore.OnEmailModify(it)) },
            onFinishEdit = { store.dispatch(ResetPasswordStore.OnEmailModifyEnd) },
            text = state.email.text,
            lastElement = true,
            hint = stringResource(id = R.string.email_title),

            hasError = state.email.hasError,
        )
        Spacer(Modifier.weight(1f))
        ActionBtn(
            onClick = { store.dispatch(ResetPasswordStore.OnSendInstructionsClick) },
            text = R.string.send_instructions_btn,
            enabled = state.sendBtnEnabled,
            hasProgress = state.isLoading
        )
    }
}

