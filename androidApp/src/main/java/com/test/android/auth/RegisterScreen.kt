package com.test.android.auth

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import com.common.feature.auth.api.data.models.ErrorCheckState
import com.common.feature.auth.api.ui.RegisterStore
import com.common.feature.navigation.api.RegisterScreen
import com.test.android.R
import com.test.android.shared.*
import com.test.android.shared.elements.*
import org.koin.java.KoinJavaComponent.getKoin

class RegisterScreenImpl : RegisterScreen(), WithComposeDraw {

    @Composable
    override fun OnDrawCompose() {
        RegisterView()
    }

    @Composable
    override fun BottomSheetView() {
        val store = remember { getKoin().get<RegisterStore>(RegisterStore::class) }
        UserAgreementBottomSheet(store)
    }

    override val parentClass = super.parentClass
}

@OptIn(ExperimentalAnimationApi::class, ExperimentalMaterialApi::class)
@Composable
fun RegisterView(
    store: RegisterStore = remember { getKoin().get() }
) {
    val state by store.observeState().collectAsState()
    LaunchedEffect(Unit) {
        store.dispatch(RegisterStore.OnShowScreen)
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .clearFocusOnTouch()
            .clearFocusOnEvent(store),
    ) {
        AnimatedVisibility(visible = !state.pass.helperPassCheckersIsVisible) {
            Column {
                ToolbarWithBackIcon(onClick = { store.dispatch(RegisterStore.OnBackClick) })
            }
        }
        TitleText(id = R.string.sign_up_title)
        Spacer(modifier = Modifier.height(XLargePadding))
        EditTextWithTitle(
            title = stringResource(id = R.string.enter_your_email_text),
            onEdit = { store.dispatch(RegisterStore.OnEmailModify(it)) },
            onFinishEdit = { store.dispatch(RegisterStore.OnEmailModifyEnd) },
            text = state.email.text,
            hint = stringResource(id = R.string.email_title),
            hasError = state.email.hasError,
        )
        EditTextWithTitle(
            title = stringResource(id = R.string.enter_your_password_text),
            onEdit = { store.dispatch(RegisterStore.OnPassModify(it)) },
            onStartEdit = { store.dispatch(RegisterStore.OnPassModifyStart) },
            onFinishEdit = { store.dispatch(RegisterStore.OnPassModifyEnd) },
            text = state.pass.field.text,
            lastElement = true,
            hint = stringResource(id = R.string.password_title),
            hasError = state.pass.field.hasError,
            keyboardType = KeyboardType.Password,
            isPasswordMode = state.pass.field.hide,
            onChangeVisibleState = { store.dispatch(RegisterStore.OnPassChangeVisible) }
        )
        AnimatedVisibility(
            visible = state.pass.helperPassCheckersIsVisible,
        ) {
            Column {
                CheckView(
                    checkState = state.pass.passHasMinimum8Characters,
                    text = R.string.minimum_8_characters_text
                )
                CheckView(
                    checkState = state.pass.passHasOneNumber,
                    text = R.string.one_number_text
                )
                AnimatedVisibility(visible = state.pass.passTrust == ErrorCheckState.ERROR) {
                    CheckView(
                        checkState = ErrorCheckState.ERROR,
                        text = R.string.weak_password_text
                    )
                }
            }
        }
        Spacer(modifier = Modifier.height(XXXLargePadding))
        ActionBtn(
            onClick = { store.dispatch(RegisterStore.OnRegisterClick) },
            text = R.string.sign_up_title,
            enabled = state.registerBtnEnabled,
            hasProgress = state.isLoading
        )

        TextWithClickAreaById(
            original = R.string.already_have_an_account_text_0,
            args = mapOf(R.string.sign_in_title to { store.dispatch(RegisterStore.OnLoginClick) }),
            styleDefault = Body2_15Regular.copy(
                color = DesignTextDescription,
                textAlign = TextAlign.Center
            ),
            styleSelect = Body2_15Medium.copy(color = DesignPrimaryLight),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = XLargePadding)
                .padding(
                    top = MediumPadding,
                    bottom = XXLargePadding
                )
        )
        Spacer(modifier = Modifier.weight(1f))
        SocialBlock(
            onGoogle = { store.dispatch(RegisterStore.OnGoogleClick) },
            onFacebook = { store.dispatch(RegisterStore.OnFacebookClick) },
        )
    }
    SocialLoginScreen(state.socialRegisterState)
}

@Composable
fun UserAgreementBottomSheet(store: RegisterStore) {
    val state by store.observeState().collectAsState()
    BottomSheetColumn {
        BottomSheetTopView()
        TextWithClickAreaById(
            original = R.string.user_agreement_text,
            args = mapOf(
                R.string.user_agreement_title to { store.dispatch(RegisterStore.OnTermsAndConditionsClick) },
                R.string.privacy_policies_title to { store.dispatch(RegisterStore.OnPrivacyPolicyClick) }
            ),
            styleDefault = Body2_15Regular.copy(
                color = DesignTextDescription,
            ),
            styleSelect = Body2_15Medium.copy(
                color = DesignPrimaryLight
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = XLargePadding,
                    end = XLargePadding,
                    bottom = XXLargePadding,
                    top = XLargePadding
                )
        )
        ActionBtn(
            onClick = { store.dispatch(RegisterStore.OnAcceptAndContinueClick) },
            text = R.string.accept_and_continue_btn,
            hasProgress = state.isLoading
        )
    }
}