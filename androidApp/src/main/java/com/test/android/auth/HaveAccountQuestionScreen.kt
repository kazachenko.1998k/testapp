package com.test.android.auth

import android.annotation.SuppressLint
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import com.common.feature.auth.api.ui.HaveAccountQuestionStore
import com.common.feature.navigation.api.HaveAccountQuestionScreen
import com.test.android.R
import com.test.android.shared.*
import com.test.android.shared.elements.ActionBtn
import com.test.android.shared.elements.SecondActionBtn
import com.test.android.shared.elements.TextWithClickAreaById
import org.koin.java.KoinJavaComponent

class HaveAccountQuestionScreenImpl : HaveAccountQuestionScreen(), WithComposeDraw {
    @Composable
    override fun OnDrawCompose() {
        HaveAccountQuestionView()
    }

    override val parentClass = super.parentClass
}

@SuppressLint("HardwareIds")
@OptIn(ExperimentalAnimationApi::class)
@Composable
fun HaveAccountQuestionView(
    store: HaveAccountQuestionStore = remember { KoinJavaComponent.getKoin().get() }
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        TextWithClickAreaById(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = XLargePadding,
                    end = XLargePadding,
                    top = HaveAccountQuestionTitlePaddingTop
                ),
            original = R.string.diversify_your_portfolio_with_0,
            styleDefault = Large_Titles1_32Bold.copy(MaterialTheme.colors.onBackground),
            styleSelect = Large_Titles1_32Bold.copy(DesignPrimaryLight),
            args = mapOf(R.string.trading_robots to {})
        )
        Spacer(modifier = Modifier.weight(1f))
        ActionBtn(
            onClick = { store.dispatch(HaveAccountQuestionStore.OnGetStartedClick) },
            text = R.string.get_started_btn
        )
        SecondActionBtn(
            onClick = { store.dispatch(HaveAccountQuestionStore.OnLoginClick) },
            text = R.string.sign_in_title
        )
    }
}
