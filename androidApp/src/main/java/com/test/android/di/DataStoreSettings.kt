package com.test.android.di

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import com.russhwolf.settings.coroutines.FlowSettings
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

@JvmDefaultWithoutCompatibility
class DataStoreSettings(private val datastore: DataStore<Preferences>) : FlowSettings {
    override suspend fun keys(): Set<String> =
        datastore.data.first().asMap().keys.map { it.name }.toSet()

    override suspend fun size(): Int = datastore.data.first().asMap().size

    override suspend fun clear(): Unit = keys().forEach { remove(it) }

    override suspend fun remove(key: String) {
        datastore.edit { it.remove(stringSetPreferencesKey(key)) }
    }

    override suspend fun hasKey(key: String): Boolean =
        datastore.data.first().contains(stringSetPreferencesKey(key))

    override suspend fun putInt(key: String, value: Int) {
        datastore.edit { it[intPreferencesKey(key)] = value }
    }

    override fun getIntFlow(key: String, defaultValue: Int): Flow<Int> =
        getValue { it[intPreferencesKey(key)] ?: defaultValue }

    override fun getIntOrNullFlow(key: String): Flow<Int?> =
        getValue { it[intPreferencesKey(key)] }

    override suspend fun putLong(key: String, value: Long) {
        datastore.edit { it[longPreferencesKey(key)] = value }
    }

    override fun getLongFlow(key: String, defaultValue: Long): Flow<Long> =
        getValue { it[longPreferencesKey(key)] ?: defaultValue }

    override fun getLongOrNullFlow(key: String): Flow<Long?> =
        getValue { it[longPreferencesKey(key)] }

    override suspend fun putString(key: String, value: String) {
        datastore.edit { it[stringPreferencesKey(key)] = value }
    }

    override fun getStringFlow(key: String, defaultValue: String): Flow<String> =
        getValue { it[stringPreferencesKey(key)] ?: defaultValue }

    override fun getStringOrNullFlow(key: String): Flow<String?> =
        getValue { it[stringPreferencesKey(key)] }

    override suspend fun putFloat(key: String, value: Float) {
        datastore.edit { it[floatPreferencesKey(key)] = value }
    }

    override fun getFloatFlow(key: String, defaultValue: Float): Flow<Float> =
        getValue { it[floatPreferencesKey(key)] ?: defaultValue }

    override fun getFloatOrNullFlow(key: String): Flow<Float?> =
        getValue { it[floatPreferencesKey(key)] }

    override suspend fun putDouble(key: String, value: Double) {
        datastore.edit { it[doublePreferencesKey(key)] = value }
    }

    override fun getDoubleFlow(key: String, defaultValue: Double): Flow<Double> =
        getValue { it[doublePreferencesKey(key)] ?: defaultValue }

    override fun getDoubleOrNullFlow(key: String): Flow<Double?> =
        getValue { it[doublePreferencesKey(key)] }

    override suspend fun putBoolean(key: String, value: Boolean) {
        datastore.edit { it[booleanPreferencesKey(key)] = value }
    }

    override fun getBooleanFlow(key: String, defaultValue: Boolean): Flow<Boolean> =
        getValue { it[booleanPreferencesKey(key)] ?: defaultValue }

    override fun getBooleanOrNullFlow(key: String): Flow<Boolean?> =
        getValue { it[booleanPreferencesKey(key)] }

    private inline fun <T> getValue(crossinline getValue: (Preferences) -> T): Flow<T> =
        datastore.data.map { getValue(it) }.distinctUntilChanged()
}