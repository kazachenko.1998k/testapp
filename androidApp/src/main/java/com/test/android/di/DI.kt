package com.test.android.di

import android.content.Context
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import com.common.feature.navigation.api.*
import com.common.feature.navigation.api.usecases.BottomSheetUseCase
import com.russhwolf.settings.coroutines.FlowSettings
import com.test.android.auth.*
import com.test.android.utils.BottomSheetComposeUseCaseImpl
import org.koin.dsl.bind
import org.koin.dsl.module
import java.io.File

val DIAuth = module {
    single { LoginScreenImpl() } bind LoginScreen::class
    single { RegisterScreenImpl() } bind RegisterScreen::class
    single { ResetPasswordScreenImpl() } bind ResetPasswordScreen::class
    single { VerifyCodeScreenImpl() } bind VerifyCodeScreen::class
    single { HaveAccountQuestionScreenImpl() } bind HaveAccountQuestionScreen::class
}


val DIOther = module {
    single { BottomSheetComposeUseCaseImpl() } bind BottomSheetUseCase::class
}

val DIStore = module {
    single { DataStoreSettings(get()) } bind FlowSettings::class
    single {
        PreferenceDataStoreFactory.create {
            val defaultPreferencesName = get<Context>().packageName + "_preferences"
            File(get<Context>().filesDir, "datastore/$defaultPreferencesName.preferences_pb")
        }
    }
}