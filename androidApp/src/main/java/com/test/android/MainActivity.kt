package com.test.android

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.Spring.StiffnessMediumLow
import androidx.compose.animation.core.SpringSpec
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowCompat
import com.common.core.mviabstraction.usecases.BannerUseCase
import com.common.feature.auth.api.ui.SplashStore
import com.common.feature.navigation.api.*
import com.common.feature.navigation.api.usecases.BottomSheetUseCase
import com.google.accompanist.insets.systemBarsPadding
import com.test.android.shared.*
import com.test.android.shared.elements.WithBannerListener
import com.test.android.utils.BottomSheetComposeUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.java.KoinJavaComponent
import org.koin.java.KoinJavaComponent.getKoin


class MainActivity : ComponentActivity() {

    private val navigator by lazy { getKoin().get<Navigation>(Navigation::class) }
    private val bannerUseCase by lazy { getKoin().get<BannerUseCase>(BannerUseCase::class) }
    private val bottomSheetComposeUseCase =
        getKoin().get<BottomSheetComposeUseCase>(BottomSheetUseCase::class)

    @OptIn(ExperimentalMaterialApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        setTheme(R.style.Default)
        val store: SplashStore = KoinJavaComponent.get(SplashStore::class.java)
        store.dispatch(SplashStore.LoadData)
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        navigator.onEmptyBackStack {
            val startMain = Intent(Intent.ACTION_MAIN).apply {
                addCategory(Intent.CATEGORY_HOME)
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
            }
            startActivity(startMain)
        }
        setContent {
            val coroutineScope = rememberCoroutineScope()
            val navController = remember { mutableStateOf(navigator.current() as? WithComposeDraw) }
            val sheetContent = remember {
                mutableStateOf<@Composable ColumnScope.() -> Unit>({
                    Box(Modifier.size(XLargePadding))
                })
            }
            var bottomSheetScaffoldState: ModalBottomSheetState? = null
            bottomSheetScaffoldState = rememberModalBottomSheetState(
                initialValue = ModalBottomSheetValue.Hidden,
                confirmStateChange = {
                    if (it == ModalBottomSheetValue.HalfExpanded) {
                        coroutineScope.launch {
                            bottomSheetScaffoldState?.hide()
                        }
                    }
                    true
                },
                animationSpec = SpringSpec(stiffness = StiffnessMediumLow)
            )
            navController.value?.let {
                CurrentScreenWithBottomSheetAndSnackBar(
                    bottomSheetScaffoldState,
                    sheetContent,
                    it
                )
            }
            ListenNavigation(
                navController,
                sheetContent
            )
            ListenExternalAppEvents()
        }
    }

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun CurrentScreenWithBottomSheetAndSnackBar(
        bottomSheetScaffoldState: ModalBottomSheetState,
        sheetContent: MutableState<@Composable (ColumnScope.() -> Unit)>,
        screen: WithComposeDraw,
    ) {
        bottomSheetComposeUseCase.Setup(modalBottomSheetState = bottomSheetScaffoldState)
        CustomAppTheme {
            ModalBottomSheetLayout(
                modifier = Modifier
                    .background(MaterialTheme.colors.background)
                    .systemBarsPadding(),
                sheetState = bottomSheetScaffoldState,
                scrimColor = DesignBlack80,
                sheetContent = {
                    Column(
                        Modifier
                            .fillMaxSize()
                            .clickable(
                                indication = null,
                                interactionSource = remember { MutableInteractionSource() },
                            ) {
                                bottomSheetComposeUseCase.close()
                            }
                            .padding(top = XXXLargePadding),
                        verticalArrangement = Arrangement.Bottom
                    ) {
                        sheetContent.value.invoke(this)
                    }
                },
                sheetElevation = 0.dp,
                sheetBackgroundColor = DesignTransparent,
                content = {
                    CurrentScreen(screen)
                    WithBannerListener(bannerUseCase = bannerUseCase)
                }
            )
        }
    }

    @Composable
    fun CurrentScreen(
        screen: WithComposeDraw,
    ) {
        Column {
            Scaffold(modifier = Modifier.weight(1f)) {
                Crossfade(targetState = screen) {
                    it.OnDrawCompose()
                }
            }
        }
    }

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun ListenNavigation(
        navController: MutableState<WithComposeDraw?>,
        sheetContent: MutableState<@Composable (ColumnScope.() -> Unit)>,
    ) {
        LaunchedEffect(Unit) {
            withContext(Dispatchers.Main) {
                navigator
                    .subscribe()
                    .filterIsInstance<WithComposeDraw>()
                    .collect { screen ->
                        navController.value = screen
                        sheetContent.value =
                            {
                                screen.BottomSheetView()
                            }
                    }
            }
        }
    }

    @Composable
    fun ListenExternalAppEvents() {
        LaunchedEffect(Unit) {
            navigator
                .subscribeExternalAppEvents()
                .collect { screen ->
                    when (screen) {
                        is BrowserLink -> screen.url.openLink()
                    }
                }
        }
    }

    private fun String.openLink() {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(this)
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (navigator.current()?.onBackListener?.invoke() != false && bottomSheetComposeUseCase.canGoBack()) {
            navigator.back()
        }
    }

}