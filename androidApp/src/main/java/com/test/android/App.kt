package com.test.android

import android.app.Application
import com.common.MultiplatformTools
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initServices()
    }

    private fun initServices() {
        MultiplatformTools().initKoinPlatform(diList)
    }

    private val diList by lazy {
        listOf(
            module { single { applicationContext } },
            com.test.android.di.DIStore,
            com.test.android.di.DIAuth,
            com.test.android.di.DIOther,
        )
    }
}