package com.test.android.shared.elements

import android.annotation.SuppressLint
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.test.android.R
import com.test.android.shared.*

@Composable
fun ActionBtn(
    onClick: () -> Unit,
    text: Int,
    hasProgress: Boolean = false,
    enabled: Boolean = true,
    color: Color = MaterialTheme.colors.primary,
    textStyle: TextStyle = Headline4_17Medium
) {
    Button(
        onClick = if (hasProgress) EmptyBtnAction else onClick,
        modifier = Modifier.defaultBtnModifier(),
        enabled = enabled,
        colors = ButtonDefaults.buttonColors(backgroundColor = color),
        elevation = ButtonDefaults.elevation(defaultElevation = 0.dp),
        shape = RoundedCornerShape(LargePadding)
    ) {
        Crossfade(
            targetState = hasProgress,
        ) { hasProgress ->
            if (hasProgress) {
                DottedProgressBar(Modifier.fillMaxSize())
            } else {
                /*wrap in box for ignore change position in crossFade anim*/
                Box(modifier = Modifier.fillMaxSize()) {
                    Text(
                        text = stringResource(id = text),
                        style = textStyle,
                        modifier = Modifier.align(Alignment.Center)
                    )
                }
            }
        }
    }
}


@Composable
fun ErrorBtn(
    onClick: () -> Unit,
    text: Int,
    hasProgress: Boolean = false,
    enabled: Boolean = true
) {
    ActionBtn(
        onClick = onClick,
        text = text,
        hasProgress = hasProgress,
        enabled = enabled,
        color = MaterialTheme.colors.error
    )
}

@SuppressLint("ModifierParameter")
@Composable
fun SecondActionBtn(
    onClick: () -> Unit,
    text: Int,
    modifier: Modifier = Modifier.defaultBtnModifier(),
) {
    SecondActionBtn(onClick, stringResource(id = text), modifier)
}

@SuppressLint("ModifierParameter")
@Composable
fun SecondActionBtn(
    onClick: () -> Unit,
    text: String,
    modifier: Modifier = Modifier.defaultBtnModifier(),
    textStyle: TextStyle = Headline4_17Medium
) {
    Button(
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(backgroundColor = DesignSecondaryBtn),
        modifier = modifier,
        elevation = ButtonDefaults.elevation(defaultElevation = 0.dp),
        shape = RoundedCornerShape(LargePadding)
    ) {
        Text(text = text, color = DesignPrimaryLight, style = textStyle)
    }
}


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun Selector(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    textSelector: String,
    painter: Painter = rememberImagePainter(data = R.drawable.ic_arrow_expand),
    background: Color = MaterialTheme.colors.surface
) {
    Card(
        modifier = modifier,
        onClick = {
            onClick.invoke()
        },
    ) {
        Row(
            modifier = Modifier
                .background(color = background)
                .padding(horizontal = LargePadding),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = textSelector,
                style = Body2_15Medium.copy(color = DesignTextDescription),
            )
            Spacer(modifier = Modifier.width(SmallPadding))
            Icon(
                painter = painter,
                contentDescription = null,
                tint = DesignTextDescription,
                modifier = Modifier
                    .padding(vertical = MediumPadding)
                    .size(XLargePadding)
            )
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun TransparentBtn(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    text: String,
) {
    Row(
        modifier = modifier
            .clip(MaterialTheme.shapes.small)
            .clickable { onClick.invoke() }
            .padding(
                horizontal = MediumPadding,
                vertical = 10.dp
            ),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            text = text,
            style = Body2_15Medium.copy(color = DesignPrimaryLight),
        )
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ButtonWithBigIcon(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    text: Int,
    icon: Int,
    background: Color = DesignUpperSurface,
    textStyle: TextStyle = Subtitle_14Medium.copy(color = MaterialTheme.colors.onSurface),
) {
    Row(
        modifier = modifier
            .height(IconBtnSize)
            .clip(MaterialTheme.shapes.medium)
            .background(color = background)
            .clickable(onClick = onClick)
            .padding(horizontal = LargePadding, vertical = MediumPadding),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Image(
            painter = rememberImagePainter(data = icon),
            contentDescription = icon.toString(),
            modifier = Modifier.size(IconInInfoSize)
        )
        Spacer(modifier = Modifier.width(LargePadding))
        Text(
            text = stringResource(id = text),
            style = textStyle,
        )
    }
}

private val EmptyBtnAction = {}

fun Modifier.defaultBtnModifier() = this
    .fillMaxWidth()
    .padding(start = XLargePadding, end = XLargePadding, bottom = XLargePadding)
    .height(IconBtnSize)