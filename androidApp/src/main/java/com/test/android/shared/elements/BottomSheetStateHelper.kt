package com.test.android.shared.elements

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.test.android.shared.LargePadding

@Composable
fun BottomSheetColumn(
    modifier: Modifier = Modifier,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = modifier.background(
            MaterialTheme.colors.surface,
            bottomSheetClip
        ),
        content = content
    )
}

val bottomSheetClip = RoundedCornerShape(topStart = LargePadding, topEnd = LargePadding)