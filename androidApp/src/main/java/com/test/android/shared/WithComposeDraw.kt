package com.test.android.shared

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.Composable
import com.common.feature.navigation.api.Destinations
import kotlin.reflect.KClass

@OptIn(ExperimentalAnimationApi::class)
interface WithComposeDraw {
    @Composable
    fun OnDrawCompose()

    @Composable
    fun BottomSheetView() {
    }

    val parentClass: KClass<out Destinations>
}