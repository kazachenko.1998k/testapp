package com.test.android.shared.elements

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp
import com.test.android.shared.DesignTextPrimary
import com.test.android.shared.progressSemantic

@Composable
fun DottedProgressBar(
    modifier: Modifier = Modifier,
    size: Int = 3,
    duration: Int = 1000,
    selectedColor: Color = DesignTextPrimary,
    unselectedColor: Color = selectedColor.copy(alpha = 0.5f),
) {
    val infiniteTransition = rememberInfiniteTransition()
    /*create list anim*/
    val colorAnim = (0 until size).map { boxIndex ->
        /*create color infinity anim*/
        infiniteTransition.animateColor(
            initialValue = unselectedColor,
            targetValue = selectedColor,
            infiniteRepeatable(
                /*for queue analog divide by key frame
                * for example if boxIndex=2, size=3, duration=1000
                * list keyframes will by
                * in 0 sec unselectedColor - start anim all boxs will be unselectedColor
                * in 333 sec unselectedColor
                * in 666 sec selectedColor
                * in 1000 sec unselectedColor
                * in 1333 sec unselectedColor - end anim all boxs will be unselectedColor
                * */
                keyframes {
                    durationMillis = duration
                    (0..size + 1).forEach { key ->
                        val color = if (key - 1 == boxIndex) {
                            selectedColor
                        } else {
                            unselectedColor
                        }
                        color at duration / (size + 1) * key
                    }
                },
            )
        )
    }
    Row(
        modifier = modifier.semantics {
            set(progressSemantic, Unit)
        },
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
    ) {
        colorAnim.forEach {
            Box(
                modifier = Modifier
                    .padding(horizontal = dotPadding / 2, vertical = dotPadding)
                    .size(dotSize)
                    .clip(dotShape)
                    .background(it.value)
            )
        }
    }
}

private val dotSize = 8.dp
private val dotCorner = 2.dp
private val dotShape = RoundedCornerShape(dotCorner)
private val dotPadding = 8.dp