package com.test.android.shared

import androidx.compose.ui.graphics.Color

val DesignBackground = Color(0xFF0C0921)
val DesignBlack = Color(0xFF000000)
val DesignBlack80 = DesignBlack.copy(alpha = 0.80f)
val DesignGreen = Color(0xFF6ADC91)
val DesignPrimary = Color(0xFF645AE4)
val DesignPrimaryLight = Color(0xFF9B87FF)
val DesignRed = Color(0xFFE44E4C)
val DesignSecondary = Color(0xFFFCAE32)
val DesignSecondaryBtn = Color(0xFF29264E)
val DesignSurface = Color(0xFF1F1D33)
val DesignUpperSurface = Color(0xFF29273C)
val DesignTextDescription = Color(0xFFA9A9C6)
val DesignTextDescription15 = DesignTextDescription.copy(alpha = 0.15f)
val DesignTextPrimary = Color(0xFFF1F1F1)
val DesignTransparent = Color(0x00112233)
val DesignLine1 = Color(0xFF4D7DFB)

val FBBackground = Color(0xFF485A96)
val GoogleBackground = Color(0xFFFFFFFF)
val FBText = Color(0xFFFFFFFF)
val GoogleText = Color(0xFF191F2D)

val darkPrimary: Color = DesignPrimary
val darkPrimaryVariant: Color = DesignPrimary
val darkSecondary: Color = DesignSecondary
val darkSecondaryVariant: Color = DesignSecondary
val darkBackground: Color = DesignBackground
val darkSurface: Color = DesignSurface
val darkError: Color = DesignRed
val darkOnPrimary: Color = DesignTextPrimary
val darkOnSecondary: Color = DesignTextPrimary
val darkOnBackground = DesignTextPrimary
val darkOnSurface = DesignTextPrimary
val darkOnError = DesignTextPrimary

val String.color
    get() = Color(android.graphics.Color.parseColor(this))