package com.test.android.shared

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.Insets
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.coerceEachDimensionAtLeast
import com.google.accompanist.insets.rememberInsetsPaddingValues

val XSmallPadding = 2.dp
val SmallPadding = 4.dp
val TextMediumPadding = 6.dp
val MediumPadding = 8.dp
val LargePadding = 12.dp
val XLargePadding = 16.dp
val XXLargePadding = 24.dp
val XXXLargePadding = 32.dp

val DefaultElevation = 4.dp

val SmallPaddings = PaddingValues(
    start = MediumPadding,
    top = SmallPadding,
    end = MediumPadding,
    bottom = SmallPadding
)
val MediumPaddings = PaddingValues(
    start = LargePadding,
    top = MediumPadding,
    end = LargePadding,
    bottom = MediumPadding
)
val LargePaddings = PaddingValues(
    start = XLargePadding,
    top = LargePadding,
    end = XLargePadding,
    bottom = LargePadding
)

val SpaceBetweenBlock = 32.dp
val ToolbarHeight = 56.dp
val HaveAccountQuestionTitlePaddingTop = ToolbarHeight + LargePadding
val ChartDividerHeight = 2.dp
val ChartInMyRobotCardHeight = 72.dp
val ChartInMyRobotCardPlaceholderHeight = 54.dp
val ChartInTopRobotBigCardWidth = 114.dp
val ChartInMyRobotCardBottomSheetWidth = 146.dp
val ChartInTopRobotBigCardHeight = 80.dp
val RobotNameInMyRobotCardWidth = 133.dp
val BalanceChartHeight = 92.dp
val BalanceChartHeightBig = 140.dp
val BigChartHeight = 171.dp
val TimeChartHeight = 15.dp
val DoubleChartPartHeight = 105.dp
val HeaderHeight = ToolbarHeight
val IconBtnSize = 48.dp
val IconInInfoSize = 24.dp
val IconBigSize = 40.dp
val IconNextSize = 16.dp
val SmallIconBtnSize = 40.dp
val TextFieldHeightSmall = SmallIconBtnSize
val DropdownItemHeight = 52.dp
val TextFieldHeightDefault = DropdownItemHeight
val CodeEmailTextFieldSize = 56.dp
val SmallTokenIconSize = 20.dp
val MediumTokenIconSize = 24.dp
val NormalTokenIconSize = SmallTokenIconSize * 2
val MyRobotCardHeight = 136.dp
val MyRobotCardWidth = 272.dp
val NotRobotsPlaceholderHeight = 64.dp
val NotCoinsPlaceholderHeight = 40.dp
val DashboardAdsIconSize = 105.dp
val LinearProgressWidth = 98.dp
val LinearProgressHeight = 8.dp
val BottomBarHeight = 56.dp
val InfoRowVerticalPadding = 14.dp
val SelectorIconSize = 9.dp


fun Modifier.imeMinPadding(): Modifier = composed {
    padding(
        rememberInsetsPaddingValues(
            insets = (LocalWindowInsets.current.ime - LocalWindowInsets.current.navigationBars)
                .coerceEachDimensionAtLeast(Insets.Empty),
            applyStart = true,
            applyEnd = true,
            applyBottom = true
        )
    )
}
