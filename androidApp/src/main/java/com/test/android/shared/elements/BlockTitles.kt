package com.test.android.shared.elements

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import coil.compose.rememberImagePainter
import com.test.android.R
import com.test.android.shared.DesignTextPrimary
import com.test.android.shared.Headline3_20Bold
import com.test.android.shared.Headline4_17Medium
import com.test.android.shared.XLargePadding


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BlockTitleWithBtn(
    title: Int,
    btnTitle: Int,
    onBtnClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = stringResource(id = title),
            style = Headline3_20Bold.copy(color = DesignTextPrimary),
        )
        Spacer(modifier = Modifier.weight(1f))
        Selector(
            onClick = onBtnClick,
            textSelector = stringResource(id = btnTitle),
            modifier = Modifier
                .align(Alignment.CenterVertically),
            painter = rememberImagePainter(data = R.drawable.ic_arrow_next)
        )
    }
}

@Composable
fun BlockTitle(
    title: Int,
    modifier: Modifier = Modifier,
) {
    Text(
        text = stringResource(id = title),
        style = Headline3_20Bold.copy(color = DesignTextPrimary),
        modifier = modifier.padding(horizontal = XLargePadding)
    )
}


@Composable
fun BlockSubtitle(title: Int) {
    Text(
        text = stringResource(id = title),
        style = Headline4_17Medium.copy(color = DesignTextPrimary),
        modifier = Modifier.padding(horizontal = XLargePadding)
    )
}

