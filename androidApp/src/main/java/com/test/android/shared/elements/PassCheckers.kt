package com.test.android.shared.elements

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.common.feature.auth.api.data.models.ErrorCheckState
import com.test.android.R
import com.test.android.shared.DesignGreen
import com.test.android.shared.DesignTextDescription
import com.test.android.shared.Subtitle_14Regular
import com.test.android.shared.XLargePadding


@Composable
fun CheckView(
    checkState: ErrorCheckState,
    text: Int
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Crossfade(
            targetState = checkState,
            modifier = Modifier.padding(start = XLargePadding)
        ) { check ->
            when (check) {
                ErrorCheckState.SUCCESS -> Icon(
                    painter = rememberImagePainter(data = R.drawable.ic_check),
                    contentDescription = R.drawable.ic_check.toString(),
                    tint = DesignGreen
                )
                ErrorCheckState.NOT_CHECK -> Icon(
                    painter = rememberImagePainter(data = R.drawable.ic_check),
                    contentDescription = R.drawable.ic_check.toString(),
                    tint = DesignTextDescription
                )
                ErrorCheckState.ERROR -> Icon(
                    painter = rememberImagePainter(data = R.drawable.ic_clear),
                    contentDescription = R.drawable.ic_clear.toString(),
                    tint = MaterialTheme.colors.error
                )
            }
        }
        Spacer(modifier = Modifier.width(5.dp))
        Text(
            text = stringResource(id = text),
            style = Subtitle_14Regular,
            color = if (checkState == ErrorCheckState.ERROR) MaterialTheme.colors.error else DesignTextDescription
        )
    }
}
