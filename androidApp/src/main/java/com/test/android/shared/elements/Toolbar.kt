package com.test.android.shared.elements

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.IntSize
import coil.compose.rememberImagePainter
import com.common.utils.models.ProfitState
import com.test.android.R
import com.test.android.shared.*


@Composable
fun ToolbarEmpty(
    modifier: Modifier = Modifier,
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        modifier
            .fillMaxWidth()
            .height(ToolbarHeight)
    ) {
        content.invoke(this)
    }
}


@Composable
fun ToolbarWithBackIcon(
    onClick: () -> Unit,
    icon: Int = R.drawable.ic_back,
    content: @Composable RowScope.() -> Unit = {}
) {
    ToolbarEmpty {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(ToolbarHeight),
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(
                modifier = Modifier
                    .padding(horizontal = MediumPadding),
                onClick = onClick
            ) {
                Icon(
                    painter = rememberImagePainter(data = icon),
                    contentDescription = icon.toString(),
                    modifier = Modifier
                        .padding(MediumPadding),
                    tint = MaterialTheme.colors.onBackground
                )
            }
            content.invoke(this)
        }
    }
}
