package com.test.android.shared.elements

import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.test.android.R
import com.test.android.shared.DesignUpperSurface
import com.test.android.shared.MediumPadding

@Composable
fun ColumnScope.BottomSheetTopView() {
    Icon(
        painter = rememberImagePainter(data = R.drawable.ic_bottom_sheet_header),
        contentDescription = R.drawable.ic_bottom_sheet_header.toString(),
        tint = DesignUpperSurface,
        modifier = Modifier
            .align(Alignment.CenterHorizontally)
            .padding(MediumPadding)
            .size(
                width = BottomSheetTopViewWidth,
                height = BottomSheetTopViewHeight
            )
    )
}

private val BottomSheetTopViewWidth = 32.dp
private val BottomSheetTopViewHeight = 4.dp