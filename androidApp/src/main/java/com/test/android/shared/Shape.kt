package com.test.android.shared

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

val CustomShapes = Shapes(
    small = RoundedCornerShape(MediumPadding),
    medium = RoundedCornerShape(LargePadding),
    large = RoundedCornerShape(LargePadding)
)

val TimeSelectorShape = RoundedCornerShape(32.dp)
