package com.test.android.shared.elements

import android.annotation.SuppressLint
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontFamily.Companion.Monospace
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.common.core.mviabstraction.Action
import com.common.core.mviabstraction.Effect
import com.common.core.mviabstraction.Store
import com.common.core.mviabstraction.helpers.ClearFocusFunctionality
import com.common.feature.auth.api.ui.VerifyCodeStore.Companion.CODE_LENGTH_BY_DEFAULT
import com.test.android.R
import com.test.android.shared.*
import com.test.android.utils.CodeTransformation
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterIsInstance

@Composable
@SuppressLint("ModifierParameter")
fun EditTextWithTitle(
    modifier: Modifier = Modifier.defaultBtnModifier(),
    title: String,
    onEdit: (String) -> Unit,
    onFinishEdit: () -> Unit = {},
    onStartEdit: () -> Unit = {},
    text: String,
    lastElement: Boolean = false,
    hint: String? = null,
    hasError: Boolean = false,
    textAlign: TextAlign = TextAlign.Start,
    visualTransformation: VisualTransformation? = null,
    keyboardType: KeyboardType = KeyboardType.Email,
    isPasswordMode: Boolean = keyboardType == KeyboardType.Password,
    onChangeVisibleState: (() -> Unit)? = null,
) {
    Column {
        Text(
            text = title,
            style = Subtitle_14Regular,
            modifier = Modifier.padding(
                start = XLargePadding,
                end = XLargePadding,
                bottom = MediumPadding
            ),
            color = DesignTextDescription
        )
        EditText(
            modifier = modifier,
            onEdit = onEdit,
            onFinishEdit = onFinishEdit,
            onStartEdit = onStartEdit,
            text = text,
            hint = hint,
            lastElement = lastElement,
            hasError = hasError,
            textAlign = textAlign,
            visualTransformation = visualTransformation,
            keyboardType = keyboardType,
            isPasswordMode = isPasswordMode,
            onChangeVisibleState = onChangeVisibleState
        )
    }
}

@Composable
@SuppressLint("ModifierParameter")
fun EditText(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .padding(start = XLargePadding, end = XLargePadding, bottom = XLargePadding)
        .height(TextFieldHeightDefault),
    onEdit: (String) -> Unit,
    onFinishEdit: () -> Unit = {},
    onStartEdit: () -> Unit = {},
    text: String,
    hint: String? = null,
    lastElement: Boolean = false,
    hasError: Boolean = false,
    keyboardType: KeyboardType = KeyboardType.Email,
    isPasswordMode: Boolean = keyboardType == KeyboardType.Password,
    onChangeVisibleState: (() -> Unit)? = null,
    textAlign: TextAlign = TextAlign.Start,
    visualTransformation: VisualTransformation? = null,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
) {
    EditTextWithEndContent(
        modifier = modifier,
        onEdit = onEdit,
        onFinishEdit = onFinishEdit,
        onStartEdit = onStartEdit,
        text = text,
        hint = hint,
        lastElement = lastElement,
        hasError = hasError,
        keyboardType = keyboardType,
        isPasswordMode = isPasswordMode,
        textAlign = textAlign,
        visualTransformation = visualTransformation,
        interactionSource = interactionSource,
    ) {
        val hasPassChangeIcon =
            keyboardType == KeyboardType.Password && onChangeVisibleState != null
        if (hasPassChangeIcon) {
            Crossfade(
                targetState = isPasswordMode,
                modifier = Modifier
                    .padding(end = XLargePadding)
                    .clip(MaterialTheme.shapes.small)
                    .clickable { onChangeVisibleState!!.invoke() }
            ) { hide ->
                if (hide) {
                    Icon(
                        painter = rememberImagePainter(data = R.drawable.ic_hide_pass),
                        contentDescription = R.drawable.ic_hide_pass.toString(),
                        tint = DesignTextDescription
                    )
                } else {
                    Icon(
                        painter = rememberImagePainter(data = R.drawable.ic_show_pass),
                        contentDescription = R.drawable.ic_show_pass.toString(),
                    )
                }
            }
        }
    }
}

@Composable
@SuppressLint("ModifierParameter")
fun EditTextWithQR(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .padding(start = XLargePadding, end = XLargePadding, bottom = XLargePadding)
        .height(TextFieldHeightDefault),
    onEdit: (String) -> Unit,
    onFinishEdit: () -> Unit = {},
    onStartEdit: () -> Unit = {},
    text: String,
    hint: String? = null,
    lastElement: Boolean = false,
    hasError: Boolean = false,
    keyboardType: KeyboardType = KeyboardType.Email,
    isPasswordMode: Boolean = keyboardType == KeyboardType.Password,
    onQrClick: () -> Unit = {},
    textAlign: TextAlign = TextAlign.Start,
    visualTransformation: VisualTransformation? = null,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
) {
    EditTextWithEndContent(
        modifier = modifier,
        onEdit = onEdit,
        onFinishEdit = onFinishEdit,
        onStartEdit = onStartEdit,
        text = text,
        hint = hint,
        lastElement = lastElement,
        hasError = hasError,
        keyboardType = keyboardType,
        isPasswordMode = isPasswordMode,
        textAlign = textAlign,
        visualTransformation = visualTransformation,
        interactionSource = interactionSource,
    ) {
        Box(
            modifier = Modifier
                .padding(vertical = PaddingQR)
                .padding(end = PaddingQR)
                .fillMaxHeight()
                .clip(MaterialTheme.shapes.small)
                .background(color = DesignSecondaryBtn)
                .clickable(onClick = onQrClick)
                .padding(horizontal = MediumPadding),
        ) {
            Image(
                painter = rememberImagePainter(data = R.drawable.ic_qr_code),
                contentDescription = R.drawable.ic_qr_code.toString(),
                modifier = Modifier
                    .size(IconInInfoSize)
                    .align(Center)
            )
        }
    }
}


@Composable
@SuppressLint("ModifierParameter")
fun EditTextWithEndContent(
    modifier: Modifier = Modifier
        .fillMaxWidth()
        .padding(start = XLargePadding, end = XLargePadding, bottom = XLargePadding)
        .height(TextFieldHeightDefault),
    onEdit: (String) -> Unit,
    onFinishEdit: () -> Unit = {},
    onStartEdit: () -> Unit = {},
    text: String,
    hint: String? = null,
    lastElement: Boolean = false,
    hasError: Boolean = false,
    keyboardType: KeyboardType = KeyboardType.Email,
    isPasswordMode: Boolean = keyboardType == KeyboardType.Password,
    textAlign: TextAlign = TextAlign.Start,
    visualTransformation: VisualTransformation? = null,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    backgroundColor: Color = MaterialTheme.colors.surface,
    content: @Composable RowScope.() -> Unit = {}
) {
    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }
    var hasFocus by remember { mutableStateOf(false) }
    val errorColor = MaterialTheme.colors.error
    val focusColor = MaterialTheme.colors.primary
    val defaultColor = DesignTransparent
    val borderColor = remember(hasError, hasFocus) {
        when {
            hasError -> errorColor
            hasFocus -> focusColor
            else -> defaultColor
        }
    }
    Row(
        modifier = modifier
            .background(backgroundColor, MaterialTheme.shapes.medium)
            .border(
                width = (0.5).dp,
                color = borderColor,
                shape = MaterialTheme.shapes.medium
            )
            .pointerInput(Unit) {
                detectTapGestures(onPress = {
                    focusRequester.requestFocus()
                })
            },
        verticalAlignment = CenterVertically
    ) {
        EndShadowView(
            background = if (hasFocus) DesignTransparent else backgroundColor,
            modifier = Modifier
                .padding(horizontal = XLargePadding)
                .weight(1f)
        ) {
            if (text.isEmpty()) {
                hint?.let {
                    Text(
                        text = it,
                        style = Body1_16Regular.copy(
                            color = DesignTextDescription.copy(alpha = if (hasFocus) 0.3f else 1.0f),
                            textAlign = textAlign
                        ),
                        modifier = Modifier
                            .align(Alignment.CenterStart)
                            .fillMaxWidth()
                    )
                }
            }
            BasicTextField(
                value = text,
                onValueChange = onEdit,
                textStyle = Body1_16Medium.copy(
                    color = MaterialTheme.colors.onSurface,
                    textAlign = textAlign
                ),
                cursorBrush = SolidColor(if (hasError) MaterialTheme.colors.error else MaterialTheme.colors.primary),
                keyboardOptions = KeyboardOptions(
                    keyboardType = keyboardType,
                    imeAction = if (lastElement) ImeAction.Done else ImeAction.Next
                ),
                keyboardActions = KeyboardActions(
                    onDone = { focusManager.clearFocus() },
                    onNext = { focusManager.moveFocus(FocusDirection.Down) }
                ),
                visualTransformation = visualTransformation
                    ?: if (isPasswordMode) {
                        PasswordVisualTransformation()
                    } else {
                        VisualTransformation.None
                    },
                singleLine = true,
                interactionSource = interactionSource,
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .fillMaxWidth()
                    .semantics {
                        set(
                            hintSemantic,
                            hint ?: ""
                        )
                        set(
                            borderSemantic,
                            borderColor
                        )
                    }
                    .focusRequester(focusRequester)
                    .onFocusChanged { focusState ->
                        when {
                            focusState.isFocused || focusState.isCaptured -> {
                                if (!hasFocus) {
                                    onStartEdit.invoke()
                                }
                                hasFocus = true
                            }
                            hasFocus -> {
                                hasFocus = false
                                onFinishEdit.invoke()
                            }
                        }
                    }
            )
        }
        content.invoke(this)
    }
}

@Composable
fun CodeEditText(
    codeNow: String,
    onEdit: (String) -> Unit,
    onFullData: () -> Unit = {},
) {
    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = XLargePadding),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BasicTextField(
            value = codeNow,
            onValueChange = onEdit,
            textStyle = Large_Titles1_32Bold.copy(
                color = MaterialTheme.colors.onSurface,
                fontFamily = Monospace,
                letterSpacing = CodeLetter,
                textAlign = TextAlign.Center
            ),
            cursorBrush = SolidColor(MaterialTheme.colors.primary),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.NumberPassword,
                imeAction = ImeAction.Done
            ),
            keyboardActions = KeyboardActions(
                onDone = { focusManager.clearFocus() },
            ),
            visualTransformation = CodeTransformation(
                trailingText = StringBuilder("_").repeat(
                    (CODE_LENGTH_BY_DEFAULT - codeNow.length).coerceAtLeast(0)
                )
            ),
            singleLine = true,
            modifier = Modifier.focusRequester(focusRequester)
        )
    }
    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }
}

fun Modifier.clearFocusOnTouch(): Modifier = composed {
    val focusManager = LocalFocusManager.current
    this.pointerInput(Unit) {
        detectTapGestures(onPress = {
            focusManager.clearFocus()
        })
    }
}


fun Modifier.clearFocusOnEvent(store: Store<out com.common.core.mviabstraction.State, out Action, out Effect>): Modifier =
    composed {
        val focusManager = LocalFocusManager.current
        LaunchedEffect(store) {
            store.observeSideEffect().filterIsInstance<ClearFocusFunctionality.ClearEffect>()
                .collect {
                    focusManager.clearFocus()
                }
        }
        this
    }

private val PaddingQR = TextMediumPadding