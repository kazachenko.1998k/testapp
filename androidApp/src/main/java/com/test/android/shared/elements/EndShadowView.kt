package com.test.android.shared.elements

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import com.test.android.shared.DesignTransparent
import com.test.android.shared.XXXLargePadding

@Composable
fun EndShadowView(
    modifier: Modifier = Modifier,
    background: Color = MaterialTheme.colors.background,
    content: @Composable BoxScope.() -> Unit = {}
) {
    Box(modifier) {
        content.invoke(this)
        Box(
            Modifier.matchParentSize()
        ) {
            Box(
                Modifier
                    .width(XXXLargePadding)
                    .fillMaxHeight()
                    .align(Alignment.CenterEnd)
                    .background(
                        Brush.horizontalGradient(
                            listOf(DesignTransparent, background)
                        )
                    )
            )
        }
    }
}