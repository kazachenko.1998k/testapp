package com.test.android.shared.elements

import android.annotation.SuppressLint
import androidx.annotation.PluralsRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.common.utils.DAY_IN_MS
import com.common.utils.models.ProfitState
import com.test.android.shared.*
import java.text.SimpleDateFormat
import java.util.*


@Composable
fun TitleText(
    modifier: Modifier = Modifier.padding(
        horizontal = XLargePadding,
        vertical = MediumPadding
    ),
    id: Int
) {
    TitleText(modifier, stringResource(id = id))
}

@Composable
fun TitleText(
    modifier: Modifier = Modifier.padding(
        horizontal = XLargePadding,
        vertical = MediumPadding
    ),
    text: String
) {
    Text(
        text,
        modifier = modifier,
        style = MaterialTheme.typography.h1.copy(color = DesignTextPrimary)
    )
}

@SuppressLint("ModifierParameter")
@Composable
fun ScreenTitleText(
    text: String,
    modifier: Modifier = Modifier.padding(
        start = XLargePadding,
        end = XLargePadding,
        top = XXXLargePadding,
        bottom = XXLargePadding
    )
) {
    Text(
        text,
        modifier = modifier,
        style = Large_Titles1_32Bold.copy(color = DesignTextPrimary)
    )
}

@Composable
fun ScreenTitleTextWithIcon(text: String, icon: Any) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(
            horizontal = XLargePadding,
            vertical = XXLargePadding,
        )
    ) {
        Image(
            painter = rememberImagePainter(data = icon),
            contentDescription = icon.toString(),
            modifier = Modifier
                .padding(end = LargePadding)
                .size(ScreenTitleIconSize),
        )
        Text(
            text,
            style = Headline2_24Bold.copy(color = DesignTextPrimary)
        )
    }
}

@Composable
fun SubTitleText(id: Int) {
    Text(
        stringResource(id = id),
        modifier = Modifier.padding(
            start = XLargePadding,
            end = XLargePadding,
            bottom = XXLargePadding
        ),
        style = Headline4_17Regular.copy(color = DesignTextDescription)
    )
}

fun timeText(timeMS: Long, locale: Locale, todayText: String, yesterdayText: String): String {
    val dateFormat = SimpleDateFormat("dd.MM.yy", locale)
    val nowTime = System.currentTimeMillis()
    val today = dateFormat.format(nowTime)
    val yesterday = dateFormat.format(nowTime - DAY_IN_MS)
    return when (val rememberText = dateFormat.format(timeMS)) {
        today -> todayText
        yesterday -> yesterdayText
        else -> rememberText
    }
}


@Composable
fun TextWithClickAreaById(
    original: Int,
    args: Map<Int, () -> Unit>,
    modifier: Modifier = Modifier,
    styleDefault: TextStyle = LocalTextStyle.current,
    styleSelect: TextStyle = styleDefault.copy(color = DesignPrimaryLight)
) {
    val mapStringId = args.map { (key, value) -> stringResource(id = key) to value }.toMap()
    TextWithClickAreaByText(
        original = original,
        args = mapStringId.toList(),
        modifier = modifier,
        styleDefault = styleDefault,
        styleSelect = styleSelect,
    )
}

@Composable
fun TextWithClickAreaByText(
    original: Int,
    args: List<Pair<String, () -> Unit>>,
    modifier: Modifier = Modifier,
    styleDefault: TextStyle = LocalTextStyle.current,
    styleSelect: TextStyle = styleDefault.copy(color = DesignPrimaryLight)
) {
    val valuesMap = args.map { it.first }
    val originalText = stringResource(id = original, *valuesMap.toTypedArray())
    val annotatedText = buildAnnotatedString {
        append(originalText)
        valuesMap.forEach {
            val startIndex = originalText.indexOf(it)
            addStyle(
                style = styleSelect.toSpanStyle(),
                start = startIndex,
                end = startIndex + it.length
            )
        }
    }
    ClickableText(
        modifier = modifier,
        text = annotatedText,
        style = styleDefault,
        onClick = { offset ->
            valuesMap.forEach { str ->
                val startIndex = originalText.indexOf(str)
                if (offset in startIndex..startIndex + str.length) {
                    args.firstOrNull { it.first == str }?.second?.invoke()
                }
            }
        }
    )
}


@Composable
fun TextOnlyClickArea(
    original: Int,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    styleSelect: TextStyle = LocalTextStyle.current.copy(color = DesignPrimaryLight)
) {
    val originalText = stringResource(id = original)
    ClickableText(
        modifier = modifier,
        text = AnnotatedString(originalText),
        style = styleSelect,
        onClick = {
            onClick.invoke()
        }
    )
}

@Composable
fun TextStyle.SetColorByState(state: ProfitState) =
    copy(
        color = when (state) {
            ProfitState.Positive -> DesignGreen
            ProfitState.Negative -> DesignRed
            ProfitState.Equals -> DesignTextDescription
        }
    )



@Composable
fun pluralResource(
    @PluralsRes resId: Int,
    quantity: Int,
    vararg formatArgs: Any? = emptyArray()
): String = LocalContext.current.resources.getQuantityString(resId, quantity, *formatArgs)

internal val ScreenTitleIconSize = 32.dp