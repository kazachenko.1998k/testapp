package com.test.android.shared.elements

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import coil.compose.rememberImagePainter
import com.common.core.mviabstraction.ActionEffect
import com.common.core.mviabstraction.ErrorEffect
import com.common.core.mviabstraction.WarningEffect
import com.common.core.mviabstraction.usecases.BannerUseCase
import com.common.utils.BackendException
import com.common.utils.models.*
import com.test.android.R
import com.test.android.shared.*
import com.test.android.shared.elements.BannerType.Error
import com.test.android.shared.elements.BannerType.Network
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlin.reflect.KClass

@Composable
fun ErrorBanner(
    hostState: SnackbarHostState
) {
    SwipeableBanner(
        hostState = hostState,
        backgroundColor = MaterialTheme.colors.error
    ) { bannerData ->
        Text(
            text = bannerData.message,
            style = Subtitle_14Regular.copy(color = MaterialTheme.colors.onError),
            modifier = Modifier.padding()
        )
    }
}

@Composable
fun NetworkBanner(
    hostState: SnackbarHostState
) {
    SwipeableBanner(
        hostState = hostState,
        backgroundColor = DesignUpperSurface
    ) { bannerData ->
        Row {
            Image(
                painter = rememberImagePainter(data = R.drawable.ic_network_snackbar),
                contentDescription = null,
                modifier = Modifier.size(XXLargePadding)
            )
            Text(
                text = bannerData.message,
                style = Subtitle_14Regular.copy(color = MaterialTheme.colors.onSurface),
                modifier = Modifier.padding(start = LargePadding)
            )
        }
    }
}

@Composable
fun WarningBanner(
    hostState: SnackbarHostState,
) {
    SwipeableBanner(
        hostState = hostState,
        backgroundColor = DesignUpperSurface
    ) { bannerData ->
        Row {
            Image(
                painter = rememberImagePainter(data = R.drawable.ic_warning_snack),
                contentDescription = null,
                modifier = Modifier.size(XXLargePadding)
            )
            Column {
                Text(
                    text = bannerData.message,
                    style = Subtitle_14Regular.copy(color = MaterialTheme.colors.onSurface),
                    modifier = Modifier.padding(start = LargePadding)
                )
            }
        }
    }
}

@SuppressLint("FlowOperatorInvokedInComposition")
@Composable
fun WithBannerListener(bannerUseCase: BannerUseCase) {
    WithBannerListener(
        errorFlow = bannerUseCase.bannerDataFlow,
    )
}

@SuppressLint("FlowOperatorInvokedInComposition")
@Composable
fun WithBannerListener(errorFlow: Flow<Banner>) {

    val bannerHostState = remember { SnackbarHostState() }
    val bufferBanner = remember { mutableSetOf<BannerDataLocal>() }
    val bannersDataFilter =
        remember { MutableSharedFlow<BannerDataLocal>(extraBufferCapacity = Int.MAX_VALUE) }

    /*Manager banners*/
    val banners = remember {
        errorFlow.transform { error ->
            when (error) {
                is HideNetworkError -> {
                    bannerHostState.currentSnackbarData?.dismiss()
                }
                else -> emit(error)
            }
        }
    }

    val bannersData = banners.setupBannerParams()

    /*Filter equals banners*/
    LaunchedEffect(Unit) {
        bannersData.collect { data ->
            if (bufferBanner.any { it.message == data.message }.not()) {
                bufferBanner.add(data)
                bannersDataFilter.emit(data)
            }
        }
    }
    BannerShower(
        bannersDataFilter,
        bannerHostState,
        bufferBanner
    )
}

@Composable
fun BannerShower(
    banners: Flow<BannerDataLocal>,
    bannerHostState: SnackbarHostState,
    bufferBanner: MutableSet<BannerDataLocal>
) {
    var bannerType by remember { mutableStateOf(Error) }
    /*Shower banners*/
    LaunchedEffect(Unit) {
        banners.collect { banner ->
            bannerType = banner.type
            val bannerResult = bannerHostState.showSnackbar(
                message = banner.message,
                duration = banner.duration,
                actionLabel = banner.actionLabel,
            )
            bufferBanner.remove(banner)
            if (bannerResult == SnackbarResult.ActionPerformed) {
                banner.action?.invoke()
            }
        }
    }

    when (bannerType) {
        Error -> ErrorBanner(hostState = bannerHostState)
        Network -> NetworkBanner(hostState = bannerHostState)
        BannerType.Warning -> WarningBanner(hostState = bannerHostState)
    }
}

@Composable
fun Flow<Banner>.setupBannerParams(): Flow<BannerDataLocal> {
    val defaultText = stringResource(id = R.string.something_then_wrong)
    val errorTexts = errorText()
    val actionTexts = actionText()
    return remember {
        map { banner ->
            val type = when (banner) {
                is NetworkBanner -> Network
                is WarningEffect -> BannerType.Warning
                else -> Error
            }

            val throwable = when (banner) {
                is ErrorEffect -> banner.error
                is WarningEffect -> banner.error
                is ActionEffect -> banner.error
                else -> banner
            }

            val localText = errorTexts[throwable::class]
            val textFromError = (throwable as? BackendException)?.message
            val message = localText ?: textFromError ?: defaultText

            val actionTextForActionError =
                (banner as? ActionEffect)?.error?.let { actionTexts[it::class] }
            BannerDataLocal(
                type = type,
                message = message,
                actionLabel = actionTextForActionError,
                action = (banner as? ActionEffect)?.action
            )
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SwipeableBanner(
    hostState: SnackbarHostState,
    backgroundColor: Color = SnackbarDefaults.backgroundColor,
    content: @Composable (SnackbarData) -> Unit
) {
    var size by remember { mutableStateOf(Size.Zero) }
    val swipeableState = rememberSwipeableState(SwipeDirection.Initial)
    val scope = rememberCoroutineScope()
    val height = remember(size) {
        if (size.height == 0f) {
            1f
        } else {
            size.height
        }
    }
    if (swipeableState.isAnimationRunning) {
        DisposableEffect(Unit) {
            onDispose {
                when (swipeableState.currentValue) {
                    SwipeDirection.Top -> {
                        hostState.currentSnackbarData?.dismiss()
                        scope.launch {
                            delay(ANIM_DELAY)
                            swipeableState.snapTo(SwipeDirection.Initial)
                        }
                    }
                    else -> {
                        return@onDispose
                    }
                }
            }
        }
    }
    val offset = with(LocalDensity.current) {
        swipeableState.offset.value.toDp()
    }
    SnackbarHost(
        hostState = hostState,
        modifier = Modifier
            .onSizeChanged { size = Size(it.width.toFloat(), it.height.toFloat()) }
            .swipeable(
                state = swipeableState,
                anchors = mapOf(
                    -height to SwipeDirection.Top,
                    0f to SwipeDirection.Initial,
                ),
                thresholds = { _, _ -> FractionalThreshold(0.3f) },
                orientation = Orientation.Vertical
            ),
        snackbar = { snackbarData ->
            Snackbar(
                modifier = Modifier
                    .padding(XLargePadding)
                    .offset(y = offset),
                backgroundColor = backgroundColor
            ) {
                content.invoke(snackbarData)
            }
        })
}

@Composable
fun errorText(): Map<KClass<out Throwable>, String> =
    mapOf(
        NetworkError::class to stringResource(id = R.string.possible_network_error_text),
        IncorrectEmailOrPassword::class to stringResource(id = R.string.error_incorrect_email_or_password),
        UserNotFound::class to stringResource(id = R.string.error_user_not_found),
        InvalidCredentialsOrEmailAlreadyRegistered::class to stringResource(id = R.string.error_invalid_credentials_or_email_already_registered),
        ErrorSendingCode::class to stringResource(id = R.string.error_sending_code_error),
        WrongResetCode::class to stringResource(id = R.string.error_wrong_reset_code),
        CodeExpired::class to stringResource(id = R.string.error_code_expired),
        InvalidPassword::class to stringResource(id = R.string.error_invalid_password),
        InvalidEmailOrPassword::class to stringResource(id = R.string.error_invalid_email_or_password),
        AccountNotFound::class to stringResource(id = R.string.error_account_not_found),
        Unauthorized::class to stringResource(id = R.string.error_unauthorized),
        CredentialsInvalidNotification::class to stringResource(id = R.string.connection_with_exchange_was_lost),
    )

@Composable
fun actionText(): Map<KClass<out Throwable>, String> =
    mapOf(
        CredentialsInvalidNotification::class to stringResource(id = R.string.connect_exchange_btn),
    )

enum class SwipeDirection {
    Top,
    Initial,
}

enum class BannerType(val duration: SnackbarDuration) {
    Network(SnackbarDuration.Indefinite),
    Warning(SnackbarDuration.Short),
    Error(SnackbarDuration.Short),
}

data class BannerDataLocal(
    val type: BannerType,
    val message: String,
    val actionLabel: String?,
    val action: (() -> Unit)?,
    val duration: SnackbarDuration = type.duration,
)

private const val ANIM_DELAY = 100L