package com.test.android.shared.elements

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.test.android.shared.DesignTextDescription15
import com.test.android.shared.XLargePadding


@Composable
fun Divider(
    modifier: Modifier = Modifier,
    color: Color = DesignTextDescription15
) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .height(0.5.dp)
            .padding(horizontal = XLargePadding)
            .background(color)
    )
}
