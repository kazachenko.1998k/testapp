package com.test.android.shared

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

val Large_Titles1_32Bold  = TextStyle(fontWeight = FontWeight.Bold, fontSize = 32.sp, lineHeight = 38.sp)
val Headline1_28Bold  = TextStyle(fontWeight = FontWeight.Bold, fontSize = 28.sp, lineHeight = 32.sp)
val Headline2_24Bold = TextStyle(fontWeight = FontWeight.Bold, fontSize = 24.sp, lineHeight = 30.sp)
val Headline2_24Medium = TextStyle(fontWeight = FontWeight.Medium, fontSize = 24.sp, lineHeight = 30.sp)
val Headline3_20Bold = TextStyle(
    fontWeight = FontWeight.Bold,
    fontSize = 20.sp,
    lineHeight = 24.sp,
    letterSpacing = (0.38).sp
)
val Headline3_20Medium = TextStyle(
    fontWeight = FontWeight.Medium,
    fontSize = 20.sp,
    lineHeight = 24.sp,
    letterSpacing = (0.01).sp
)
val Headline4_17Medium =
    TextStyle(fontWeight = FontWeight.Medium, fontSize = 17.sp, lineHeight = 24.sp)
val Headline4_17Regular =
    TextStyle(fontWeight = FontWeight.Normal, fontSize = 17.sp, lineHeight = 24.sp)

val Body1_16Bold = TextStyle(
    fontWeight = FontWeight.Bold,
    fontSize = 16.sp,
    lineHeight = 22.sp,
    letterSpacing = (0.15).sp
)
val Body1_16Medium = TextStyle(
    fontWeight = FontWeight.Medium,
    fontSize = 16.sp,
    lineHeight = 22.sp,
    letterSpacing = (0.1).sp
)
val Body1_16Regular = TextStyle(
    fontWeight = FontWeight.Normal,
    fontSize = 16.sp,
    lineHeight = 22.sp,
    letterSpacing = (0.1).sp
)

val Body2_15Medium = TextStyle(
    fontWeight = FontWeight.Medium,
    fontSize = 15.sp,
    lineHeight = 20.sp,
    letterSpacing = (0.2).sp
)
val Body2_15Regular = TextStyle(
    fontWeight = FontWeight.Normal,
    fontSize = 15.sp,
    lineHeight = 20.sp,
    letterSpacing = (-0.1).sp
)
val Subtitle_14Medium = TextStyle(
    fontWeight = FontWeight.Medium,
    fontSize = 14.sp,
    lineHeight = 20.sp,
    letterSpacing = (-0.1).sp
)
val Subtitle_14Regular = TextStyle(fontWeight = FontWeight.Normal, fontSize = 14.sp, lineHeight = 20.sp, letterSpacing = (-0.1).sp)
val Caption1_12Medium = TextStyle(
    fontWeight = FontWeight.Medium,
    fontSize = 12.sp,
    lineHeight = 16.sp,
    letterSpacing = (0.2).sp
)

val CodeLetter = 24.sp

val CustomTypography = Typography(
    h1 = Headline1_28Bold,
    h2 = Headline2_24Medium,
    h3 = Headline3_20Medium,
    h4 = Headline4_17Medium,
    h5 = Headline4_17Regular,
    h6 = Headline4_17Regular,
    subtitle1 = Subtitle_14Medium,
    subtitle2 = Subtitle_14Regular,
    body1 = Body1_16Medium,
    body2 = Body2_15Medium,
    button = Headline4_17Medium,
    caption = Caption1_12Medium,
    overline = Caption1_12Medium
)
