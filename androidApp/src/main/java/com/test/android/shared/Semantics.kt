package com.test.android.shared

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.SemanticsPropertyKey

val hintSemantic = SemanticsPropertyKey<String>("Hint")
val borderSemantic = SemanticsPropertyKey<Color>("Border")
val progressSemantic = SemanticsPropertyKey<Unit>("Progress")