package com.test.android.utils

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetState
import androidx.compose.runtime.Composable
import com.common.feature.navigation.api.usecases.BottomSheetUseCase

interface BottomSheetComposeUseCase : BottomSheetUseCase {

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun Setup(modalBottomSheetState: ModalBottomSheetState)

}