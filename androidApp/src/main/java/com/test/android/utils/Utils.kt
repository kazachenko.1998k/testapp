package com.test.android.utils

import android.content.res.Resources
import android.os.Build

object Utils {
    fun Resources.locale() = (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        configuration.locales.get(0)
    } else {
        configuration.locale
    })!!
}