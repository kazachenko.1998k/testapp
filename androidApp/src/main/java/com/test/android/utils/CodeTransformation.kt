package com.test.android.utils

import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.text.input.VisualTransformation
import com.test.android.shared.DesignTextDescription

class CodeTransformation(
    val trailingText: String = ""
) : VisualTransformation {

    private fun offsetMapping(text: AnnotatedString): OffsetMapping = object : OffsetMapping {

        override fun originalToTransformed(offset: Int): Int =
            if (text.isEmpty()) 0 else offset

        override fun transformedToOriginal(offset: Int): Int = offset

    }

    override fun filter(text: AnnotatedString): TransformedText {
        val result = AnnotatedString.Builder(text).apply {
            append(
                AnnotatedString(
                    trailingText,
                    spanStyle = SpanStyle(
                        color = DesignTextDescription,
                        fontWeight = FontWeight.Normal
                    )
                )
            )
        }.toAnnotatedString()
        return TransformedText(
            result,
            offsetMapping(result)
        )
    }

}