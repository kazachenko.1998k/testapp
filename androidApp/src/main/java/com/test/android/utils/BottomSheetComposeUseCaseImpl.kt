package com.test.android.utils

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetState
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.snapshotFlow
import com.common.core.mviabstraction.usecases.SuspendUseCase
import com.common.feature.navigation.api.usecases.BottomSheetUseCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class BottomSheetComposeUseCaseImpl : BottomSheetComposeUseCase, SuspendUseCase {
    private val actions = MutableSharedFlow<SheetActions>()
    private val _state = MutableStateFlow(BottomSheetUseCase.BottomSheetState.Close)
    private val state = _state.asStateFlow()

    private val _nowVisible = state.map {
        it != BottomSheetUseCase.BottomSheetState.Close
    }.stateIn(this, SharingStarted.Eagerly, false)

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    override fun Setup(modalBottomSheetState: ModalBottomSheetState) {
        val scope = rememberCoroutineScope()
        LaunchedEffect(Unit) {
            actions.collect {
                scope.launch {//work correctly only in compose scope
                    when (it) {
                        SheetActions.CloseSuspend ->
                            modalBottomSheetState.hide()
                        SheetActions.CloseForce ->
                            modalBottomSheetState.snapTo(ModalBottomSheetValue.Hidden)
                        SheetActions.OpenSuspend ->
                            modalBottomSheetState.animateTo(ModalBottomSheetValue.Expanded)
                        SheetActions.OpenForce ->
                            modalBottomSheetState.snapTo(ModalBottomSheetValue.Expanded)
                    }
                }.join()
                waitAndAction()
            }
        }
        LaunchedEffect(Unit) {
            snapshotFlow {
                Pair(modalBottomSheetState.currentValue, modalBottomSheetState.isAnimationRunning)
            }.collect {
                _state.value = when (it) {
                    Pair(ModalBottomSheetValue.Hidden, false) ->
                        BottomSheetUseCase.BottomSheetState.Close
                    Pair(ModalBottomSheetValue.Expanded, false) ->
                        BottomSheetUseCase.BottomSheetState.Open
                    Pair(ModalBottomSheetValue.Hidden, true) ->
                        BottomSheetUseCase.BottomSheetState.Opening
                    Pair(ModalBottomSheetValue.Expanded, true) ->
                        BottomSheetUseCase.BottomSheetState.Closing
                    else -> throw IllegalArgumentException(it.toString())
                }
            }
        }
    }

    override val nowState: StateFlow<BottomSheetUseCase.BottomSheetState> = state

    override val nowVisible: StateFlow<Boolean> = _nowVisible

    override fun canGoBack(): Boolean {
        return if (_nowVisible.value) {
            close()
            false
        } else {
            true
        }
    }

    override fun close() {
        launch { awaitClose() }
    }

    override suspend fun awaitClose() {
        actions.emit(SheetActions.CloseSuspend)
        onFirstClose {}.join()
    }

    override suspend fun closeForce() {
        actions.emit(SheetActions.CloseForce)
        onFirstClose {}.join()
    }

    override fun onFirstClose(block: suspend () -> Unit) = launch {
        state.first { it == BottomSheetUseCase.BottomSheetState.Close }
        block.invoke()
    }

    override fun open() {
        launch { awaitOpen() }
    }

    override suspend fun awaitOpen() {
        actions.emit(SheetActions.OpenSuspend)
        onFirstOpen {}.join()
    }

    override suspend fun openForce() {
        actions.emit(SheetActions.OpenForce)
        onFirstOpen {}.join()
    }

    override fun onFirstOpen(block: suspend () -> Unit) = launch {
        state.first { it == BottomSheetUseCase.BottomSheetState.Open }
        block.invoke()
    }

    enum class SheetActions {
        CloseSuspend,
        CloseForce,
        OpenSuspend,
        OpenForce
    }

    private suspend fun waitAndAction() =
        state.first { it == BottomSheetUseCase.BottomSheetState.Close || it == BottomSheetUseCase.BottomSheetState.Open }


}