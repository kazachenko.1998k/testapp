package com.test.android.test.utils

import androidx.compose.ui.test.ComposeTimeoutException
import androidx.compose.ui.test.junit4.AndroidComposeTestRule

fun AndroidComposeTestRule<*, *>.string(id: Int) =
    activity.getString(id)

fun AndroidComposeTestRule<*, *>.waitUntilHasError(
    timeoutMillis: Long,
    block: () -> Unit
) {
    val startTime = System.nanoTime()
    var lastError: Throwable = ComposeTimeoutException(
        "Condition still not satisfied after $timeoutMillis ms"
    )
    while (try {
            block.invoke()
            false
        } catch (er: AssertionError) {
            lastError = er
            true
        }
    ) {
        //copy/paste from impl waitUntil /////////////////////////////////////////////
        if (mainClock.autoAdvance) {
            mainClock.advanceTimeByFrame()
        }
        // Let Android run measure, draw and in general any other async operations.
        Thread.sleep(10)
        if (System.nanoTime() - startTime > timeoutMillis * 1_000_000) {
            //////////////////////////////////////////////////////////////////////////
            throw lastError
        }
    }
}

fun AndroidComposeTestRule<*, *>.hasError(
    block: () -> Unit
): Boolean = try {
    block.invoke()
    false
} catch (er: AssertionError) {
    true
}