package com.test.android.test.auth

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.test.filters.LargeTest
import com.common.feature.navigation.api.LoginScreen
import com.test.android.R
import com.test.android.test.base.ParamsTestMainActivity
import com.test.android.test.base.SetupParams
import com.test.android.test.utils.*
import org.junit.Test
import org.junit.jupiter.api.DisplayName

class LoginUITest : ParamsTestMainActivity(SetupParams(startDestination = LoginScreen::class)) {

    @Test
    @LargeTest
    @DisplayName("CB-T2")
    fun validLoginAndLogout() = with(testRule) {
        with(LoginTestUtils) {
            login()//Step 1
            goToSettings()//Step 2
            openLogoutDialog()//Step 3
            closeBySwipe()//Step 4
            openLogoutDialog()//Step 5
            logout()//Step 6
            goToLogin()//Step 7
            checkFieldsOnEmpty()//Step 8
            goToRegister()//Step 9
            runOnUiThread { activity.onBackPressed() }//Step 10
            invalidLogin()//Step 10
            incorrectLogin()//Step 11
            validateHideShowPass()//Step 12
        }
    }

}

object LoginTestUtils {
    fun AndroidComposeTestRule<*, *>.login() {
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(validEmail)
        onNode(hasHint(string(R.string.password_title)))
            .performTextReplacement(validPass)
        onNode(hasText(string(R.string.sign_in_title)) and hasClickAction())
            .performClick()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.settings_text))
                .assertExists()
        }
    }

    fun AndroidComposeTestRule<*, *>.invalidLogin() {
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(invalidEmail)
        onNode(hasHint(string(R.string.password_title)))
            .performTextReplacement(validPass)
        onNode(hasText(string(R.string.sign_in_title)) and hasClickAction())
            .performClick()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.error_user_not_found))
                .assertExists()
        }
    }

    fun AndroidComposeTestRule<*, *>.incorrectLogin() {
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(incorrectEmail)
        onNode(hasHint(string(R.string.password_title)))
            .performTextReplacement(validPass)
        onNode(hasText(string(R.string.sign_in_title)) and hasClickAction())
            .performClick()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.error_invalid_email_or_password))
                .assertExists()
        }
    }

    fun AndroidComposeTestRule<*, *>.goToSettings() {
        onNodeWithText(string(R.string.settings_text))
            .performClick()
        onNodeWithText(validEmail)
            .assertIsDisplayed()
    }

    fun AndroidComposeTestRule<*, *>.openLogoutDialog() {
        onNode(hasContentIconId(R.drawable.ic_logout))
            .performClick()
        onNodeWithText(string(R.string.log_out_title))
            .assertIsDisplayed()
        onNodeWithText(string(R.string.log_out_text))
            .assertIsDisplayed()
    }

    fun AndroidComposeTestRule<*, *>.closeBySwipe() {
        onRoot().performTouchInput { swipeDown() }
        onNodeWithText(string(R.string.log_out_title)).assertIsNotDisplayed()
    }


    fun AndroidComposeTestRule<*, *>.goToLogin() {
        onNodeWithText(string(R.string.sign_in_title))
            .performClick()
        onAllNodesWithText(string(R.string.sign_in_title)).onFirst().assertIsDisplayed()
    }


    fun AndroidComposeTestRule<*, *>.checkFieldsOnEmpty() {
        onNode(hasHint(string(R.string.email_title)))
            .assertTextEquals("")
        onNode(hasHint(string(R.string.password_title)))
            .assertTextEquals("")
        onNode(hasText(string(R.string.sign_in_title)) and hasClickAction())
            .assertIsNotEnabled()
    }

    fun AndroidComposeTestRule<*, *>.logout() {
        onNode(hasText(string(R.string.log_out_btn)) and !hasContentIconId(R.drawable.ic_logout))
            .performClick()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.settings_text))
                .assertDoesNotExist()
        }
        onNodeWithText(string(R.string.get_started_btn)).assertIsDisplayed()
    }

    fun AndroidComposeTestRule<*, *>.goToRegister() {
        onNode(
            hasText(
                string(R.string.sign_up_title),
                substring = true
            )
        ).performTouchInput { click(position = centerRight) }
        onNodeWithText(
            string(R.string.already_have_an_account_text),
            substring = true
        ).assertIsDisplayed()
    }

    fun AndroidComposeTestRule<*, *>.validateHideShowPass() {
        onNode(hasContentIconId(R.drawable.ic_hide_pass)).assertIsDisplayed()
        onNode(hasHint(string(R.string.password_title))).performTextReplacement(validPass)
        onNode(hasHint(string(R.string.password_title))).assertTextEquals(validPass.map { '•' }
            .joinToString(""))
        onNode(hasContentIconId(R.drawable.ic_hide_pass)).performClick()
        onNode(hasContentIconId(R.drawable.ic_show_pass)).assertIsDisplayed()
        onNode(hasHint(string(R.string.password_title))).assertTextEquals(validPass)
        onNode(hasContentIconId(R.drawable.ic_show_pass)).performClick()
        onNode(hasContentIconId(R.drawable.ic_hide_pass)).assertIsDisplayed()
    }

}
