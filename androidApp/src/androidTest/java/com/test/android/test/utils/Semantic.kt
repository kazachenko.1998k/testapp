package com.test.android.test.utils

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.semantics.getOrNull
import androidx.compose.ui.test.SemanticsMatcher
import androidx.compose.ui.test.hasContentDescription
import com.test.android.shared.borderSemantic
import com.test.android.shared.hintSemantic
import com.test.android.shared.progressSemantic

fun hasContentIconId(
    resourceId: Int
): SemanticsMatcher = hasContentDescription(resourceId.toString())

fun hasHint(
    hint: String
): SemanticsMatcher = SemanticsMatcher("Hint") {
    it.config.getOrNull(hintSemantic) == hint
}

fun hasProgress(): SemanticsMatcher = SemanticsMatcher("Has progress") {
    it.config.contains(progressSemantic)
}


fun hasBorderWithColor(
    color: Color
): SemanticsMatcher = SemanticsMatcher("Border color") {
    it.config.getOrNull(borderSemantic) == color
}

