package com.test.android.test.base

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import com.common.core.mviabstraction.usecases.SuspendUseCase
import com.common.core.preferences.api.PreferencesApi
import com.common.feature.account.api.data.models.DeleteCredentialsReq
import com.common.feature.account.api.data.models.SaveCredentialsReq
import com.common.feature.account.api.usecases.CredentialUseCase
import com.common.feature.account.api.usecases.DeleteCredentialsUseCase
import com.common.feature.account.api.usecases.InstructionIsShowOnceUseCase
import com.common.feature.account.api.usecases.SaveCredentialsUseCase
import com.common.feature.auth.api.data.AuthLocalApi
import com.common.feature.auth.api.data.models.Token
import com.common.feature.chat.api.data.ChatLocalApi
import com.common.feature.dashboard.api.usecases.MyAccountsUseCase
import com.common.feature.navigation.api.Navigation
import com.test.android.MainActivity
import com.test.android.test.utils.TIME_FOR_FIRST_SETUP
import com.test.android.test.utils.validApi
import com.test.android.test.utils.validSecret
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.junit.Before
import org.koin.core.component.get

abstract class ParamsTestMainActivity(
    private val params: SetupParams = SetupParams(),
) : ActivityTest<MainActivity>(createAndroidComposeRule()), SuspendUseCase {

    @Before
    fun setup() {
        val setupParams = params
        var paramsHasSetup = false
        launch(Dispatchers.IO) {
            setupParams.token?.let {
                get<AuthLocalApi>().saveToken(Token(it))
                if (setupParams.hasCredentials) {
                    get<SaveCredentialsUseCase>().saveCredentials(
                        SaveCredentialsReq(
                            secretKey = validSecret,
                            apiKey = validApi,
                        )
                    )
                    get<CredentialUseCase>().updateValue()
                    get<MyAccountsUseCase>().updateValue()
                } else {
                    val id = get<MyAccountsUseCase>().getOrLoadValue().first().getOrThrow()
                        .firstOrNull()?.id ?: return@let
                    get<DeleteCredentialsUseCase>().deleteCredentials(DeleteCredentialsReq(id))
                    get<CredentialUseCase>().updateValue()
                    get<MyAccountsUseCase>().updateValue()
                }
            } ?: run {
                get<PreferencesApi>().remove(PreferencesApi.TOKEN)
            }
            get<InstructionIsShowOnceUseCase>().saveShowOnce(setupParams.instructionWasShowOnce)
            get<PreferencesApi>().putBoolean(
                PreferencesApi.UserAgreementAndPrivacyPolicies,
                setupParams.acceptTermAndConditions
            )
            get<ChatLocalApi>().saveChatState(setupParams.hasChat)
            withContext(Dispatchers.Main) {
                get<Navigation>().navigate(setupParams.startDestination)
            }
            paramsHasSetup = true
        }
        testRule.waitUntil(timeoutMillis = TIME_FOR_FIRST_SETUP) {
            paramsHasSetup
        }
    }

}