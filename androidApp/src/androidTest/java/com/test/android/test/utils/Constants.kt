package com.test.android.test.utils

const val TIME_FOR_BACKEND_REQ = 5000L
const val TIME_FOR_UI_ACTION = 2000L
const val TIME_FOR_FIRST_SETUP = TIME_FOR_BACKEND_REQ * 2