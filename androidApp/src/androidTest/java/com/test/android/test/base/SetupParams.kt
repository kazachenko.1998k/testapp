package com.test.android.test.base

import com.common.feature.navigation.api.Destinations
import com.common.feature.navigation.api.HaveAccountQuestionScreen
import kotlin.reflect.KClass

data class SetupParams(
    val token: String? = null,
    val hasCredentials: Boolean = true,
    val instructionWasShowOnce: Boolean = true,
    val acceptTermAndConditions: Boolean = false,
    val hasChat: Boolean = false,
    val startDestination: KClass<out Destinations> = HaveAccountQuestionScreen::class,
)
