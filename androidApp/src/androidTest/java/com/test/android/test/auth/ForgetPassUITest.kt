package com.test.android.test.auth

import android.util.Log
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.test.filters.LargeTest
import com.common.feature.navigation.api.ResetPasswordScreen
import com.test.android.R
import com.test.android.shared.DesignRed
import com.test.android.test.auth.ForgetPassTestUtils.assertHasError
import com.test.android.test.auth.ForgetPassTestUtils.assertNotHasError
import com.test.android.test.base.ParamsTestMainActivity
import com.test.android.test.base.SetupParams
import com.test.android.test.utils.*
import org.junit.Test
import org.junit.jupiter.api.DisplayName

class ForgetPassUITest :
    ParamsTestMainActivity(SetupParams(startDestination = ResetPasswordScreen::class)) {

    @Test
    @LargeTest
    @DisplayName("CB-T4")
    fun forgetPassFlow() = with(testRule) {
        with(ForgetPassTestUtils) {
            validateEmail()//Step 1
            trySendWithEmptyField()//Step 2
            sendOnNotExistEmail()//Step 3
            writeCodeAndWaitError()//Step 4
            goBack()//Step 5
            clearEmailAndWriteExistEmail()//Step 6
            Log.e(
                "NOT_IMPLEMENTED_STEP",
                "step 7-8 not checked. Code always right in test build"
            )
            writeCode()//Step 9
            validatePass()//Step 10
            writeDifferentPass()//Step 11
            goBack()//Step 12
            clearEmailAndWriteExistEmailWithCode()//Step 13
            writeEqualsPass()//Step 14
        }
    }

}

object ForgetPassTestUtils {


    fun AndroidComposeTestRule<*, *>.trySendWithEmptyField() {
        onNode(hasHint(string(R.string.email_title)))
            .performTextClearance()
        onNodeWithText(string(R.string.send_instructions_btn))
            .assertIsNotEnabled()
    }

    fun AndroidComposeTestRule<*, *>.sendOnNotExistEmail() {
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(invalidEmail)
        onNodeWithText(string(R.string.send_instructions_btn))
            .assertIsEnabled()
            .performClick()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.resend_title), true)
                .assertIsDisplayed()
        }
    }

    fun AndroidComposeTestRule<*, *>.writeCodeAndWaitError() {
        onNode(hasSetTextAction())
            .performTextReplacement(codeFromEmail)
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.error_code_expired))
                .assertIsDisplayed()
        }
        onNodeWithText(string(R.string.resend_title), true)
            .assertIsDisplayed()
    }

    fun AndroidComposeTestRule<*, *>.writeCode() {
        onNode(hasSetTextAction())
            .performTextReplacement(codeFromEmail)
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.set_new_password_title))
                .assertIsDisplayed()
        }
    }

    fun AndroidComposeTestRule<*, *>.goBack() {
        runOnUiThread { activity.onBackPressed() }
    }

    fun AndroidComposeTestRule<*, *>.clearEmailAndWriteExistEmail() {
        onNode(hasHint(string(R.string.email_title)))
            .assertTextEquals(invalidEmail)
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(validEmail)
        onNodeWithText(string(R.string.send_instructions_btn))
            .assertIsEnabled()
            .performClick()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.resend_title), true)
                .assertIsDisplayed()
        }
    }

    fun AndroidComposeTestRule<*, *>.writeDifferentPass() {
        onNode(hasHint(string(R.string.password_title)))
            .performTextReplacement(validPass)
        onNode(hasHint(string(R.string.confirm_btn)))
            .performTextReplacement(validPass.dropLast(1))
        onNodeWithText(string(R.string.set_new_password_title))
            .performClick()
        onNode(hasHint(string(R.string.confirm_btn)))
            .assertHasError()
        onNodeWithText(string(R.string.save_btn))
            .assertIsNotEnabled()
    }

    fun AndroidComposeTestRule<*, *>.clearEmailAndWriteExistEmailWithCode() {
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(validEmail)
        onNodeWithText(string(R.string.send_instructions_btn))
            .assertIsEnabled()
            .performClick()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.resend_title), true)
                .assertIsDisplayed()
        }
        writeCode()
    }

    fun AndroidComposeTestRule<*, *>.writeEqualsPass() {
        onNode(hasHint(string(R.string.password_title)))
            .performTextReplacement(validPass)
        onNode(hasHint(string(R.string.confirm_btn)))
            .performTextReplacement(validPass)
        onNode(hasHint(string(R.string.confirm_btn)))
            .assertNotHasError()
        onNodeWithText(string(R.string.save_btn))
            .assertIsEnabled()
            .performClick()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.settings_text))
                .assertIsDisplayed()
        }
    }

    fun SemanticsNodeInteraction.assertHasError(): SemanticsNodeInteraction =
        assert(hasBorderWithColor(DesignRed))

    fun SemanticsNodeInteraction.assertNotHasError(): SemanticsNodeInteraction =
        assert(!hasBorderWithColor(DesignRed))

}

fun AndroidComposeTestRule<*, *>.validateEmail() {
    onNode(hasHint(string(R.string.email_title)))
        .performClick()
        .assertIsFocused()
        .assertNotHasError()
    onNodeWithText(string(R.string.reset_password_text)).performClick()
    onNode(hasHint(string(R.string.email_title)))
        .assertIsNotFocused()
        .assertHasError()
        .performClick()
    listOf(
        "Ema.il.test@pptst.de",
        "Email_tets@pptst.de",
        "Email-test@pptst.de",
        "Email+test@pptst.de",
        "Email@test.email",
    ).forEach {
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(it)
        onNode(hasHint(string(R.string.email_title)))
            .assertNotHasError()
    }
    listOf(
        "Ema.il.test",
        "@pptst.de",
        "Emailtest@p_ptst.de",
        "Email@test@pptst.de",
    ).forEach {
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(it)
        onNode(hasHint(string(R.string.email_title)))
            .assertHasError()
    }
}

fun AndroidComposeTestRule<*, *>.validatePass() {
    onNode(hasHint(string(R.string.password_title)))
        .performClick()
        .assertIsFocused()
        .assertNotHasError()
    onAllNodes(hasContentIconId(R.drawable.ic_check)).assertCountEquals(2)
    onNode(isRoot()).performClick()
    onNode(hasHint(string(R.string.password_title)))
        .assertIsNotFocused()
        .assertHasError()
        .performClick()
    onAllNodes(hasContentIconId(R.drawable.ic_clear)).assertCountEquals(2)
    listOf(
        validPass,
        "1wertyui",
        "1wertyui!",
        "1wertyui\"",
        "1wertyui№",
        "1wertyui*",
        "1wertyui;",
        "1wertyui?",
    ).forEach {
        onNode(hasHint(string(R.string.password_title)))
            .performTextReplacement(it)
        onNode(hasHint(string(R.string.password_title)))
            .assertNotHasError()
        onAllNodes(hasContentIconId(R.drawable.ic_check)).assertCountEquals(2)
    }
    onNode(hasHint(string(R.string.password_title)))
        .performTextReplacement("qwertyu")
    onNode(hasHint(string(R.string.password_title)))
        .assertHasError()
    onAllNodes(hasContentIconId(R.drawable.ic_clear)).assertCountEquals(2)
    onNode(hasHint(string(R.string.password_title)))
        .performTextReplacement("qwertyui")
    onNode(hasHint(string(R.string.password_title)))
        .assertHasError()
    onAllNodes(hasContentIconId(R.drawable.ic_clear)).assertCountEquals(1)
    onAllNodes(hasContentIconId(R.drawable.ic_check)).assertCountEquals(1)
    onNode(hasHint(string(R.string.password_title)))
        .performTextReplacement("12345678")
    onNode(hasHint(string(R.string.password_title)))
        .assertHasError()
    onAllNodes(hasContentIconId(R.drawable.ic_clear)).assertCountEquals(1)
    onAllNodes(hasContentIconId(R.drawable.ic_check)).assertCountEquals(2)
}
