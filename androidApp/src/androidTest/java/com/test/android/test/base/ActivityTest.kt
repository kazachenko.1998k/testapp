package com.test.android.test.base

import androidx.activity.ComponentActivity
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Rule

abstract class ActivityTest<A : ComponentActivity>(
    @get:Rule
    val testRule: AndroidComposeTestRule<ActivityScenarioRule<A>, A>
) : DefaultTest