package com.test.android.test.auth

import android.util.Log
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.test.filters.LargeTest
import com.common.feature.navigation.api.RegisterScreen
import com.test.android.R
import com.test.android.test.base.ParamsTestMainActivity
import com.test.android.test.base.SetupParams
import com.test.android.test.utils.*
import org.junit.Test
import org.junit.jupiter.api.DisplayName

class RegisterUITest : ParamsTestMainActivity(
    SetupParams(startDestination = RegisterScreen::class)
) {

    @Test
    @LargeTest
    @DisplayName("CB-T3")
    fun validRegisterAndLogout(): Unit = with(testRule) {
        with(RegisterTestUtils) {
            tryRegister()//Step 1
            //step 2 testing in other TestCase
            closeBySwipe()//Step 3
            register()//Step 4
            logoutAndTryRegisterWithEmptyFields()//Step 5
            //step 6-7 testing in other TestCase
            tryRegisterExistsUser()//Step 8
            Log.e(
                "NOT_IMPLEMENTED_STEP",
                "step 9-10 not checked. Require testing manually (change network state)"
            )
        }
    }

}

object RegisterTestUtils {
    fun AndroidComposeTestRule<*, *>.tryRegister() {
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(genValidEmail())
        onNode(hasHint(string(R.string.password_title)))
            .performTextReplacement(validPass)
        onNode(hasText(string(R.string.sign_up_title)) and hasClickAction())
            .performClick()
        onNodeWithText(string(R.string.accept_and_continue_btn))
            .assertIsDisplayed()
    }

    fun AndroidComposeTestRule<*, *>.tryRegisterExistsUser() {
        onNode(hasHint(string(R.string.email_title)))
            .performTextReplacement(validEmail)
        onNode(hasHint(string(R.string.password_title)))
            .performTextReplacement(validPass)
        onNode(hasText(string(R.string.sign_up_title)) and hasClickAction())
            .performClick()
        onNodeWithText(string(R.string.accept_and_continue_btn))
            .assertDoesNotExist()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.error_invalid_credentials_or_email_already_registered))
                .assertIsDisplayed()
        }
    }

    fun AndroidComposeTestRule<*, *>.register() {
        onNode(hasText(string(R.string.sign_up_title)) and hasClickAction())
            .performClick()
        onNodeWithText(string(R.string.accept_and_continue_btn))
            .assertIsDisplayed()
            .performClick()
        waitUntilHasError(TIME_FOR_BACKEND_REQ) {
            onNodeWithText(string(R.string.settings_text))
                .assertExists()
        }
    }


    fun AndroidComposeTestRule<*, *>.logoutAndTryRegisterWithEmptyFields() {
        onNodeWithText(string(R.string.settings_text))
            .performClick()
        with(LoginTestUtils) {
            onNode(hasText(string(R.string.log_out_btn)) and hasContentIconId(R.drawable.ic_logout))
                .performClick()
            logout()
        }
        onNodeWithText(string(R.string.get_started_btn))
            .performClick()
        onNode(hasHint(string(R.string.email_title)))
            .assertTextEquals("")
        onNode(hasHint(string(R.string.password_title)))
            .assertTextEquals("")
        onNode(hasText(string(R.string.sign_up_title)) and hasClickAction())
            .assertIsNotEnabled()
    }


    fun AndroidComposeTestRule<*, *>.closeBySwipe() {
        onRoot().performTouchInput { swipeDown() }
        onNodeWithText(string(R.string.accept_and_continue_btn)).assertIsNotDisplayed()
        onAllNodesWithText(string(R.string.sign_up_title)).onFirst().assertIsDisplayed()
    }


}
