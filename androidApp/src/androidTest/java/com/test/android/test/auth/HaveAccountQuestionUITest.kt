package com.test.android.test.auth

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.test.filters.MediumTest
import androidx.test.filters.SmallTest
import com.test.android.R
import com.test.android.test.base.ParamsTestMainActivity
import com.test.android.test.utils.hasContentIconId
import com.test.android.test.utils.string
import org.junit.Test
import org.junit.jupiter.api.DisplayName

class HaveAccountQuestionUITest : ParamsTestMainActivity() {

    @Test
    @SmallTest
    @DisplayName("CB-T1 step 1")
    fun goToRegister() = with(HaveAccountQuestionTestUtils) {
        testRule.goToRegister()
    }

    @Test
    @SmallTest
    @DisplayName("CB-T1 step 1+2")
    fun goToRegisterWithBack() = with(HaveAccountQuestionTestUtils) {
        goToRegister()
        testRule.goToRegisterWithBack()
    }

    @Test
    @SmallTest
    @DisplayName("CB-T1 step 3")
    fun goToLogin() = with(HaveAccountQuestionTestUtils) {
        testRule.goToLogin()
    }

    @Test
    @SmallTest
    @DisplayName("CB-T1 step 3+4")
    fun goToLoginWithBack() = with(HaveAccountQuestionTestUtils) {
        testRule.goToLogin()
        testRule.goToLoginWithBack()
    }

    @Test
    @MediumTest
    @DisplayName("CB-T1 step 1-4")
    fun haveAccountQuestionFull() {
        goToRegisterWithBack()
        goToLoginWithBack()
    }

}

object HaveAccountQuestionTestUtils {
    fun AndroidComposeTestRule<*, *>.goToRegister() {
        onNodeWithText(string(R.string.get_started_btn))
            .performClick()
        onAllNodesWithText(string(R.string.sign_up_title)).onFirst().assertIsDisplayed()
    }

    fun AndroidComposeTestRule<*, *>.goToRegisterWithBack() {
        goToRegister()
        onNode(hasContentIconId(R.drawable.ic_back)).performClick()
        onAllNodesWithText(string(R.string.get_started_btn)).onFirst().assertIsDisplayed()
        onAllNodesWithText(string(R.string.sign_in_title)).onFirst().assertIsDisplayed()
    }

    fun AndroidComposeTestRule<*, *>.goToLogin() {
        onNodeWithText(string(R.string.sign_in_title))
            .performClick()
        onAllNodesWithText(string(R.string.sign_in_title)).onFirst().assertIsDisplayed()
    }

    fun AndroidComposeTestRule<*, *>.goToLoginWithBack() {
        goToLogin()
        onNode(hasContentIconId(R.drawable.ic_back)).performClick()
        onAllNodesWithText(string(R.string.get_started_btn)).onFirst().assertIsDisplayed()
        onAllNodesWithText(string(R.string.sign_in_title)).onFirst().assertIsDisplayed()
    }

}

