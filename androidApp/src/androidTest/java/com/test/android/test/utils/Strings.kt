package com.test.android.test.utils

import kotlin.random.Random

const val validEmail = "abc@pptst.de"
const val validEmailWithCred = "abccred@pptst.de"
const val invalidEmail = "abc@pptst.pppppp"
const val codeFromEmail = "12345"
const val incorrectEmail = "abcpppppp"
const val tokenAtEmptyAccount =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYjllYmY1ZDdmZTM1MDAwNzc1ZmQ1ZSIsImlhdCI6MTYzOTU3NzI3M30.Tyk0INNByEvkwNYFeESNwuGE0gTlofXivNwns4CPigo"
const val tokenAtAccountWithCred =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxYmEwMzE4OTEwZGYxMDAwNzgwNTZkZSIsImlhdCI6MTYzOTU4MDQ0MH0.UHyUMTme73PL1i54Qv-Xr7_Q8PslNCO-TNoyQdOWUvI"
const val validPass = "12345Ppp"
const val validApi = "LFowPCNGzmQb5mxLMPuU36DtVa9xkwPfvQfCjxQszDlBytGNpHzUJUBFvcSuqf8y"
const val validSecret = "FyYMabF7gqDaQNyk4CHz9HtYBEKZHwLQuzs0KOxSlQd1tEG6j71qyMrWK3hOwexx"
const val binance = "binance"
const val validSecretHint = "*************"
val validApiHint = validApi.take(5) + validSecretHint.take(8)
fun genValidEmail() =
    (0..Random.nextInt(5, 10)).map { ('a'..'z').random() }.joinToString("") + "@mail.test"