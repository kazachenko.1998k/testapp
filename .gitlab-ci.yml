variables:
  APP_NAME: "Test"
  TEST_RESULTS_PATH: "testResults"

  # QA VARIABLES
  BUILD_FILE_NAME: "androidApp-qa.apk"
  FILE_PATH: "androidApp/build/outputs/apk/qa"
  MAPPING_PATH: "androidApp/build/outputs/mapping/qa/mapping.txt"
  FULL_FILE_PATH: "$FILE_PATH/$APP_NAME-$CI_PIPELINE_ID.apk"
  ANDROID_GRADLE_TASK: :androidApp:assembleQa

  # RELEASE VARIABLES
  BUILD_FILE_NAME_RELEASE: "androidApp-release"
  FILE_PATH_RELEASE: "androidApp/build/outputs/apk/release"
  MAPPING_PATH_RELEASE: "androidApp/build/outputs/mapping/release/mapping.txt"
  FULL_FILE_PATH_RELEASE: "$FILE_PATH_RELEASE/$APP_NAME-$CI_PIPELINE_ID"
  ANDROID_GRADLE_TASK_RELEASE: :androidApp:assembleRelease :androidApp:bundleRelease

  #  SLACK NOTIFICATION VARIABLES
  PROJECT_NAME: "$APP_NAME Android"

stages:
  - build
  - appdistribution
  - release
  - appdistribution_release
  - create_release
  - dokka
  - ui_test

cache:
  key: $CI_PROJECT_NAME
  paths:
    - .gradle/
    - "*/build/"

pages:
  stage: dokka
  script:
    - ./gradlew dokkaHtmlMultiModule
    - rm -rf public
    - mkdir public
    - cp -r ./build/dokkaCustomMultiModuleOutput/* public
  artifacts:
    paths:
      - public
  rules:
    - if: $DOKKA_NIGHTLY == "dokka"
  tags: [ pp-office-macmini-m1 ]

build:
  stage: build
  script:
    - export BUILD_NUMBER=${CI_PIPELINE_ID}
    - echo $KEYSTORE_FILE | base64 -d > app.keystore
    - ./gradlew -PdisablePreDex ${ANDROID_GRADLE_TASK}
    - mv ${FILE_PATH}/${BUILD_FILE_NAME} ${FULL_FILE_PATH}
    - cp ${FULL_FILE_PATH} ${APP_NAME}.apk
  artifacts:
    expire_in: 1 week
    paths:
      - ${APP_NAME}.apk
    reports:
      dotenv: build.env
  only: [ master ]

create_release:
  stage: create_release
  script:
    - create_release.sh
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /Merge branch 'release/ && $CI_COMMIT_REF_NAME == "master"

deploy_production:
  stage: appdistribution
  script:
    - nvm use v14.17.6
    - firebase appdistribution:distribute ${APP_NAME}.apk --app "$FIREBASE_APP_ID" --release-notes "Build $CI_PIPELINE_ID from CI\nChange log:\n$CI_COMMIT_MESSAGE" --groups "test-group" --token "$FIREBASE_CI_TOKEN"
    - export DOWNLOAD_URL="https://git.ppnet.io/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/-/jobs/${BUILD_JOB_ID}/artifacts/download?file_type=archive"
    - notify.sh
  only: [ master ]
  dependencies: [ build ]

release:
  stage: release
  script:
    - echo "BUILD_JOB_ID=$CI_JOB_ID" >> build.env
    - auto_merge_request.sh
    - entrypoint_release.sh
    - cp ${FULL_FILE_PATH_RELEASE}.apk ${APP_NAME}.apk
    - cp ${FULL_FILE_PATH_RELEASE}.aab ${APP_NAME}.aab
  artifacts:
    expire_in: 30 days
    paths:
      - ${APP_NAME}.apk
      - ${APP_NAME}.aab
      - ${MAPPING_PATH_RELEASE}
    reports:
      dotenv: build.env
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^release/

deploy_release:
  stage: appdistribution_release
  script:
    - nvm use v14.17.6
    - firebase appdistribution:distribute ${APP_NAME}.apk --app "$FIREBASE_APP_ID_RELEASE" --release-notes "RELEASE Build $CI_PIPELINE_ID from CI\nChange log:\n$CI_COMMIT_MESSAGE" --groups "test-group" --token "$FIREBASE_CI_TOKEN"
    - notify_release.sh
  rules:
    - if: $CI_COMMIT_BRANCH =~ /^release/
  dependencies: [ release ]

ui_test:
  stage: ui_test
  timeout: 2h
  before_script:
    - rm -rf ./testResults || true
    - rm testResults.zip || true
    - mkdir -p ./testResults
  script:
    - export ANDROID_SERIAL=qwertqwer
    - export BUILD_NUMBER=${CI_PIPELINE_ID}
    - ./gradlew :androidApp:uninstallAll
    - ./gradlew :androidApp:installDebugAndroidTest
    - >
      if [ "$(adb shell dumpsys power | grep mHoldingDisplaySuspendBlocker= | grep -oE '(true|false)')" == false ]; then
        echo "Screen is off. Turning on."
        echo "wakeup"
        adb shell input keyevent 26
        sleep 2
        echo "swipe up"
        adb shell input keyevent 82
        echo "write pass and press enter"
        sleep 2
        adb shell input text 1234 && adb shell input keyevent 66
        echo "OK, should be on now."
      else
        echo "Screen is already on."
      fi
    - ./gradlew :androidApp:connectedDebugAndroidTest --max-workers=5
    - cp -r ./androidApp/build/reports/androidTests/connected ./testResults/test
    - zip -r testResults.zip testResults
    - curl -F "file=@testResults.zip" https://reporter.work.ppnet.io/android/ui/report/$CI_PROJECT_NAME
    - BUILD_ID="$CI_PIPELINE_ID" BUILD_JOB_ID="$CI_JOB_ID" notify_ui_tests_complete
  artifacts:
    expire_in: 1 week
    paths: [ testResults ]
  when: manual

