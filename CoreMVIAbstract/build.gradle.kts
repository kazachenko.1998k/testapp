apply(from = "../versions.gradle.kts")
val coroutines_version: String by extra
plugins {
    id("multiplatform-library-convention")
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
                implementation(project(":Utils"))
                implementation(project(":CoreBackendApi"))
            }
        }
    }
}
