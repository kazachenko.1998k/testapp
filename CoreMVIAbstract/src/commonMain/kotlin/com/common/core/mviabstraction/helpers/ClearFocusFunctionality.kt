package com.common.core.mviabstraction.helpers

import com.common.core.mviabstraction.Effect

interface ClearFocusFunctionality {
    object ClearEffect : Effect
}