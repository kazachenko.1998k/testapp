package com.common.core.mviabstraction.usecases

import kotlinx.coroutines.flow.StateFlow


/**
 * Use case for save value [T] and provide it anywhere
 */
interface ValueUseCase<T> : GetOrLoadUseCase<T> {

    /**
     * Return local value
     * @return null by default
     */
    val current: StateFlow<T?>

    /**
     * Update local value
     */
    fun setNewValue(new: T?)

}