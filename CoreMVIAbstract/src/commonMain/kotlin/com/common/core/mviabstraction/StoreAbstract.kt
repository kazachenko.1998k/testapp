package com.common.core.mviabstraction

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*

abstract class StoreAbstract<S : State, A : Action, E : Effect>(defaultState: S) :
    Store<S, A, E>, CoroutineScope by CoroutineScope(Dispatchers.Default) {

    private val _state = MutableStateFlow(defaultState)
    private val _sideEffect = MutableSharedFlow<E>()
    private val _propertyEffect = MutableSharedFlow<Unit>()
    private val state = _state.asStateFlow()
    private val sideEffect = _sideEffect.asSharedFlow()
    override fun observeState(): StateFlow<S> = state

    override fun observeStateWithProperty(): Flow<S> = combine(
        _state, _propertyEffect
    ) { state, _ ->
        state
    }

    override fun observeSideEffect(): Flow<E> = sideEffect


    /**
     * Manual call to update the field inside the [_state]
     * */
    protected fun currentState() = state.value


    /**
     * Manual call to update the field inside the [_state]
     * */
    protected suspend fun updateProperty() {
        _propertyEffect.emit(Unit)
    }


    /**
     * Manual call to emit new side effect to flow [sideEffect]
     * */
    protected suspend fun emitSideEffect(effect: E) {
        _sideEffect.emit(effect)
    }

    /**
     * Updates the [_state] value if the instance has changed;
     * Otherwise, it updates the [_propertyEffect] with trigger [observeStateWithProperty].
     * Designed to prevent duplication of the same changes.
     * */
    protected suspend fun updateStateWithProperty(action: (suspend S.() -> S)) {
        if (!updateStateSuspend(action)) { //if value not changed, but property changed
            updateProperty()
        }
    }


    /**
     *
     * Updating the meaning of this [_state].
     * Returns [true] if the value has been updated otherwise [false]
     */
    protected fun updateState(action: (S.() -> S)): Boolean =
        updateState(action.invoke(currentState()))

    /**
     *
     * Suspend updating the meaning of this [_state].
     * Returns [true] if the value has been updated otherwise [false]
     */
    protected suspend fun updateStateSuspend(action: (suspend S.() -> S)): Boolean =
        updateState(action.invoke(currentState()))

    protected fun updateState(result: S): Boolean {
        val hasChanges = currentState() != result
        if (hasChanges) {
            _state.value = result
        }
        return hasChanges
    }

    /**
     * if result is failure emit Error effect to [sideEffect].
     * Also implement default flow collector functional
     */
    suspend fun <T> Flow<Result<T>>.collectWithEmitError(
        action: suspend (Result<T>) -> Unit
    ) = collect {
        action.invoke(it)
        it.onFailure { exception ->
            (ErrorEffect(exception) as? E)?.let { effect -> emitSideEffect(effect) }
        }
    }

}