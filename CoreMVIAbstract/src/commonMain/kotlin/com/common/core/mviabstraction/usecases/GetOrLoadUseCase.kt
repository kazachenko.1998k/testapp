package com.common.core.mviabstraction.usecases

import kotlinx.coroutines.flow.Flow


/**
 * Use case can load data from anywhere, after value successfully received it is saved locally
 */
interface GetOrLoadUseCase<T> : BaseUseCase {

    /**
     * Load if local value is failed or empty
     * else return local value
     * @return [NoSuchElementException] by default
     */
    suspend fun getOrLoadValue(): Flow<Result<T>>

    /**
     * Update local value
     */
    suspend fun updateValue(): Result<T>

}