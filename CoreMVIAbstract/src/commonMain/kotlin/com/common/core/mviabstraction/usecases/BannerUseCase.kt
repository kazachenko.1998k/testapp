package com.common.core.mviabstraction.usecases

import com.common.core.mviabstraction.Effect
import com.common.core.mviabstraction.Store
import com.common.utils.models.Banner
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterIsInstance

interface BannerUseCase : SuspendUseCase {
    val bannerDataFlow: Flow<Banner>
    fun subscribeEffect(snackFlow: Flow<Banner>)
    fun subscribeFlow(snackFlow: Flow<Effect>) = subscribeEffect(snackFlow.filterIsInstance())
    fun subscribe(store: Store<*, *, *>) = subscribeFlow(store.observeSideEffect())
}