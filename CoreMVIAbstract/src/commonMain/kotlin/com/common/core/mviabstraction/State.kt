package com.common.core.mviabstraction

/**
 * Stores all data for a specific screen.
 * It is recommended to use data class with val fields.
 * Update with copy.
 */
interface State