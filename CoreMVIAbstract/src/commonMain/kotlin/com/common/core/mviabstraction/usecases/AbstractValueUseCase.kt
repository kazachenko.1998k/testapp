package com.common.core.mviabstraction.usecases

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map


/**
 * Abstract use case for save value [T] and provide it anywhere
 */
abstract class AbstractValueUseCase<T> : ValueUseCase<T> {

    override val current: MutableStateFlow<T?> = MutableStateFlow(null)

    override fun setNewValue(new: T?) {
        current.value = new
    }

    override suspend fun getOrLoadValue(): Flow<Result<T>> = current.map { it.toResult() }

    override suspend fun updateValue(): Result<T> = current.value.toResult()

    private inline fun T?.toResult() =
        this?.let { Result.success(it) } ?: Result.failure(NoSuchElementException())
}