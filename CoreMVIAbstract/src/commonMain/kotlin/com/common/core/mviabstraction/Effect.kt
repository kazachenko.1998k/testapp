package com.common.core.mviabstraction

import com.common.utils.models.Banner

/**
 * A one-time event that needs to be executed that does not change the current state
 * */
interface Effect

abstract class BannerEffect(val error: Throwable) : Effect, Banner()

/**
 * Error effect for all exceptions in app
 */
class ErrorEffect constructor(error: Throwable) : BannerEffect(error)

/**
 * Effect for all Warnings in app
 */
class WarningEffect constructor(error: Throwable) : BannerEffect(error)

/**
 * Error effect with action after trigger reaction
 */
class ActionEffect(val action: () -> Unit, error: Throwable) : BannerEffect(error)
