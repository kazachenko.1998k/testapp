package com.common.core.mviabstraction.usecases

/**
 * Common Interface for inheriting all UseCases in the application
 */
interface BaseUseCase