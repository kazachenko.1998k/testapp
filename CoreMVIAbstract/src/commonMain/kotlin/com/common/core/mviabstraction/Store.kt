package com.common.core.mviabstraction

import com.common.utils.wrap
import kotlinx.coroutines.flow.*

/**
 * A common interface for the state management layer and handling all Actions.
 * */
interface Store<S : State, A : Action, E : Effect> {

    /**
     * Returns flow from view values as [S].
     * IGNORES property changes.
     * Get current value as [StateFlow.value]
     * The default is assigned a value from the constructor abstract class[defaultState]
     * Can be updated with [StoreAbstract.updateState], maybe [StoreAbstract.updateStateWithProperty]
     */
    fun observeState(): StateFlow<S>

    /**
     * Returns flow from view values as [S].
     * NOT IGNORES property changes.
     * Get current value only suspend as [Flow.first]
     * The default is assigned a value from the constructor abstract class[defaultState]
     * Can be updated with [StoreAbstract.updateProperty], [StoreAbstract.updateState], [StoreAbstract.updateStateWithProperty]
     */
    fun observeStateWithProperty(): Flow<S>


    /**
     * Returns flow from side effects values as [E].
     * Not save current or last value
     * Can be updated with [Store.dispatch] if [Action] is [Effect]
     */
    fun observeSideEffect(): Flow<E>


    /**
     * Dispatch [action] from View
     */
    fun dispatch(action: A)

    /**
     * Returns the same as [Store.observeState]. Use only for iOS.
     * */
    fun watchState() = observeState().wrap()

    /**
     * Returns the same as [Store.observeStateWithProperty]. Use only for iOS.
     * */
    fun watchStateWithProperty() = observeStateWithProperty().wrap()


    /**
     * Returns the same as [Store.observeSideEffect]. Use only for iOS.
     * */
    fun watchSideEffect() = observeSideEffect().wrap()
}