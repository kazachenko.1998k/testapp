package com.common.core.mviabstraction.usecases

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancelChildren
import kotlin.coroutines.CoroutineContext

/**
 * Used for subscribing and making asynchronous requests.
 * All inherited classes must call [release] according to the application's behavior logic.
 */
interface SuspendUseCase : CoroutineScope, BaseUseCase {

    override val coroutineContext: CoroutineContext
        get() = SupervisorJob() + Dispatchers.Default

    fun release() {
        coroutineContext.cancelChildren()
    }

}