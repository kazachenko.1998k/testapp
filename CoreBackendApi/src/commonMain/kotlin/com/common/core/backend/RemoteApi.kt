package com.common.core.backend

/**
 * Common Interface for inheriting all methods for remote backend.
 * @sample com.common.feature.auth.api.data.AuthRemoteApi
 */
interface RemoteApi