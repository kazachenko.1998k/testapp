package com.common.core.backend

interface ReqResMapper<RecC, RecI, ResI, ResC> : ReqMapper<RecC, RecI>, ResMapper<ResI, ResC>