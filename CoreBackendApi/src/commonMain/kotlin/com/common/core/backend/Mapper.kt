package com.common.core.backend

/**
 * Common Interface for inheriting all mappers models in app.
 * @sample ReqMapper
 * @sample ReqResMapper
 * @sample ResMapper
 */
interface Mapper