package com.common.core.backend

/**
 * Helper function for convert response from raw [ResI] to [ResC]
 * @sample com.common.feature.auth.impl.data.rawmodels.UserLoginMapper.toCommon
 * */
interface ResMapper<ResI, ResC>: Mapper {
    fun toCommon(from: ResI): ResC
}