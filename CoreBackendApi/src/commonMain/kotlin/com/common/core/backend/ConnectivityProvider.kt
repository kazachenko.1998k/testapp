package com.common.core.backend

import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first

/**
 * Internet state provider.
 * Used for auto reload date after connection lose if it needed.
 *
 * @suppress Need platform implementations on Android and iOS
 */
interface ConnectivityProvider {

    /**
     * State flow with boolean
     * return [true] if has internet else [false]
     */
    val hasInternet: StateFlow<Boolean>

    /**
     * Wait connect to internet
     */
    suspend fun waitConnect() = hasInternet.first { it }

}