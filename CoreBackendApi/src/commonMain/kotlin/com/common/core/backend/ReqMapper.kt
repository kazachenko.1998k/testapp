package com.common.core.backend

/**
 * Interface for convert request [ReqC] from common to raw [ReqI]
 * @sample com.common.feature.auth.impl.data.rawmodels.SaveCredentialsMapper.fromCommon
 * */
interface ReqMapper<RecC, RecI> : Mapper {
    fun fromCommon(from: RecC): RecI
}