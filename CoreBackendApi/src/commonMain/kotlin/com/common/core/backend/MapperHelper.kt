package com.common.core.backend

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

/**
 * Helper function for convert request [ReqC] from common to raw [ReqI] and response from raw [ResI] to [ResC]
 * @sample com.common.feature.auth.impl.data.AuthRemoteImpl.saveCredentials
 * */
suspend fun <ReqC, ReqI, ResI, ResC> request(
    builder: ReqResMapper<ReqC, ReqI, ResI, ResC>,
    function: suspend (ReqI) -> Result<ResI>,
    request: ReqC
): Result<ResC> = function(builder.fromCommon(request)).map { builder.toCommon(it) }

suspend fun <ReqC, ReqI, ResI, ResC> requestFlow(
    builder: ReqResMapper<ReqC, ReqI, ResI, ResC>,
    function: suspend (ReqI) -> Result<Flow<ResI>>,
    request: ReqC
): Result<Flow<ResC>> =
    function(builder.fromCommon(request)).map { flowValue -> flowValue.map { builder.toCommon(it) } }
