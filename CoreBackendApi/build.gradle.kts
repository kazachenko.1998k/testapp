import com.codingfeline.buildkonfig.compiler.FieldSpec.Type

apply(from = "../versions.gradle.kts")
val coroutines_version: String by extra
val ktor_version: String by extra
val our_side_url_debug: String by extra
val our_side_url_release: String by extra

plugins {
    id("multiplatform-library-convention")
    id("com.codingfeline.buildkonfig")
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                api("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
            }
        }
        val androidMain by getting {
            dependencies {
                api("io.ktor:ktor-client-android:$ktor_version")
            }
        }
        val iosMain by getting {
            dependencies {
                api("io.ktor:ktor-client-ios:$ktor_version")
            }
        }
    }
}
buildkonfig {
    packageName = "com.common.core.backend"
    // objectName = 'YourAwesomeConfig'
    exposeObjectWithName = "CustomBuildConfig"
    val releaseTypeList = listOf("release", "rc")
    val isRelease = project.gradle.startParameter.taskNames.any { taskName ->
        releaseTypeList.forEach { type ->
            if (taskName.contains(type, ignoreCase = true)) return@any true
        }
        return@any false
    }

    defaultConfigs {
        val url = if (isRelease) our_side_url_release else our_side_url_debug
        buildConfigField(Type.STRING, "OUR_SIDE_URL", url)
        buildConfigField(Type.STRING, "CRISP_KEY", "6fbc2643-9316-4bb2-a0c2-3e4049032777")
    }
}