apply(from = "../versions.gradle.kts")
val coroutines_version: String by extra
val ktor_version: String by extra

plugins {
    id("multiplatform-library-convention")
    kotlin("plugin.serialization")
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
                implementation("io.ktor:ktor-client-serialization:$ktor_version")
            }
        }
//        commonTest { //commented for IDE right work(commonMain not indexing as source root)
//            dependencies {
//                implementation(kotlin("test-common"))
//                implementation(kotlin("test-annotations-common"))
//                implementation(kotlin("test-junit5"))
//            }
//        }
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}