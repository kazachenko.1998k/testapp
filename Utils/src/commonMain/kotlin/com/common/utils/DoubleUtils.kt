package com.common.utils

import kotlin.math.pow

inline fun Double.incLastBit(mask: String): Double {
    return this + (10.0.pow(-mask.lastIndex + mask.indexOf('.')))
}

inline fun Double.roundToUp(mask: String): Double {
    val validMask = mask.filterNot { it == ' ' }
    val formatMinBalance = validMask.toDouble()
    return if (formatMinBalance < this) {
        this.incLastBit(validMask)
    } else formatMinBalance
}