package com.common.utils

const val SECOND_IN_MS = 1000L
const val MINUTE_IN_MS = SECOND_IN_MS * 60
const val HOUR_IN_MS = MINUTE_IN_MS * 60
const val DAY_IN_MS = HOUR_IN_MS * 24
const val WEEK_IN_MS = DAY_IN_MS * 7
const val MONTH_IN_MS = DAY_IN_MS * 32
const val YEAR_IN_MS = MONTH_IN_MS * 12
