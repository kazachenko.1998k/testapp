package com.common.utils.models

/**
 * Generalization of Missing Data Errors
 */
interface NoDataError

/**
 * Error not will be show
 */
interface NotShowedError

/**
 * Error will be show as Warning
 */
interface Warning

/**
 * unauthorized - Anywhere. Authorization error or lack thereof
 */
object Unauthorized : Throwable(), NoDataError

/**
 * no token - Anywhere. Not authorized or logout
 */
object NoToken : Throwable(), NoDataError, NotShowedError

/**
 * wrong reset code - password recovery. we received a confirmation code by email, but we entered an incorrect 5-digit code in the field
 */
object WrongResetCode : Throwable()

/**
 * code expired - Password recovery. we received a confirmation code by mail, but we entered the code after a long time and it is no longer valid
 */
object CodeExpired : Throwable()

/**
 * sending code error - Password recovery. failed to send confirmation code to mail
 */
object ErrorSendingCode : Throwable()

/**
 * invalid password - Password recovery. An attempt was made to assign a new password of an incorrect format
 */
object InvalidPassword : Throwable()

/**
 * invalid email or password - Login. You entered an invalid (impossible) username or password
 */
object InvalidEmailOrPassword : Throwable()

/**
 * incorrect email or password - Login. You entered an incorrect username or password
 */
object IncorrectEmailOrPassword : Throwable()

/**
 * user not found - Login. Login entered does not exist
 */
object UserNotFound : Throwable(), NoDataError

/**
 * invalid credentials or email already registered - Registration. incorrect data or account is already registered
 */
object InvalidCredentialsOrEmailAlreadyRegistered : Throwable()

/**
 * account not found - Allocation of funds for the robot. The platform account has not yet been connected, and therefore it is impossible to allocate non-existent money
 */
object AccountNotFound : Throwable(), NoDataError