package com.common.utils.models

enum class ProfitState {
    Positive,
    Negative,
    Equals
}