package com.common.utils.models

abstract class Banner : Throwable()

abstract class CredentialsBanner(open val id: String) : Banner()
abstract class NetworkBanner : Banner()

data class CredentialsInvalidNotification(override val id: String) : CredentialsBanner(id)
data class SpotAndMarginTradingDisabled(override val id: String) : CredentialsBanner(id)
data class BotsNeedRestart(val ids: List<String>, override val id: String) : CredentialsBanner(id)


/**
 * network error - Anywhere. Error due to lack of internet
 */
object NetworkError : NetworkBanner()
object HideNetworkError : NetworkBanner()