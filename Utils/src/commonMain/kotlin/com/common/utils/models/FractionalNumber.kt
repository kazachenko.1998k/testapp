package com.common.utils.models

data class FractionalNumber(val dividend: Int, val divider: Int) {
    override fun toString(): String = "$dividend/$divider"
}