package com.common.utils.models

val CurrencySignMap = hashMapOf(
    "AED" to "د.إ",
    "AFN" to "؋",
    "ALL" to "L",
    "AMD" to "֏",
    "ANG" to "ƒ",
    "AOA" to "Kz",
    "ARS" to "ARS$",
    "AUD" to "A$",
    "AWG" to "ƒ",
    "AZN" to "₼",
    "BAM" to "KM",
    "BBD" to "Bds$",
    "BDT" to "৳",
    "BGN" to "лв",
    "BHD" to ".د.ب",
    "BIF" to "FBu",
    "BMD" to "BD$",
    "BND" to "B$",
    "BOB" to "\$b",
    "BOV" to "BOV",
    "BRL" to "R$",
    "BSD" to "B$",
    "BTN" to "Nu.",
    "BWP" to "P",
    "BYN" to "Br",
    "BYR" to "Br",
    "BZD" to "BZ$",
    "CAD" to "C$",
    "CDF" to "FC",
    "CHE" to "CHE",
    "CHF" to "CHF",
    "CHW" to "CHW",
    "CLF" to "CLF",
    "CLP" to "C$",
    "CNY" to "¥",
    "COP" to "COL$",
    "COU" to "COU",
    "CRC" to "₡",
    "CUC" to "C$",
    "CUP" to "₱",
    "CVE" to "C$",
    "CZK" to "Kč",
    "DJF" to "Fdj",
    "DKK" to "kr",
    "DOP" to "RD$",
    "DZD" to "دج",
    "EEK" to "kr",
    "EGP" to "£",
    "ERN" to "Nfk",
    "ETB" to "Br",
    "EUR" to "€",
    "FJD" to "FJ$",
    "FKP" to "£",
    "GBP" to "£",
    "GEL" to "₾",
    "GGP" to "£",
    "GHC" to "₵",
    "GHS" to "GH₵",
    "GIP" to "£",
    "GMD" to "D",
    "GNF" to "FG",
    "GTQ" to "Q",
    "GYD" to "GY$",
    "HKD" to "HK$",
    "HNL" to "L",
    "HRK" to "kn",
    "HTG" to "G",
    "HUF" to "Ft",
    "IDR" to "Rp",
    "ILS" to "₪",
    "IMP" to "£",
    "INR" to "₹",
    "IQD" to "ع.د",
    "IRR" to "﷼",
    "ISK" to "kr",
    "JEP" to "£",
    "JMD" to "J$",
    "JOD" to "JD",
    "JPY" to "¥",
    "KES" to "KSh",
    "KGS" to "с",
    "KHR" to "៛",
    "KMF" to "CF",
    "KPW" to "₩",
    "KRW" to "₩",
    "KWD" to "KD",
    "KYD" to "K$",
    "KZT" to "₸",
    "LAK" to "₭",
    "LBP" to "£",
    "LKR" to "₨",
    "LRD" to "L$",
    "LSL" to "M",
    "LTC" to "Ł",
    "LTL" to "Lt",
    "LVL" to "Ls",
    "LYD" to "LD",
    "MAD" to "MAD",
    "MDL" to "lei",
    "MGA" to "Ar",
    "MKD" to "den",
    "MMK" to "K",
    "MNT" to "₮",
    "MOP" to "MOP$",
    "MRO" to "UM",
    "MRU" to "UM",
    "MUR" to "₨",
    "MVR" to "Rf",
    "MWK" to "MK",
    "MXN" to "MX$",
    "MXV" to "MXV",
    "MYR" to "RM",
    "MZN" to "MT",
    "NAD" to "N$",
    "NGN" to "₦",
    "NIO" to "C$",
    "NOK" to "kr",
    "NPR" to "₨",
    "NZD" to "N$",
    "OMR" to "﷼",
    "PAB" to "B/.",
    "PEN" to "S/.",
    "PGK" to "K",
    "PHP" to "₱",
    "PKR" to "₨",
    "PLN" to "zł",
    "PYG" to "Gs",
    "QAR" to "﷼",
    "RMB" to "￥",
    "RON" to "lei",
    "RSD" to "din",
    "RUB" to "₽",
    "RWF" to "R₣",
    "SAR" to "﷼",
    "SBD" to "SI$",
    "SCR" to "₨",
    "SDG" to "ج.س.",
    "SEK" to "kr",
    "SGD" to "S$",
    "SHP" to "£",
    "SLL" to "Le",
    "SOS" to "S",
    "SRD" to "S$",
    "SSP" to "£",
    "STD" to "Db",
    "STN" to "Db",
    "SVC" to "₡",
    "SYP" to "£",
    "SZL" to "E",
    "THB" to "฿",
    "TJS" to "SM",
    "TMT" to "T",
    "TND" to "د.ت",
    "TOP" to "T$",
    "TRL" to "₤",
    "TRY" to "₺",
    "TTD" to "TT$",
    "TVD" to "TV$",
    "TWD" to "NT$",
    "TZS" to "TSh",
    "UAH" to "₴",
    "UGX" to "USh",
    "USD" to "$",
    "UYI" to "UYI",
    "UYU" to "\$U",
    "UYW" to "UYW",
    "UZS" to "So'm",
    "VEF" to "Bs",
    "VES" to "Bs.S",
    "VND" to "₫",
    "VUV" to "VT",
    "WST" to "WS$",
    "XAF" to "FCFA",
    "XBT" to "Ƀ",
    "XCD" to "EC$",
    "XOF" to "CFA",
    "XPF" to "₣",
    "XSU" to "Sucre",
    "XUA" to "XUA",
    "YER" to "﷼",
    "ZAR" to "R",
    "ZMW" to "ZK",
    "ZWD" to "Z$",
    "ZWL" to "ZW$"
)