package com.common.utils.models

/**
 * Class for all editable field that will can have exception
 * @property text text that draw
 * @property error if has error than will have exception
 * @property hasError has error now or not
 * @property hide only for pass. state ***** or Pas11
 * @property enabled equals [EditText.enabled]
 */
data class FieldEditable(
    val text: String,
    val hint: String?,
    val enabled: Boolean,
    val hasError: Boolean,
    val hasFocusOnce: Boolean,
    val hide: Boolean,
    val error: Exception?,
) {
    companion object {
        fun defaultInstance() = FieldEditable(
            text = "",
            enabled = false,
            hide = true,
            hasError = false,
            hasFocusOnce = false,
            error = null,
            hint = null
        )
    }
}
