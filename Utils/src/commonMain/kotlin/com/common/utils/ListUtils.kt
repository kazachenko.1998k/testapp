package com.common.utils

inline fun <T> MutableList<T>.replaceFirstMutable(
    condition: (T) -> Boolean,
    rule: (T) -> T
): MutableList<T> {
    val index = indexOfFirst(condition)
    if (index >= 0) {
        add(index, rule.invoke(removeAt(index)))
    }
    return this
}

inline fun <T> List<T>.replaceFirst(new: T, condition: (T) -> Boolean): List<T> =
    replaceFirst(condition, rule = { new })

inline fun <T> List<T>.replaceFirst(condition: (T) -> Boolean, rule: (old: T) -> T): List<T> =
    toMutableList().replaceFirstMutable(condition, rule)

