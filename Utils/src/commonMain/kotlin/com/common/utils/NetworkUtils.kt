package com.common.utils

import com.common.utils.models.*
import io.ktor.client.features.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.utils.io.charsets.*
import io.ktor.utils.io.errors.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json

private val json = Json { ignoreUnknownKeys = true }

expect suspend fun <T> executeRequest(block: suspend () -> T): T

/**
 * Wrapper for all network request.
 * @param block suspend function returning response body as [T]
 * @return [Result] with generic [T], when [T] it is response body
 *  * success - Response with [T] body
 *  * failure - Response with [Throwable]
 * */
suspend fun <T> requestWrapper(block: suspend () -> T): Result<T> =
    try {
        Result.success(executeRequest(block))
    } catch (ex: Exception) {
        when (ex) {
            is IOException, is HttpRequestTimeoutException -> Result.failure(NetworkError)
            is ClientRequestException -> try {
                Result.failure(
                    json.decodeFromString(
                        BackendException.serializer(),
                        ex.response.readText(Charsets.UTF_8)
                    ).tryConvertToObject()
                )
            } catch (_: SerializationException) {
                Result.failure(ex)
            }
            else -> Result.failure(ex)
        }
    }

@Serializable
data class BackendException(
    val error: String,
    override val message: String
) : Throwable() {
    fun tryConvertToObject(): Throwable = when (error) {
        "unauthorized" -> Unauthorized
        "wrong_reset_code" -> WrongResetCode
        "code_expired" -> CodeExpired
        "sending_code_error" -> ErrorSendingCode
        "invalid_password" -> InvalidPassword
        "invalid_email_or_password" -> InvalidEmailOrPassword
        "incorrect_email_or_password" -> IncorrectEmailOrPassword
        "user_not_found" -> UserNotFound
        "email_already_registered" -> InvalidCredentialsOrEmailAlreadyRegistered
        "invalid_credentials_or_email_already_registered" -> InvalidCredentialsOrEmailAlreadyRegistered
        "account_not_found" -> AccountNotFound
        else -> this
    }
}

/**
 * Header key.
 * Used for authorization with access token
 * */
const val AUTHORIZATION_TOKEN_KEY = "x-access-token"

/**
 * Wrap library call for use suspend function inside apply block
 *
 * Also add default header "Content-Type" with key "application/json"
 */
inline fun HttpRequestBuilder.headers(block: HeadersBuilder.() -> Unit): HeadersBuilder =
    headers.apply(block).apply { append("Content-Type", "application/json") }