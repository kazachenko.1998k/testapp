package com.common.utils

import kotlin.math.pow
import kotlin.math.withSign

val emailRegex = Regex(
    "^(?=.{1,64}@)[A-Za-z0-9+_-]+(\\.[A-Za-z0-9+_-]+)*@"
            + "[^-][A-Za-z0-9+-]+(\\.[A-Za-z0-9+-]+)*(\\.[A-Za-z]{2,})$"
)

const val defaultPlatform = "Binance"
const val passMaxLength = 255
const val emailMaxLength = 63

/**
 * format double value as
 * ### ### ###.00000000
 * @suppress max symbols after dot = [precision] - all symbols
 * @sample StringUtilsTest
 */
fun Double?.toNormalText(maxLengthFractionRule: (Int) -> Int): String {
    if (this == null) return "0.$fractionalPartIfNumberInteger"
    val integerPartOfNumber = this.withSign(positive).toLong()
    val fractionalPartOfNumber = this.withSign(positive) - integerPartOfNumber
    val lengthIntegerPart = integerPartOfNumber.toString().length
    val maxLengthFractionPart = maxLengthFractionRule.invoke(lengthIntegerPart)
    val fractionalPartAsInteger =
        ((1/*for save leading "0"*/ + fractionalPartOfNumber) * 10.0.pow(maxLengthFractionPart))
            .toLong()
    val fractionalPartAsString =
        if (fractionalPartAsInteger.toDouble() == 10.0.pow(maxLengthFractionPart)) fractionalPartIfNumberInteger else
            fractionalPartAsInteger
                .toString()
                .drop(1)
                .dropLastWhile { it == '0' }
    val cutFractionalPartAsString = fractionalPartAsString.take(maxLengthFractionPart)
    val integerPartAsStringWithSpaces =
        (if (cutFractionalPartAsString.isEmpty()) this.withSign(positive)
            .toLong() else integerPartOfNumber)
            .toString()
            .reversed()
            .chunked(lengthGroupNumber)
            .reversed()
            .joinToString(separator = " ") { it.reversed() }
    val cutFractionalWithDotIfNeeded = if (cutFractionalPartAsString.isEmpty()) {
        cutFractionalPartAsString
    } else {
        ".$cutFractionalPartAsString"
    }
    return "$integerPartAsStringWithSpaces$cutFractionalWithDotIfNeeded"
}

fun Double?.toNormalText(): String = toNormalText { 0.coerceAtLeast(fullLengthNumber - it) }
fun Double?.toCurrencyText(): String =
    toNormalText { digitAfterDotInCurrency.coerceAtMost(fullLengthNumber - it).coerceAtLeast(0) }

private const val positive = 1
private const val digitAfterDotInCurrency = 2
private const val lengthGroupNumber = 3
private const val fullLengthNumber = 7
private const val fractionalPartIfNumberInteger = "00"
