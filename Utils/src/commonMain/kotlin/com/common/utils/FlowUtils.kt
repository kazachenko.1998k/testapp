package com.common.utils

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first

/**
 * Return current value if exists, else wait first not null value in flow
 */
suspend fun <T> Flow<T?>.firstNotNull(): T = filterNotNull().first()

/**
 * Combines several flows according to the following rules:
 * 1. If at least one flow has an error, an error will be returned
 * 2. If there is no error, the original flow will return
 */
fun <T> Flow<Result<T>>.combineResult(list: List<Flow<Result<*>>>): Flow<Result<T>> =
    combine(this, *list.toTypedArray()) {
        val firstError = it.find { result -> result.isFailure }
        return@combine if (firstError != null) {
            firstError as Result<T>
        } else {
            it[0] as Result<T>
        }
    }