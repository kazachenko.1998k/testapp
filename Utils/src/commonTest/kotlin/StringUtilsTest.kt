import com.common.utils.toNormalText
import kotlin.test.Test
import kotlin.test.assertEquals

class StringUtils {

    @Test
    fun `number format`() {
        assertEquals("123.00", "123.00".toDoubleOrNull()?.toNormalText())
        assertEquals("123 324.00", "123324.00".toDoubleOrNull()?.toNormalText())
        assertEquals("123.01", "123.01000".toDoubleOrNull()?.toNormalText())
        assertEquals("123.1", "123.1000".toDoubleOrNull()?.toNormalText())
        assertEquals("123.45679", "123.45678901".toDoubleOrNull()?.toNormalText())
        assertEquals("123 456 789", "123456789.123434".toDoubleOrNull()?.toNormalText())
        assertEquals("123 456 789", "123456789.0".toDoubleOrNull()?.toNormalText())
        assertEquals("123 456 789 091", "123456789090.9".toDoubleOrNull()?.toNormalText())
        assertEquals("3 456 789 091", "3456789090.9".toDoubleOrNull()?.toNormalText())
        assertEquals("456 789 091", "456789090.9".toDoubleOrNull()?.toNormalText())
        assertEquals("123 456.00", "123456.0000".toDoubleOrNull()?.toNormalText())
        assertEquals("123 456.00", "123456".toDoubleOrNull()?.toNormalText())
    }

}