package com.common.utils

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

actual suspend fun <T> executeRequest(block: suspend () -> T): T =
    withContext(Dispatchers.Default) { block.invoke() }
