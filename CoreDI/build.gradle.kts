apply(from = "../versions.gradle.kts")
val koin_version: String by extra
val ktor_version: String by extra

plugins {
    id("multiplatform-library-convention")
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                api("io.insert-koin:koin-core:$koin_version")
                api("io.ktor:ktor-client-core:$ktor_version")
                implementation(project(":CoreBackendApi"))
                implementation(project(":CoreBackendImpl"))
                implementation(project(":FeatureAuthApi"))
                implementation(project(":FeatureAuthImpl"))
                implementation(project(":FeatureNavigationApi"))
                implementation(project(":FeatureNavigationImpl"))
                implementation(project(":CorePreferenceApi"))
                implementation(project(":CorePreferenceImpl"))
                implementation(project(":CoreMVIImpl"))
            }
        }
    }
}

