package com.common.core

import com.common.core.backend.impl.clientEngine
import io.ktor.client.engine.*
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.KoinAppDeclaration

/**
 * Need call on start app
 * @param factory [HttpClientEngineFactory] - example [io.ktor.client.engine.android.Android]
 * @param modules platform modules - example module for UI
 * @param appDeclaration Create a KoinApplication instance and help configure it
 */
fun initKoin(
    factory: HttpClientEngineFactory<*>,
    modules: List<Module> = emptyList(),
    appDeclaration: KoinAppDeclaration = {}
) = startKoin {
    appDeclaration()
    modules(
        *commonModule.toTypedArray(),
        *modules.toTypedArray(),
        clientEngine(factory)
    )
}

// called by iOS etc
fun initKoin(
    factory: HttpClientEngineFactory<*>,
    modules: List<Module> = emptyList()
) = initKoin(factory, modules) {}

val commonModule = listOf(
    com.common.core.backend.impl.DI,
    com.common.core.backend.impl.platformDI(),
    com.common.feature.auth.impl.di.DI,
    com.common.feature.navigation.impl.di.DI,
    com.common.core.preferences.impl.DI,
    com.common.core.mvi.impl.DI,
)