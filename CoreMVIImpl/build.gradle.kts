apply(from = "../versions.gradle.kts")
val koin_version: String by extra
val coroutines_version: String by extra
val multiplatform_settings: String by extra
val firebase_installations: String by extra

plugins {
    id("multiplatform-library-convention")
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":Utils"))
                implementation(project(":CoreMVIAbstract"))
                implementation(project(":CoreBackendApi"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
                implementation("io.insert-koin:koin-core:$koin_version")
            }
        }
    }
}
