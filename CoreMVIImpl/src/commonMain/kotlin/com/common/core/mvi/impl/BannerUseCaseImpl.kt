package com.common.core.mvi.impl

import com.common.core.backend.ConnectivityProvider
import com.common.core.mviabstraction.BannerEffect
import com.common.core.mviabstraction.usecases.BannerUseCase
import com.common.utils.models.Banner
import com.common.utils.models.HideNetworkError
import com.common.utils.models.NetworkError
import com.common.utils.models.NotShowedError
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class BannerUseCaseImpl(connectivityProvider: ConnectivityProvider) : BannerUseCase {

    private val errorFlow: MutableSharedFlow<Banner> = MutableSharedFlow()

    @OptIn(ExperimentalCoroutinesApi::class)
    override val bannerDataFlow = merge(
        errorFlow.filterNot { it is NetworkError },/*for ignore repeat network error*/
        connectivityProvider.hasInternet.map { if (it) HideNetworkError else NetworkError }
    ).filterNot { it is BannerEffect && it.error is NotShowedError }

    override fun subscribeEffect(snackFlow: Flow<Banner>) {
        launch { snackFlow.collect { errorFlow.emit(it) } }
    }

}