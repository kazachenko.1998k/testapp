package com.common.core.mvi.impl

import com.common.core.mviabstraction.usecases.BannerUseCase
import org.koin.dsl.bind
import org.koin.dsl.module

val DI = module {
    single {
        BannerUseCaseImpl(get())
    } bind BannerUseCase::class
}