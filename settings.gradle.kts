pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
    plugins {
        apply(from = "versions.gradle.kts")
        val dokka: String by extra
        id("org.jetbrains.dokka") version (dokka)
        id("org.jetbrains.kotlin.jvm") version "1.5.31"
    }
    resolutionStrategy {
        apply(from = "versions.gradle.kts")
        val build_tools_gradle: String by extra
        val kotlin_version: String by extra
        eachPlugin {
            when (requested.id.id) {
                "com.android.library" -> useModule("com.android.tools.build:gradle:$build_tools_gradle")
                "org.jetbrains.kotlin.plugin.serialization" -> useModule("org.jetbrains.kotlin:kotlin-serialization:$kotlin_version")
                "org.jetbrains.kotlin.multiplatform" -> useModule("org.jetbrains.kotlin:kotlin-gradle-plugin$kotlin_version")
            }
        }
    }
}

includeBuild("build-logic")
rootProject.name = "TestApp"
include(":androidApp")
include(":shared")
include(":CoreMVIAbstract")
include(":CoreDI")
include(":CoreBackendApi")
include(":CoreBackendImpl")
include(":CorePreferenceApi")
include(":CorePreferenceImpl")
include(":FeatureAuthApi")
include(":FeatureAuthImpl")
include(":FeatureNavigationApi")
include(":FeatureNavigationImpl")
include(":Utils")
include(":CoreMVIImpl")
