apply(from = "../versions.gradle.kts")
val koin_version: String by extra
val coroutines_version: String by extra
val ktor_version: String by extra
val compose_ui: String by extra
val serialization_version: String by extra

plugins {
    id("multiplatform-library-convention")
    kotlin("plugin.serialization")
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
                implementation("io.insert-koin:koin-core:$koin_version")
                implementation(project(":Utils"))
            }
        }
    }
}
