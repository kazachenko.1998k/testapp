package com.common.feature.navigation.api

import com.common.utils.CFlow
import kotlinx.coroutines.flow.Flow
import kotlin.reflect.KClass

/**
 * Interface for navigating screens in the app
 */
interface Navigation {

    /**
     * @return selected screen
     * @suppress return null if not set anywhere
     */
    fun current(): Destinations?

    /**
     * Navigate go back by stack or invoke [onEmptyBackStack]
     * @suppress if [Destinations.onBackListener] return false do nothing
     * @return new current screen
     */
    fun back(): Destinations

    /**
     * Navigate to new screen
     * @param destination current screen to navigate
     * @suppress If a screen is in the stack of previously issued screens, it will be removed and placed at the end of the stack
     */
    fun navigate(
        destination: KClass<out Destinations>
    )

    /**
     * Navigate to new screen
     * @param destination current screen to navigate
     * @param screensForRemoveFromStack after redirect to new screen this screens will be removed from back stack
     * @suppress If a screen is in the stack of previously issued screens, it will be removed and placed at the end of the stack
     */
    fun navigate(
        destination: KClass<out Destinations>,
        screensForRemoveFromStack: List<KClass<out Destinations>> = emptyList()
    )

    /**
     * Navigate to external app
     * @param destination current screen to navigate
     */
    fun navigateToExternalApp(
        destination: ExternalAppEvents
    )

    /**
     * Callback if there is nowhere to go
     */
    fun onEmptyBackStack(action: () -> Unit)

    /**
     * Navigate to new screen with clear all backstack
     */
    fun navigateWithClear(destination: KClass<out Destinations>)

    /**
     * @return flow from selected screen
     */
    fun subscribe(): Flow<Destinations>

    /**
     * @return flow at [ExternalAppEvents]
     */
    fun subscribeExternalAppEvents(): Flow<ExternalAppEvents>

    fun watch(): CFlow<Destinations>

    /**
     * @return list from all endpoints
     */
    val fullGraph: List<Destinations>
}