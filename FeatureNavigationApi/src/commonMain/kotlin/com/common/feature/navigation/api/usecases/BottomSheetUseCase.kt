package com.common.feature.navigation.api.usecases

import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.StateFlow

interface BottomSheetUseCase {

    val nowState: StateFlow<BottomSheetState>

    val nowVisible: StateFlow<Boolean>

    fun close()
    suspend fun awaitClose()
    suspend fun closeForce()
    fun onFirstClose(block: suspend () -> Unit): Job

    fun open()
    suspend fun awaitOpen()
    suspend fun openForce()
    fun onFirstOpen(block: suspend () -> Unit): Job

    fun canGoBack(): Boolean

    enum class BottomSheetState {
        Open,
        Close,
        Opening,
        Closing
    }

}