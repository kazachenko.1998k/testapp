package com.common.feature.navigation.api

import kotlin.reflect.KClass


abstract class Destinations {
    open fun onDraw() {}

    /**
     * Listener for navigation between screen.
     * @return [true] if need go back by navigation graph [optionally do anything]
     * @return [false] if need ignore back navigate [optionally do anything]
     */
    open val onBackListener: () -> Boolean = { true }

    /**
     * Param for navigation in app
     * Returning value will be contains in [fullDestinations]
     * @suppress Used for navigation
     * @return abstract class
     * @sample com.test.android.auth.LoginScreenImpl
     */
    open val parentClass: KClass<out Destinations> = this::class
}

sealed interface ExternalAppEvents

open class LoginScreen : Destinations()

open class RegisterScreen : Destinations()

open class HaveAccountQuestionScreen : Destinations()

open class ResetPasswordScreen : Destinations()

open class VerifyCodeScreen : Destinations()

class BrowserLink(val url: String) : ExternalAppEvents

/**
 * List all endpoints in app.
 * @suppress Each of the list must have an implementation and is in DI
 */
val fullDestinations by lazy {
    listOf(
        LoginScreen::class,
        HaveAccountQuestionScreen::class,
        ResetPasswordScreen::class,
        VerifyCodeScreen::class,
        RegisterScreen::class,
    )
}