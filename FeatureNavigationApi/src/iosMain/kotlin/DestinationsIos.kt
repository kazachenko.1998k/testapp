package com.common.feature.navigation.api

import org.koin.dsl.bind
import org.koin.dsl.module

val HaveAccountQuestionScreenClass = HaveAccountQuestionScreen::class

val DI = module {
    single { HaveAccountQuestionScreen() } bind HaveAccountQuestionScreen::class
}