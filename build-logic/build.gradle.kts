plugins {
    `kotlin-dsl`
}
apply(from = "../versions.gradle.kts")
val kotlin_gradle_plugin: String by extra
val build_tools_gradle: String by extra
val detekt_gradle_plugin: String by extra
val icerock_mobile_multiplatform: String by extra

repositories {
    google()
    gradlePluginPortal()
    mavenCentral()
}

dependencies {
    api("dev.icerock:mobile-multiplatform:$icerock_mobile_multiplatform")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_gradle_plugin")
    implementation("com.android.tools.build:gradle:$build_tools_gradle")
    implementation("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:$detekt_gradle_plugin")
}

//gradlePlugin {
//    plugins{
//        create("easy-export") {
//            id = "com.common.gradle.easy-export"
//            displayName = "easy-export"
//            description = "After enable this plugin while using framework configuration you can add dependencies to export just like in iOS framework."
//            implementationClass = "com.common.gradle.AppleFrameworkPlugin"
//        }
//    }
//}