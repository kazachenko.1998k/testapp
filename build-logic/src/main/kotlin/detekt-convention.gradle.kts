/*
 * Copyright 2021 IceRock MAG Inc. Use of this source code is governed by the Apache 2.0 license.
 */
apply(from = "../versions.gradle.kts")
val detekt_formatting: String by extra

plugins {
    id("io.gitlab.arturbosch.detekt")
}

detekt {
    input.setFrom("src/commonMain/kotlin", "src/androidMain/kotlin", "src/iosMain/kotlin")
}

dependencies {
    "detektPlugins"("io.gitlab.arturbosch.detekt:detekt-formatting:$detekt_formatting")
}
