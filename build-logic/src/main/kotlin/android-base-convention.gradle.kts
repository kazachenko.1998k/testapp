/*
 * Copyright 2021 IceRock MAG Inc. Use of this source code is governed by the Apache 2.0 license.
 */

import com.android.build.gradle.BaseExtension
apply(from = "../versions.gradle.kts")
val min_sdk: String by extra
val target_sdk: String by extra

configure<BaseExtension> {
    compileSdkVersion(target_sdk.toInt())

    defaultConfig {
        minSdk = min_sdk.toInt()
        targetSdk = target_sdk.toInt()
    }
    buildTypes {
        create("qa")
        create("rc")
    }
}
