/*
 * Copyright 2021 IceRock MAG Inc. Use of this source code is governed by the Apache 2.0 license.
 */
apply(from = "../versions.gradle.kts")
val compose_ui: String by extra

plugins {
    id("base-convention")
    id("com.android.library")
    id("kotlin-android")
    id("android-base-convention")
}

android {
    buildFeatures {
        // Enables Jetpack Compose for this module
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "$compose_ui"
    }

    sourceSets.all { java.srcDir("src/$name/kotlin") }
}
