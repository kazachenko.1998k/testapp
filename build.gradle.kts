import org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension

plugins {
    id("org.jetbrains.dokka")
}

buildscript {
    apply(from = "versions.gradle.kts")
    val kotlin_gradle_plugin: String by extra
    val build_tools_gradle: String by extra
    val build_konfig_gradle: String by extra
    val google_services: String by extra
    val firebase_crashlytics_gradle: String by extra
    val firebase_perf_plugin: String by extra

    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_gradle_plugin")
        classpath("com.android.tools.build:gradle:$build_tools_gradle")
        classpath("com.codingfeline.buildkonfig:buildkonfig-gradle-plugin:$build_konfig_gradle")
        classpath("com.google.gms:google-services:$google_services")
        classpath("com.google.firebase:firebase-crashlytics-gradle:$firebase_crashlytics_gradle")
        classpath("com.google.firebase:perf-plugin:$firebase_perf_plugin")
        classpath(":build-logic")
    }
}


allprojects {
    repositories {
        google()
        mavenCentral()
        maven("https://jitpack.io")
        maven("https://developer.huawei.com/repo/")
        maven("https://oss.sonatype.org/content/repositories/snapshots/")
    }
    afterEvaluate {
        // Remove log pollution until Android support in KMP improves.
        project.extensions.findByType<KotlinMultiplatformExtension>()?.let { kmpExt ->
            kmpExt.sourceSets.removeAll { sourceSet ->
                setOf(
                    "androidAndroidTestQa",
                    "androidAndroidTestRc",
                    "androidAndroidTestRelease",
                ).contains(sourceSet.name)
            }
        }
    }

}

subprojects {
    apply {
        plugin("org.jetbrains.dokka")
    }
}

/**
 * For generate all documentation code need run [gradlew dokkaHtmlMultiModule]
 * Result will be in [./build/dokkaCustomMultiModuleOutput/index.html]
 * */
tasks.dokkaHtmlMultiModule.configure {
    outputDirectory.set(buildDir.resolve("dokkaCustomMultiModuleOutput"))
}