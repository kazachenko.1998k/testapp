package com.common.core.preferences.api

/**
 * Common Interface for inheriting all methods for local storage.
 * @sample com.common.feature.auth.api.data.AuthLocalApi
 */
interface LocalApi