package com.common.core.preferences.api

import kotlinx.coroutines.flow.Flow

/**
 * Interface for work with key-value data-base
 *
 * Include methods from [com.russhwolf.settings.coroutines.FlowSettings]
 *
 * Used custom interface for isolate dependency only in impl
 */
interface PreferencesApi {
    suspend fun clear()
    suspend fun remove(key: String)

    suspend fun keys(): Set<String>
    suspend fun size(): Int
    suspend fun hasKey(key: String): Boolean

    suspend fun putInt(key: String, value: Int)

    fun getIntFlow(key: String, defaultValue: Int = 0): Flow<Int>
    suspend fun getInt(key: String, defaultValue: Int): Int

    fun getIntOrNullFlow(key: String): Flow<Int?>
    suspend fun getIntOrNull(key: String): Int?

    suspend fun putLong(key: String, value: Long)

    fun getLongFlow(key: String, defaultValue: Long = 0): Flow<Long>
    suspend fun getLong(key: String, defaultValue: Long): Long

    fun getLongOrNullFlow(key: String): Flow<Long?>
    suspend fun getLongOrNull(key: String): Long?

    suspend fun putString(key: String, value: String)

    fun getStringFlow(key: String, defaultValue: String = ""): Flow<String>
    suspend fun getString(key: String, defaultValue: String): String

    fun getStringOrNullFlow(key: String): Flow<String?>
    suspend fun getStringOrNull(key: String): String?

    suspend fun putFloat(key: String, value: Float)

    fun getFloatFlow(key: String, defaultValue: Float = 0f): Flow<Float>
    suspend fun getFloat(key: String, defaultValue: Float): Float

    fun getFloatOrNullFlow(key: String): Flow<Float?>
    suspend fun getFloatOrNull(key: String): Float?

    suspend fun putDouble(key: String, value: Double)

    fun getDoubleFlow(key: String, defaultValue: Double = 0.0): Flow<Double>
    suspend fun getDouble(key: String, defaultValue: Double): Double

    fun getDoubleOrNullFlow(key: String): Flow<Double?>
    suspend fun getDoubleOrNull(key: String): Double?

    suspend fun putBoolean(key: String, value: Boolean)

    fun getBooleanFlow(key: String, defaultValue: Boolean = false): Flow<Boolean>
    suspend fun getBoolean(key: String, defaultValue: Boolean): Boolean

    fun getBooleanOrNullFlow(key: String): Flow<Boolean?>
    suspend fun getBooleanOrNull(key: String): Boolean?

    companion object ConstantsKey {
        const val TOKEN = "TOKEN"
        const val UserAgreementAndPrivacyPolicies = "UAAndPP"
    }
}