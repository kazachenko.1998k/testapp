package com.common.core.backend.impl

import com.common.utils.SECOND_IN_MS
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*

fun httpClient(factory: HttpClientEngineFactory<*>) = HttpClient(factory) {
    install(Logging) {
        logger = Logger.DEFAULT
        level = LogLevel.ALL
    }
    install(HttpTimeout) {
        requestTimeoutMillis = SECOND_IN_MS * 10L
    }
    install(JsonFeature) {
        val json = kotlinx.serialization.json.Json {
            ignoreUnknownKeys = true
            allowSpecialFloatingPointValues = true
            useArrayPolymorphism = false
        }
        serializer = KotlinxSerializer(json)
    }

}