package com.common.core.backend.impl

import io.ktor.client.*
import io.ktor.client.engine.*
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module

val DI = module {
    single {
        httpClient(get())
    } bind HttpClient::class
}

expect fun platformDI(): Module

fun clientEngine(factory: HttpClientEngineFactory<*>) = module {
    single {
        factory
    } bind HttpClientEngineFactory::class
}