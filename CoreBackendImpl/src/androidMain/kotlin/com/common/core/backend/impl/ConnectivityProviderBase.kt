package com.common.core.backend.impl

import com.common.core.backend.ConnectivityProvider
import kotlinx.coroutines.flow.MutableStateFlow

abstract class ConnectivityProviderBase : ConnectivityProvider {

    override val hasInternet: MutableStateFlow<Boolean> by lazy {
        subscribe()
        MutableStateFlow(getNetworkState())
    }

    protected fun dispatchChange(state: Boolean) {
        hasInternet.value = state
    }

    protected abstract fun subscribe()
    protected abstract fun getNetworkState(): Boolean
}