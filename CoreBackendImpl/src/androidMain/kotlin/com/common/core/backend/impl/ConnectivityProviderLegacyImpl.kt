package com.common.core.backend.impl

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.ConnectivityManager.CONNECTIVITY_ACTION
import android.net.ConnectivityManager.EXTRA_NETWORK_INFO
import android.net.NetworkInfo

@Suppress("DEPRECATION")
@SuppressLint("MissingPermission")
class ConnectivityProviderLegacyImpl(
    private val context: Context,
    private val cm: ConnectivityManager
) : ConnectivityProviderBase() {

    private val receiver = ConnectivityReceiver()

    override fun subscribe() {
        context.registerReceiver(receiver, IntentFilter(CONNECTIVITY_ACTION))
    }

    override fun getNetworkState(): Boolean {
        val activeNetworkInfo = cm.activeNetworkInfo
        return activeNetworkInfo?.isConnectedOrConnecting ?: false
    }

    private inner class ConnectivityReceiver : BroadcastReceiver() {
        override fun onReceive(c: Context, intent: Intent) {
            // on some devices ConnectivityManager.getActiveNetworkInfo() does not provide the correct network state
            // https://issuetracker.google.com/issues/37137911
            val networkInfo = cm.activeNetworkInfo
            val fallbackNetworkInfo: NetworkInfo? = intent.getParcelableExtra(EXTRA_NETWORK_INFO)
            // a set of dirty workarounds
            val state: Boolean =
                if (networkInfo?.isConnectedOrConnecting == true) {
                    networkInfo.isConnectedOrConnecting
                } else if (networkInfo != null && fallbackNetworkInfo != null &&
                    networkInfo.isConnectedOrConnecting != fallbackNetworkInfo.isConnectedOrConnecting
                ) {
                    fallbackNetworkInfo.isConnectedOrConnecting
                } else {
                    val state = networkInfo ?: fallbackNetworkInfo
                    state?.isConnectedOrConnecting ?: false
                }
            dispatchChange(state)
        }
    }
}