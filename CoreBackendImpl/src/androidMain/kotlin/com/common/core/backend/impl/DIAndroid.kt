package com.common.core.backend.impl

import com.common.core.backend.ConnectivityProvider
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.dsl.module

actual fun platformDI(): Module {
    return module {
        single { ConnectivityProviderFactory(get()) }
        single { get<ConnectivityProviderFactory>().createProvider() } bind ConnectivityProvider::class
    }
}