package com.common.core.backend.impl

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import com.common.core.backend.ConnectivityProvider

class ConnectivityProviderFactory(private val context: Context) {

    fun createProvider(): ConnectivityProvider {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ConnectivityProviderNewImpl(cm)
        } else {
            ConnectivityProviderLegacyImpl(context, cm)
        }
    }
}