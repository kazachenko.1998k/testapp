apply(from = "../versions.gradle.kts")
val koin_version: String by extra
val ktor_version: String by extra
val serialization_version: String by extra
val coroutines_version: String by extra
val logback_version: String by extra
plugins {
    id("multiplatform-library-convention")
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":Utils"))
                implementation(project(":CoreBackendApi"))
                implementation("ch.qos.logback:logback-classic:$logback_version")
                implementation("io.ktor:ktor-client-logging:$ktor_version")
                implementation("io.insert-koin:koin-core:$koin_version")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:$serialization_version")
                implementation("io.ktor:ktor-client-serialization:$ktor_version")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
            }
        }
    }
}
