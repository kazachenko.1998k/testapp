package com.common.feature.navigation.impl.callbacks.auth

import com.common.feature.auth.api.domain.ResetPasswordCallbacks
import com.common.feature.navigation.api.Navigation
import com.common.feature.navigation.api.VerifyCodeScreen

class ResetPasswordCallbacksImpl(private val navigator: Navigation) : ResetPasswordCallbacks {

    override fun onSuccessResetPassword() {
        navigator.navigate(VerifyCodeScreen::class)
    }

    override fun onBackClick() {
        navigator.back()
    }
}