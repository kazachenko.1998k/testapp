package com.common.feature.navigation.impl.callbacks.auth

import com.common.feature.auth.api.domain.SetNewEmailCallbacks
import com.common.feature.navigation.api.Navigation
import com.common.feature.navigation.api.VerifyCodeScreen

class SetNewEmailCallbacksImpl(private val navigator: Navigation) : SetNewEmailCallbacks {

    override fun onSuccessSetEmail() {
        navigator.navigate(VerifyCodeScreen::class)
    }

    override fun onBackClick() {
        navigator.back()
    }
}