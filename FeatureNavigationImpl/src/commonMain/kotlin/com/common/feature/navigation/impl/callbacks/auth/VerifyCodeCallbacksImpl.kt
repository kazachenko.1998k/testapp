package com.common.feature.navigation.impl.callbacks.auth

import com.common.feature.auth.api.domain.VerifyCodeCallbacks
import com.common.feature.navigation.api.Navigation

class VerifyCodeCallbacksImpl(
    private val navigator: Navigation,
) : VerifyCodeCallbacks {

    override fun onSuccessValidateCode() {
    }

    override fun onBackClick() {
        navigator.back()
    }
}