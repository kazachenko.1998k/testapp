package com.common.feature.navigation.impl

import com.common.feature.navigation.api.Destinations
import com.common.feature.navigation.api.ExternalAppEvents
import com.common.feature.navigation.api.Navigation
import com.common.feature.navigation.api.fullDestinations
import com.common.utils.CFlow
import com.common.utils.wrap
import kotlinx.coroutines.flow.*
import org.koin.core.Koin
import org.koin.mp.KoinPlatformTools
import kotlin.reflect.KClass

/**
 * A simple navigator which maintains a back stack.
 */
class Navigator(
    private val koin: Koin = KoinPlatformTools.defaultContext().get(),
    private val initialBackStack: MutableList<Destinations> = mutableListOf()
) : Navigation {

    private val _current = MutableStateFlow<Destinations?>(null)
    private val externalAppEvents = MutableSharedFlow<ExternalAppEvents>(extraBufferCapacity = 1)
    val current = _current.asStateFlow()
    private var onEmptyBackStackAction = {}
    override fun onEmptyBackStack(action: () -> Unit) {
        onEmptyBackStackAction = action
    }

    override fun back(): Destinations {
        when {
            !current.value!!.onBackListener.invoke() -> {
                /* do nothing*/
            }
            canGoBack() -> {
                initialBackStack.removeLast()
                navigate(initialBackStack.last()::class)
            }
            else -> {
                onEmptyBackStackAction.invoke()
            }
        }
        return koin.get(current.value!!::class)
    }

    override fun navigate(destination: KClass<out Destinations>) {
        navigate(destination = destination, screensForRemoveFromStack = emptyList())
    }

    override fun navigate(
        destination: KClass<out Destinations>,
        screensForRemoveFromStack: List<KClass<out Destinations>>
    ) {
        val new = koin.get<Destinations>(destination)
        if (new is ExternalAppEvents) {
            navigateToExternalApp(new)
            return
        }
        val screensForRemoveFromStackInstance = screensForRemoveFromStack.map {
            koin.get<Destinations>(it)
        }
        if (initialBackStack.lastOrNull() == new) {
            updateCurrent()
            return
        }
        initialBackStack.removeAll(screensForRemoveFromStackInstance + new)
        initialBackStack.add(new)
        updateCurrent()
    }

    override fun navigateWithClear(destination: KClass<out Destinations>) {
        initialBackStack.clear()
        navigate(destination)
    }

    override fun subscribe(): Flow<Destinations> = current.filterNotNull()

    override fun watch(): CFlow<Destinations> = subscribe().wrap()

    override val fullGraph: List<Destinations> by lazy {
        fullDestinations.map { koin.get(it) }
    }

    override fun current(): Destinations? = current.value

    override fun navigateToExternalApp(destination: ExternalAppEvents) {
        externalAppEvents.tryEmit(destination)
    }

    override fun subscribeExternalAppEvents(): Flow<ExternalAppEvents> =
        externalAppEvents.asSharedFlow()

    private fun updateCurrent() {
        _current.value = initialBackStack.last()
    }

    private fun canGoBack(): Boolean = initialBackStack.size > 1

}
