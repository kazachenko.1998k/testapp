package com.common.feature.navigation.impl.callbacks.auth

import com.common.feature.auth.api.domain.HaveAccountQuestionCallbacks
import com.common.feature.navigation.api.LoginScreen
import com.common.feature.navigation.api.Navigation
import com.common.feature.navigation.api.RegisterScreen

class HaveAccountQuestionCallbacksImpl(
    private val navigator: Navigation
) : HaveAccountQuestionCallbacks {

    override fun onLoginClick() {
        navigator.navigate(LoginScreen::class)
    }

    override fun onRegisterClick() {
        navigator.navigate(RegisterScreen::class)
    }
}