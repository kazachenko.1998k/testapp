package com.common.feature.navigation.impl.callbacks.auth

import com.common.feature.auth.api.domain.LoginCallbacks
import com.common.feature.navigation.api.*

class LoginCallbacksImpl(
    private val navigator: Navigation,
) : LoginCallbacks {
    override fun onSuccessLogin() {
    }

    override fun onRegisterClick() {
        navigator.navigate(RegisterScreen::class)
    }

    override fun onForgotPasswordClick() {
        navigator.navigate(ResetPasswordScreen::class)
    }

    override fun onBackClick() {
        navigator.back()
    }
}