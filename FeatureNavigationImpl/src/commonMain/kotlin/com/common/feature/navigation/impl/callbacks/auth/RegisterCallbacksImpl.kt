package com.common.feature.navigation.impl.callbacks.auth

import com.common.feature.auth.api.domain.RegisterCallbacks
import com.common.feature.navigation.api.BrowserLink
import com.common.feature.navigation.api.LoginScreen
import com.common.feature.navigation.api.Navigation

class RegisterCallbacksImpl(private val navigator: Navigation) : RegisterCallbacks {

    override fun onSuccessRegister() {
    }

    override fun onLoginClick() {
        navigator.navigate(LoginScreen::class)
    }

    override fun onBackClick() {
        navigator.back()
    }


    override fun onPrivacyPolicies(link: String) {
        navigator.navigateToExternalApp(BrowserLink(link))
    }

    override fun onTermsAndConditions(link: String) {
        navigator.navigateToExternalApp(BrowserLink(link))
    }

}