package com.common.feature.navigation.impl.di

import com.common.feature.auth.api.domain.*
import com.common.feature.navigation.api.Navigation
import com.common.feature.navigation.impl.Navigator
import com.common.feature.navigation.impl.callbacks.auth.*
import org.koin.dsl.bind
import org.koin.dsl.module

val DI = module {
    single { Navigator() } bind Navigation::class

    single { LoginCallbacksImpl(get()) } bind LoginCallbacks::class
    single { RegisterCallbacksImpl(get()) } bind RegisterCallbacks::class
    single { HaveAccountQuestionCallbacksImpl(get()) } bind HaveAccountQuestionCallbacks::class
    single { ResetPasswordCallbacksImpl(get()) } bind ResetPasswordCallbacks::class
    single { VerifyCodeCallbacksImpl(get()) } bind VerifyCodeCallbacks::class
    single { SetNewEmailCallbacksImpl(get()) } bind SetNewEmailCallbacks::class
    single { SetNewPasswordCallbacksImpl(get()) } bind SetNewPasswordCallbacks::class
    single { SplashCallbacksImpl(get()) } bind SplashCallbacks::class
}