package com.common.feature.navigation.impl.callbacks.auth

import com.common.feature.auth.api.domain.SetNewPasswordCallbacks
import com.common.feature.navigation.api.Navigation

class SetNewPasswordCallbacksImpl(
    private val navigator: Navigation
) : SetNewPasswordCallbacks {

    override fun onSuccessSaveNewPass() {
    }

    override fun onBackClick() {
        navigator.back()
    }

}