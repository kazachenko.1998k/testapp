package com.common.feature.navigation.impl.callbacks.auth

import com.common.feature.auth.api.domain.SplashCallbacks
import com.common.feature.navigation.api.HaveAccountQuestionScreen
import com.common.feature.navigation.api.Navigation

class SplashCallbacksImpl(
    private val navigator: Navigation
) : SplashCallbacks {

    override fun onSuccessAuth() {
    }

    override fun onFailedAuth() {
        navigator.navigate(HaveAccountQuestionScreen::class)
    }

}