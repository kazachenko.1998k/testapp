package com.common.core.preferences.impl

import com.common.core.preferences.api.PreferencesApi
import org.koin.dsl.bind
import org.koin.dsl.module

/** Common
[https://github.com/russhwolf/multiplatform-settings]

expect val settings: FlowSettings

Android: actual val settings: FlowSettings = DataStoreSettings(/*...*/)

iOS: actual val settings: FlowSettings = AppleSettings(/*...*/).toFlowSettings()
 **/

val DI = module {
    single {
        PreferencesImpl(get())
    } bind PreferencesApi::class
}