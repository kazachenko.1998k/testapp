package com.common.core.preferences.impl

import com.common.core.preferences.api.PreferencesApi
import com.russhwolf.settings.coroutines.FlowSettings
import kotlinx.coroutines.flow.Flow

class PreferencesImpl(private val flowSettings: FlowSettings) : PreferencesApi {

    override suspend fun clear() =
        flowSettings.clear()

    override suspend fun remove(key: String) =
        flowSettings.remove(key)

    override suspend fun keys(): Set<String> =
        flowSettings.keys()

    override suspend fun size(): Int =
        flowSettings.size()

    override suspend fun hasKey(key: String): Boolean =
        flowSettings.hasKey(key)

    override suspend fun putInt(key: String, value: Int) =
        flowSettings.putInt(key, value)

    override fun getIntFlow(key: String, defaultValue: Int): Flow<Int> =
        flowSettings.getIntFlow(key, defaultValue)

    override suspend fun getInt(key: String, defaultValue: Int): Int =
        flowSettings.getInt(key, defaultValue)

    override fun getIntOrNullFlow(key: String): Flow<Int?> =
        flowSettings.getIntOrNullFlow(key)

    override suspend fun getIntOrNull(key: String): Int? =
        flowSettings.getIntOrNull(key)

    override suspend fun putLong(key: String, value: Long) =
        flowSettings.putLong(key, value)

    override fun getLongFlow(key: String, defaultValue: Long): Flow<Long> =
        flowSettings.getLongFlow(key, defaultValue)

    override suspend fun getLong(key: String, defaultValue: Long): Long =
        flowSettings.getLong(key, defaultValue)

    override fun getLongOrNullFlow(key: String): Flow<Long?> =
        flowSettings.getLongOrNullFlow(key)

    override suspend fun getLongOrNull(key: String): Long? =
        flowSettings.getLongOrNull(key)

    override suspend fun putString(key: String, value: String) =
        flowSettings.putString(key, value)

    override fun getStringFlow(key: String, defaultValue: String): Flow<String> =
        flowSettings.getStringFlow(key, defaultValue)

    override suspend fun getString(key: String, defaultValue: String): String =
        flowSettings.getString(key, defaultValue)

    override fun getStringOrNullFlow(key: String): Flow<String?> =
        flowSettings.getStringOrNullFlow(key)

    override suspend fun getStringOrNull(key: String): String? =
        flowSettings.getStringOrNull(key)

    override suspend fun putFloat(key: String, value: Float) =
        flowSettings.putFloat(key, value)

    override fun getFloatFlow(key: String, defaultValue: Float): Flow<Float> =
        flowSettings.getFloatFlow(key, defaultValue)

    override suspend fun getFloat(key: String, defaultValue: Float): Float =
        flowSettings.getFloat(key, defaultValue)

    override fun getFloatOrNullFlow(key: String): Flow<Float?> =
        flowSettings.getFloatOrNullFlow(key)

    override suspend fun getFloatOrNull(key: String): Float? =
        flowSettings.getFloatOrNull(key)

    override suspend fun putDouble(key: String, value: Double) =
        flowSettings.putDouble(key, value)

    override fun getDoubleFlow(key: String, defaultValue: Double): Flow<Double> =
        flowSettings.getDoubleFlow(key, defaultValue)

    override suspend fun getDouble(key: String, defaultValue: Double): Double =
        flowSettings.getDouble(key, defaultValue)

    override fun getDoubleOrNullFlow(key: String): Flow<Double?> =
        flowSettings.getDoubleOrNullFlow(key)

    override suspend fun getDoubleOrNull(key: String): Double? =
        flowSettings.getDoubleOrNull(key)

    override suspend fun putBoolean(key: String, value: Boolean) =
        flowSettings.putBoolean(key, value)

    override fun getBooleanFlow(key: String, defaultValue: Boolean): Flow<Boolean> =
        flowSettings.getBooleanFlow(key, defaultValue)

    override suspend fun getBoolean(key: String, defaultValue: Boolean): Boolean =
        flowSettings.getBoolean(key, defaultValue)

    override fun getBooleanOrNullFlow(key: String): Flow<Boolean?> =
        flowSettings.getBooleanOrNullFlow(key)

    override suspend fun getBooleanOrNull(key: String): Boolean? =
        flowSettings.getBooleanOrNull(key)
}