apply(from = "../versions.gradle.kts")
val koin_version: String by extra
val coroutines_version: String by extra
val multiplatform_settings: String by extra

plugins {
    id("multiplatform-library-convention")
}

kotlin {
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(project(":Utils"))
                implementation(project(":CorePreferenceApi"))
                implementation("com.russhwolf:multiplatform-settings-coroutines-native-mt:$multiplatform_settings")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
                implementation("io.insert-koin:koin-core:$koin_version")
            }
        }
        val androidMain by getting {
            dependencies {
                implementation(project(":CorePreferenceApi"))
                implementation("io.insert-koin:koin-core:$koin_version")
            }
        }
        val iosMain by getting {
            dependencies {
                implementation(project(":CorePreferenceApi"))
                implementation("io.insert-koin:koin-core:$koin_version")
            }
        }
    }
}
