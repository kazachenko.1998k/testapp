import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

apply(from = "../versions.gradle.kts")
val core_ktx: String by extra
val ktor_version: String by extra

plugins {
    id("multiplatform-library-convention")
}

version = "1.0"

kotlin {
    val iosTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget = when {
        System.getenv("SDK_NAME")?.startsWith("iphoneos") == true -> ::iosArm64
//        System.getenv("NATIVE_ARCH")?.startsWith("arm") == true -> ::iosSimulatorArm64
        else -> ::iosX64
    }

    iosTarget("ios") {
        binaries {
            framework {
                baseName = "shared"
                export(project(":CoreMVIAbstract"))
                export(project(":CoreMVIImpl"))
                export(project(":FeatureAuthApi"))
                export(project(":FeatureAuthImpl"))
                export(project(":FeatureNavigationApi"))
                export(project(":FeatureNavigationImpl"))
                export(project(":Utils"))
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(project(":Utils"))
                api(project(":CoreDI"))
                api(project(":CoreMVIAbstract"))
                api(project(":CorePreferenceApi"))
                api(project(":CoreBackendApi"))
                api(project(":FeatureAuthApi"))
                api(project(":FeatureNavigationApi"))
            }
        }
        val androidMain by getting {
            dependencies {
                api("io.ktor:ktor-client-android:$ktor_version")
            }
        }
        val iosMain by getting {
            dependencies {
                implementation("io.ktor:ktor-client-ios:$ktor_version")
                api(project(":Utils"))
                api(project(":CoreDI"))
                api(project(":CoreMVIAbstract"))
                api(project(":CoreMVIImpl"))
                api(project(":CorePreferenceApi"))
                api(project(":CoreBackendApi"))
                api(project(":FeatureAuthApi"))
                api(project(":FeatureAuthImpl"))
                api(project(":FeatureNavigationApi"))
                api(project(":FeatureNavigationImpl"))
            }
        }
    }
}
