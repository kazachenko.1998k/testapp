package com.common

import org.koin.core.KoinApplication
import org.koin.core.module.Module


expect class MultiplatformTools {
    fun initKoinPlatform(modules: List<Module> = emptyList()): KoinApplication
}
