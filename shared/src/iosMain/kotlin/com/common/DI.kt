package com.common

import com.russhwolf.settings.AppleSettings
import com.russhwolf.settings.ExperimentalSettingsApi
import com.russhwolf.settings.coroutines.FlowSettings
import com.russhwolf.settings.coroutines.toFlowSettings
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.dsl.bind
import org.koin.dsl.module
import platform.Foundation.NSUserDefaults

val settingsDelegate = NSUserDefaults


@OptIn(ExperimentalSettingsApi::class, ExperimentalCoroutinesApi::class)
val DIStore = module {
    single {
        AppleSettings(settingsDelegate.standardUserDefaults).toFlowSettings()
    } bind FlowSettings::class
}