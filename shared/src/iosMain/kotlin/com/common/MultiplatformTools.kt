package com.common

import com.common.core.initKoin
import com.russhwolf.settings.ExperimentalSettingsApi
import org.koin.core.KoinApplication
import io.ktor.client.engine.ios.*
import org.koin.core.module.Module

actual class MultiplatformTools {
    @OptIn(ExperimentalSettingsApi::class)
    actual fun initKoinPlatform(modules: List<Module>): KoinApplication {
        return initKoin(Ios, modules
            .plus(com.common.feature.navigation.api.DI)
            .plus(DIStore)
        )
    }
}