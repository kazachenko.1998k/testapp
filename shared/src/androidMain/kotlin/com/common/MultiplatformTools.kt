package com.common

import com.common.core.initKoin
import io.ktor.client.engine.android.*
import org.koin.core.KoinApplication
import org.koin.core.module.Module


actual class MultiplatformTools {

    actual fun initKoinPlatform(modules: List<Module>): KoinApplication =
        initKoin(Android, modules)

}