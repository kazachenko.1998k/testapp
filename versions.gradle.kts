//Build versions
mapOf(
    "build_tools_gradle" to "7.0.2",
    "build_konfig_gradle" to "0.9.0",
    "kotlin_gradle_plugin" to "1.6.10",
    "kotlin_version" to "1.6.10",
    "detekt_formatting" to "1.15.0",
    "detekt_gradle_plugin" to "1.15.0",
    "icerock_mobile_multiplatform" to "0.12.0",
    "dokka" to "1.5.0"
).forEach { (name, version) ->
    extra.set(name, version)
}

//Common versions
mapOf(
    "coroutines_version" to "1.5.1-native-mt",
    "koin_version" to "3.1.2",
    "ktor_version" to "1.6.5",
    "koin_ktor" to "3.1.2",
    "core_ktx" to "1.6.0",
    "serialization_version" to "1.2.1",
    "logback_version" to "1.2.5",
    "multiplatform_settings" to "0.8",
    "google_services" to "4.3.10",
    "firebase_perf_plugin" to "1.4.0",
    "firebase_crashlytics_gradle" to "2.7.1"
).forEach { (name, version) ->
    extra.set(name, version)
}

//Android versions
mapOf(
    "amplitude_sdk" to "2.29.1",
    "okhttp" to "4.9.1",
    "one_signal" to "4.6.3",
    "huawei_ads" to "3.4.28.305",
    "android_browser" to "1.4.0",
    "play_services_ads_identifier" to "17.1.0",
    "branch_sdk" to "5.0.15",
    "activity_version" to "1.4.0",
    "compose_ui" to "1.1.0-rc01",
    "lifecycle_runtime_ktx" to "2.4.0",
    "splashscreen" to "1.0.0-alpha02",
    "constraintlayout_compose" to "1.0.0-rc01",
    "gson" to "2.8.6",
    "android_chart" to "v3.1.0",
    "compose_shimmer" to "1.0.0",
    "datastore" to "1.0.0",
    "nav_version" to "2.4.0-beta01",
    "accompanist_version" to "0.22.0-rc",
    "compose_activity" to "1.3.1",
    "firebase_bom" to "29.0.0",
    "firebase_installations" to "17.0.0",
    "coil_compose" to "1.4.0",
    "lottie_version" to "4.2.2",
    "crisp" to "1.0.7",
    "exoplayer" to "2.13.3",
    "androidx_camera" to "1.0.2",
    "androidx_camera_view" to "1.0.0-alpha31",
    "barcode_scanning" to "17.0.0",
    "billing_version" to "4.0.0",
    "guava" to "27.0.1-android",
    "mockito" to "2.22.0",
    "orchestrator" to "1.4.0",
    "multidex" to "2.0.1",
    "min_sdk" to "23",
    "target_sdk" to "31",
    "compile_sdk" to "android-31",
    "application_id" to "com.test.android",
    "version_code" to "1",
    "version_name" to "1.0.0"
).forEach { (name, version) ->
    extra.set(name, version)
}

//Analytics keys
mapOf(
    "oneSignalAppID" to "06ff4c1d-4116-46d3-a066-7063c380f6e7",
    "amplitudeKeyTest" to "qwertyuisdfghjxcvbnm",
    "amplitudeKeyLive" to "qwertyasdfghzxcvbnmvbn",
    "branchKeyTest" to "key_test_qwertyuqwertyuqwertyui",
    "branchKeyLive" to "key_live_qwertyqwertyuqwedfghj",
).forEach { (name, version) ->
    extra.set(name, version)
}

//App fields
mapOf(
    "our_side_url_debug" to "https://qwerty",
    "our_side_url_release" to "https://qwerty"
).forEach { (name, version) ->
    extra.set(name, version)
}
